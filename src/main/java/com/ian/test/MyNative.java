package com.ian.test;

public class MyNative {
	static{
		System.loadLibrary("MyNative");
	}
	public native String sayHello();
	public native int processingVideo(String pathName, String videoName, String ext, int frameRate);
	
	public native int parseAllPoints(String pathName, String videoName, String ext, int frameRate, int isLastPart);
	
    public native int parseSmiles(String pathName, String videoName, String ext, int frameRate, int isLastPart);
    
    public native int parseTouches(String pathName, String videoName, String ext, int frameRate, int isLastPart);
    
    public native int parseMoves(String pathName, String videoName, String ext, int frameRate, int isLastPart);
	
}
