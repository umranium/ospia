package org.poscomp.eqclinic.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.service.interfaces.ApplicationService;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.poscomp.eqclinic.util.EmailTool;
import org.poscomp.eqclinic.util.SMSTool;
import org.poscomp.eqclinic.util.SecurityTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class ApplicationController {

    private static final Logger logger = Logger.getLogger(ApplicationController.class);

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private PatientService patientService;

    @Autowired
    private ScenarioService scenarioService;

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";

    @RequestMapping(value = DOCTORPATH + "/applyappoitment2/{aptId}", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue applyAppoitment2(HttpServletRequest request, @PathVariable int aptId) {

        Doctor doctor = (Doctor) SecurityTool.getActiveUser();
        Appointment apt = appointmentService.getAppointmentByAptId(aptId);

        int doctorId = doctor.getDoctorId();
        // check whether student has 2 pending request
        List<Appointment> apts = appointmentService.getDoctorAptStatus(doctorId, 8);
        List<Appointment> appliedApts = appointmentService.getDoctorAptStatus(doctorId, 2);
        List<Appointment> confirmedApts = appointmentService.getDoctorAptStatus(doctorId, 3);

        ReturnValue rv = new ReturnValue();
        if (apts.size() >= 1 && doctorId != 261) {
            rv.setContent("Thanks for completing one OSPIA! Curretly, we are unable to offer more appointments to you.");
            rv.setCode(0);
            return rv;
        }

        if ((appliedApts.size() >= 1 || confirmedApts.size() >= 1) && doctorId != 261) {
            rv.setContent("Thanks for applying one OSPIA! Curretly, we are unable to offer more appointments to you.");
            rv.setCode(0);
            return rv;
        }

        if (apt == null) {
            rv.setContent("Request error, please apply it again.");
            rv.setCode(0);
            return rv;
        }

        if (apt.getStatus() != 1) {
            rv.setContent("Sorry this appointment is not available for booking, please choose another time slot. Thanks.");
            rv.setCode(0);
            return rv;
        }

        if ((apt.getStarttime() - System.currentTimeMillis()) < 24 * 3600 * 1000) {
            rv.setContent("Sorry, you have to request the appointment at 24 hours before. Please choose another time slot. Thanks.");
            rv.setCode(0);
            return rv;
        }

        apt.setDoctorId(doctorId);
        apt.setDoctorname(doctor.getFirstname());
        apt.setStatus(2);
        appointmentService.saveApp(apt);

        int patientId = apt.getPatientId();
        Patient patient = patientService.getPatientByPatientId(patientId);

        // send a email to SP
        EmailTool.send(apt, patient, null);
        // send a sms to sp
        SMSTool.sendSMS(apt, doctor, patient);

        rv.setCode(1);
        return rv;

    }



    @RequestMapping(value = DOCTORPATH + "/applyappoitment/{aptId}", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue applyAppoitment(HttpServletRequest request, @PathVariable int aptId) {

        Doctor doc = (Doctor) SecurityTool.getActiveUser();

        Application applicationInDb = applicationService.findByAptIdDorctorId(aptId, doc.getDoctorId());
        ReturnValue rv = new ReturnValue();

        if (applicationInDb != null) {
            rv.setCode(0);
            rv.setContent("You already sent a request to this appoitment.");
        } else {
            Appointment apt = appointmentService.getAppointmentByAptId(aptId);
            
            if ((apt.getStarttime() - System.currentTimeMillis()) < 24 * 3600 * 1000) {
                rv.setContent("Sorry, you have to request the appointment at 24 hours before. Please choose another time slot. Thanks.");
                rv.setCode(0);
                return rv;
            }
            
            Application application = new Application();
            application.setApplyAt(System.currentTimeMillis());
            application.setAptId(aptId);
            application.setDoctorId(doc.getDoctorId());
            application.setResult(0);
            applicationService.saveApplication(application);
            rv.setCode(1);

            // update application number for the appointment
            if (apt.getApplicationNo() == null) {
                apt.setApplicationNo(1);
            } else {
                apt.setApplicationNo(apt.getApplicationNo() + 1);
            }
            appointmentService.saveApp(apt);
        }

        return rv;
    }

    @Scheduled(cron = "0 45 07 ? * *")
    @RequestMapping(value = "/selectapplication", method = {RequestMethod.GET})
    public void selectApplication() {

        long curTime = System.currentTimeMillis();
        Date today = new Date(curTime);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String todayText = formatter.format(today);
        Date date;
        try {
            date = formatter.parse(todayText);
            long start = date.getTime();
            long end = start + 3600 * 24 * 1000;

            // List<Appointment> apts = appointmentService.findBetweenTwoTime(start, end);
            List<Appointment> apts = appointmentService.findAllAvailabel(end);
            Map<Integer, String> patientToRemember = new HashMap<Integer, String>();

            for (Appointment appointment : apts) {
                int aptId = appointment.getAptid();
                List<Application> applications = applicationService.findByAptId(aptId);
                // find which doctor should be allocation to this appointments
                Doctor doctor = appointmentService.allocateApt(applications, appointment);

                if (doctor != null) {

                    Patient patient = patientService.getPatientByPatientId(appointment.getPatientId());
                    int key = patient.getPatientid();
                    if (patientToRemember.containsKey(key)) {
                        String value = patientToRemember.get(key);
                        value += "," + appointment.getAptid();
                        patientToRemember.put(key, value);
                    } else {
                        patientToRemember.put(key, appointment.getAptid() + "");
                    }
                    // send a email to SP
                    EmailTool.send(appointment, patient, null);
                    // send a sms to sp
                    SMSTool.sendSMS(appointment, doctor, patient);
                }
            }

            for (Entry<Integer, String> entry : patientToRemember.entrySet()) {
                int key = entry.getKey();
                String value = entry.getValue();
                Patient patient = patientService.getPatientByPatientId(key);
                if (patient.getAppliedApt() != null && !"".equals(patient.getAppliedApt())) {
                    patient.setAppliedApt(patient.getAppliedApt() + "," + value);
                } else {
                    patient.setAppliedApt(value);
                }

                patientService.savePatient(patient);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

}
