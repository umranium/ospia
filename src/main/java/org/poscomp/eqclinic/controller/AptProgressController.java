package org.poscomp.eqclinic.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.AppointmentProgress;
import org.poscomp.eqclinic.service.interfaces.AppointmentProgressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class AptProgressController {

    private static final Logger logger = Logger.getLogger(AptProgressController.class);

    @Autowired
    private AppointmentProgressService appointmentProgressService;

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";

    @RequestMapping(value = DOCTORPATH + "/resumedoctor/{apId}", method = { RequestMethod.GET })
    public String resumeDoctor(HttpServletRequest request, @PathVariable int apId) {

        AppointmentProgress ap = appointmentProgressService.getAppointmentProgressById(apId);

        String sessionId = ap.getSessionId();

        int stage = ap.getStage();
        String returnPage = "";
        switch (stage) {
            case 0:
                returnPage = "/perref1_stu";
                break;
            case 1:
                returnPage = "/autonomy_stu";
                break;
            case 2:
                returnPage = "/evaluatesp_stu";
                break;
            case 3:
                returnPage = "/feedback_stu";
                break;
            case 4:
                returnPage = "/second_soca_stu/" + sessionId;
                break;
            case 5:
                returnPage = "/thanks_stu";
                break;
            case 6:
                returnPage = "/rectype_stu";
                break;
            case 7:
                returnPage = "/selfsoca_stu";
                break;
            default:
                returnPage = "";
                break;
        }

        request.getSession().setAttribute("doctorSessionIdCombined", sessionId);
        return "redirect:" + DOCTORPATH + returnPage;
    }

    @RequestMapping(value = PATIENTPATH + "/resumepatient/{apId}", method = { RequestMethod.GET })
    public String resumePatient(HttpServletRequest request, @PathVariable int apId) {

        AppointmentProgress ap = appointmentProgressService.getAppointmentProgressById(apId);

        String sessionId = ap.getSessionId();

        int stage = ap.getStage();
        String returnPage = "";
        switch (stage) {
            case 0:
                returnPage = "soca_sp";
                break;
            case 1:
                returnPage = "autonomy_sp";
                break;
            case 2:
                returnPage = "feedback_sp";
                break;
            default:
                returnPage = "";
                break;
        }

        request.getSession().setAttribute("patientSessionIdCombined", sessionId);
        return "redirect:" + PATIENTPATH + "/" + returnPage;
    }

}
