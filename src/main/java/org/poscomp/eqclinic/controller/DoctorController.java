package org.poscomp.eqclinic.controller;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.LoginLog;
import org.poscomp.eqclinic.domain.StudentGroup;
import org.poscomp.eqclinic.domain.pojo.MyUserPrincipal;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.service.interfaces.AvailableStudentGroupService;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.LoginLogService;
import org.poscomp.eqclinic.service.interfaces.StudentGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class DoctorController {

    private static final Logger logger = Logger.getLogger(DoctorController.class);


    @Autowired
    private DoctorService doctorService;
    
    @Autowired
    private LoginLogService loginLogService;

    @Autowired
    private StudentGroupService studentGroupService;

    @Autowired
    private AvailableStudentGroupService availableStudentGroupService;

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";

    @RequestMapping(value = DOCTORPATH + "/updateagreement", method = {RequestMethod.POST},
                    headers = {"Content-type=application/json"})
    @ResponseBody
    public ReturnValue savePatient(HttpServletRequest request, @RequestBody Doctor doctor) {
        ReturnValue rv = new ReturnValue();
        if (doctor != null) {
            Doctor doctorInDb = doctorService.getDoctorByDoctorId(doctor.getDoctorId());
            doctorInDb.setAgreement(doctor.getAgreement());
            doctorService.saveDoctor(doctorInDb);
            rv.setCode(1);

            MyUserPrincipal activeUser =
                            (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            activeUser.setDoctor(doctorInDb);
        } else {
            rv.setCode(0);
        }
        return rv;
    }

    @RequestMapping(value = DOCTORPATH + "/finishtraining/{doctorId}", method = {RequestMethod.POST})
    @ResponseBody
    public ReturnValue finishTraning(HttpServletRequest request, @PathVariable int doctorId) {
        ReturnValue rv = new ReturnValue();

        Doctor doctorInDb = doctorService.getDoctorByDoctorId(doctorId);
        if (doctorInDb != null) {
            doctorInDb.setTraining(1);
            doctorService.saveDoctor(doctorInDb);
            rv.setCode(1);

            MyUserPrincipal activeUser =
                            (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            activeUser.setDoctor(doctorInDb);
        } else {
            rv.setCode(0);
        }
        return rv;

    }

    @RequestMapping(value = "/verifyUserDetails", method = {RequestMethod.POST},
                    headers = {"Content-type=application/json"})
    @ResponseBody
    public Doctor verifyUserDetails(HttpServletRequest request, @RequestBody Doctor doctor) {

        String allowGroupsStr = availableStudentGroupService.getAllAvailableGroups();

        String studentId = doctor.getUserId();
        StudentGroup sGroup = studentGroupService.getStudentGroupByStudentId(studentId);

        if (sGroup == null) {
            return null;
        }

        int groupNumber = sGroup.getGroupNumber();

        boolean isAllowed = false;
        String[] allowGroups = allowGroupsStr.split(",");


        for (int i = 0; i < allowGroups.length; i++) {
            if (allowGroups[i].equals(groupNumber + "")) {
                isAllowed = true;
                break;
            }
        }

        if (isAllowed == false) {
            return null;
        }

        Doctor doctorInDb = doctorService.getDoctorByUsername(doctor.getUsername());
        if (doctorInDb == null) {
            String password = UUID.randomUUID().toString();
            Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
            String hashedPassword = passwordEncoder.encodePassword(password, doctor.getUsername());
            doctor.setPassword(hashedPassword);
            doctor.setAgreement(0);
            doctor.setStatus(1);
            doctor.setLastLoginAt(0L);
            doctor.setTraining(0);
            doctorService.saveDoctor(doctor);
            doctor.setPassword(password);
            
            LoginLog loginLog = new LoginLog();
            loginLog.setDoctorId(doctor.getDoctorId());
            loginLog.setLoginAt(System.currentTimeMillis());
            loginLogService.saveLoginLog(loginLog);
            
            return doctor;

        } else {

            String password = UUID.randomUUID().toString();
            Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
            String hashedPassword = passwordEncoder.encodePassword(password, doctor.getUsername());
            doctorInDb.setPassword(hashedPassword);
            doctorService.saveDoctor(doctorInDb);
            doctorInDb.setPassword(password);
            
            LoginLog loginLog = new LoginLog();
            loginLog.setDoctorId(doctorInDb.getDoctorId());
            loginLog.setLoginAt(System.currentTimeMillis());
            loginLogService.saveLoginLog(loginLog);
            
            return doctorInDb;

        }
    }

}
