package org.poscomp.eqclinic.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.service.interfaces.ExportService;
import org.poscomp.eqclinic.util.SecurityTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class ExportController {

    private static final Logger logger = Logger.getLogger(ExportController.class);

    @Autowired
    private ExportService exportService;

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";

//    @RequestMapping(value = "/exportweek", method = {RequestMethod.GET})
//    public void applyAppoitment(HttpServletRequest request, @PathVariable int aptId) {
//        exportService.exportAptOffered(1487509200000L);
//    }


}
