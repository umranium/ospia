package org.poscomp.eqclinic.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Teacher;
import org.poscomp.eqclinic.domain.pojo.MyUserPrincipal;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.service.interfaces.DoctorService;
import org.poscomp.eqclinic.service.interfaces.PatientService;
import org.poscomp.eqclinic.service.interfaces.TeacherService;
import org.poscomp.eqclinic.util.EmailTool;
import org.poscomp.eqclinic.util.StringTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class LoginController {

    private static final Logger logger = Logger.getLogger(LoginController.class);

    private static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";

    @Autowired
    private PatientService patientService;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private TeacherService teacherService;

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";
    private static final String TUTORPATH = "/tut";

    @RequestMapping(value = PATIENTPATH, method = { RequestMethod.GET })
    public String login_patient(HttpServletRequest request) {
        request.setAttribute("redpage", "uhome_sp");
        return "sp/01_SP_home";
    }

    @RequestMapping(value = DOCTORPATH, method = { RequestMethod.GET })
    public String login_doctor(HttpServletRequest request) {
        request.setAttribute("redpage", "uhome_stu");
        return "stu/18_Stu_home";
    }

    @RequestMapping(value = TUTORPATH, method = { RequestMethod.GET })
    public String login_tutor(HttpServletRequest request) {
        request.setAttribute("redpage", "uhome_tut");
        return "tut/31_Tut_home";
    }

    /*
     * access doctor's login time
     */
    @RequestMapping(value = DOCTORPATH + "/doctorlogintime/{doctorId}", method = { RequestMethod.GET })
    @ResponseBody
    public ReturnValue doctorLoginTime(HttpServletRequest request, @PathVariable int doctorId) {

        Doctor doctor = doctorService.getDoctorByDoctorId(doctorId);
        Date date = new Date(doctor.getLastLoginAt());
        SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
        String dateText = sdf.format(date);

        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        rv.setContent(dateText);

        return rv;
    }

    /*
     * access patient's login time
     */
    @RequestMapping(value = PATIENTPATH + "/patientlogintime/{patientId}", method = { RequestMethod.GET })
    @ResponseBody
    public ReturnValue patientLoginTime(HttpServletRequest request, @PathVariable int patientId) {

        Patient patient = patientService.getPatientByPatientId(patientId);
        Date date = new Date(patient.getLastLoginAt());
        SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
        String dateText = sdf.format(date);

        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        rv.setContent(dateText);

        return rv;

    }

    /*
     * log off the system
     */
    @RequestMapping(value = PATIENTPATH + "/logoff", method = { RequestMethod.GET })
    public String patientlogoff(HttpServletRequest request) {

        HttpSession session = request.getSession();

        session.removeAttribute("loginedPatient");
        String redirectUrl = PATIENTPATH + "/ulogin_sp";
        session.removeAttribute("errorInfoD");
        return "redirect:" + redirectUrl;

    }

    @RequestMapping(value = DOCTORPATH + "/logoff", method = { RequestMethod.GET })
    public String doctorlogoff(HttpServletRequest request) {

        HttpSession session = request.getSession();
        String redirectUrl = DOCTORPATH + "/ulogin_stu";
        session.removeAttribute("loginedDoctor");
        return "redirect:" + redirectUrl;

    }

    @RequestMapping(value = TUTORPATH + "/logoff", method = { RequestMethod.GET })
    public String tutorlogoff(HttpServletRequest request) {

        HttpSession session = request.getSession();
        String redirectUrl = TUTORPATH + "/ulogin_tut";
        session.removeAttribute("loginedTeacher");
        return "redirect:" + redirectUrl;

    }

    /*
     * patient log in the system
     */
    @RequestMapping(value = PATIENTPATH + "/patientlogin", method = { RequestMethod.POST },
                    headers = { "Content-type=application/json" })
    @ResponseBody
    public ReturnValue patientLogin(HttpServletRequest request, @RequestBody Patient patient) {
        boolean isValid = false;
        Patient loginedPatient = null;
        if (patient != null && StringTool.isValidString(patient.getUsername())
                        && StringTool.isValidString(patient.getPassword())) {
            loginedPatient = patientService.patientLogin(patient.getUsername(), patient.getPassword());
            if (loginedPatient != null) {
                isValid = true;
            }
        }
        ReturnValue rv = new ReturnValue();
        if (!isValid) {
            rv.setCode(0);
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("loginedPatient", loginedPatient);
            rv.setCode(1);
        }

        return rv;
    }

    /*
     * doctor log in the system
     */
    @RequestMapping(value = DOCTORPATH + "/doctorlogin", method = { RequestMethod.POST },
                    headers = { "Content-type=application/json" })
    @ResponseBody
    public ReturnValue doctorLogin(HttpServletRequest request, @RequestBody Doctor doctor) {

        boolean isValid = false;
        Doctor loginedDoctor = null;
        if (doctor != null && StringTool.isValidString(doctor.getUsername())
                        && StringTool.isValidString(doctor.getPassword())) {
            loginedDoctor = doctorService.doctorLogin(doctor.getUsername(), doctor.getPassword());
            if (loginedDoctor != null) {
                isValid = true;
            }
        }
        ReturnValue rv = new ReturnValue();
        if (!isValid) {
            rv.setCode(0);
        } else {
            HttpSession session = request.getSession();
            loginedDoctor.setLastLoginAt(System.currentTimeMillis());
            doctorService.saveDoctor(loginedDoctor);
            session.setAttribute("loginedDoctor", loginedDoctor);
            rv.setCode(1);
        }

        return rv;

    }

    /*
     * tutor log in the system
     */
    @RequestMapping(value = TUTORPATH + "/tutorlogin", method = { RequestMethod.POST },
                    headers = { "Content-type=application/json" })
    @ResponseBody
    public ReturnValue tutorLogin(HttpServletRequest request, @RequestBody Teacher teacher) {

        boolean isValid = false;
        Teacher loginedTeacher = null;
        if (teacher != null && StringTool.isValidString(teacher.getUsername())
                        && StringTool.isValidString(teacher.getPassword())) {
            loginedTeacher = teacherService.teacherLogin(teacher.getUsername(), teacher.getPassword());
            if (loginedTeacher != null) {
                isValid = true;
            }
        }
        ReturnValue rv = new ReturnValue();
        if (!isValid) {
            rv.setCode(0);
        } else {
            HttpSession session = request.getSession();
            loginedTeacher.setLastLoginAt(System.currentTimeMillis());
            teacherService.saveTeacher(loginedTeacher);
            session.setAttribute("loginedTeacher", loginedTeacher);
            rv.setCode(1);
        }
        return rv;
    }
    
    
    /*
     * SP forgot password
     */
    @RequestMapping(value = PATIENTPATH + "/forgotPwd", method = { RequestMethod.GET})
    public String patientForgotPwd(HttpServletRequest request) {
    	request.setAttribute("redpage", "ulogin_sp");
    	return "sp/02_SP_forgotPwd";
    }
    
    /*
     * SP forgot password
     */
    @RequestMapping(value = PATIENTPATH + "/resetPwd", method = { RequestMethod.POST }, headers = { "Content-type=application/json" })
    @ResponseBody
    public ReturnValue patientResetPwd(HttpServletRequest request, @RequestBody Patient patient) {
    	ReturnValue rv = new ReturnValue();
    	if(patient!=null){
    		Patient patientInDB = patientService.getPatientByUsername(patient.getUsername());
    		if(patientInDB==null){
    			rv.setCode(0);
    			rv.setContent("This email is not registered, please check the email again.");
    		}else{
    			// send an email to the users
        		EmailTool.sendResetPwdToSP(patientInDB);
        		rv.setCode(1);
        		rv.setContent("We have sent an email to your registered email '"+patient.getUsername()+"', please check that email and finish the password resetting.");
    		}
    	}else{
    		rv.setCode(0);
    		rv.setContent("This email is not registered, please check the email again.");
    	}
    	return rv;
    }
    
    /*
     * SP forgot password
     */
    @RequestMapping(value = PATIENTPATH + "/resetNewPwd/{patientId}", method = { RequestMethod.GET })
    public String patientResetNewPwd(HttpServletRequest request, @PathVariable int patientId) {
    	Patient patient = patientService.getPatientByPatientId(patientId);
    	if(patient!=null){
    		request.setAttribute("patient", patient);
    		return "sp/02_SP_setNewPwd";
    	}else{
    		return "";
    	}
    }
    
    @RequestMapping(value = PATIENTPATH + "/confirmNewPwd",  method = { RequestMethod.POST }, headers = { "Content-type=application/json" })
    @ResponseBody
    public ReturnValue patientConfirmNewPwd(HttpServletRequest request, @RequestBody Patient patient) {
    	ReturnValue rv = new ReturnValue();
    	if(patient != null){
    		Patient patientInDB = patientService.getPatientByPatientId(patient.getPatientid());
    		if(patientInDB != null){
    			Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
    			String hashedNewPassword = passwordEncoder.encodePassword(patient.getPassword(), patientInDB.getUsername());
    			patientInDB.setPassword(hashedNewPassword);
                patientService.savePatient(patientInDB);
                rv.setCode(1);
                rv.setContent("Successfully reset your password");
                return rv;
    		}
    	}
    	rv.setCode(0);
        rv.setContent("Sorry, you are fail to reset your password, please try again and contact the administrator of OSPIA.");
        return rv;
    }
    
}
