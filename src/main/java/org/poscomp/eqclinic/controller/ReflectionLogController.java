package org.poscomp.eqclinic.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.ReflectionLog;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.service.interfaces.ReflectionLogService;
import org.poscomp.eqclinic.util.SecurityTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class ReflectionLogController {

    private static final Logger logger = Logger.getLogger(ReflectionLogController.class);

    @Autowired
    private ReflectionLogService reflectionLogService;

    private static final String DOCTORPATH = "/stu";

    @RequestMapping(value = DOCTORPATH + "/addreflectionlog", method = {RequestMethod.POST},
                    headers = {"Content-type=application/json"})
    @ResponseBody
    public ReturnValue addReflectionLog(HttpServletRequest request, @RequestBody ReflectionLog reflectionLog) {

        Doctor doc = (Doctor) SecurityTool.getActiveUser();

        ReturnValue rv = new ReturnValue();
        reflectionLog.setDoctorId(doc.getDoctorId());
        reflectionLogService.save(reflectionLog);
        rv.setCode(1);
        return rv;
    }

    @RequestMapping(value = DOCTORPATH + "/updatereflectionlog/{reflectionLogId}", method = {RequestMethod.POST})
    public ReturnValue updateReflectionLog(HttpServletRequest request, @PathVariable int reflectionLogId) {
        ReflectionLog log = reflectionLogService.getById(reflectionLogId);
        ReturnValue rv = new ReturnValue();
        if(log!=null){
            log.setEndTime(System.currentTimeMillis());
            reflectionLogService.save(log);
            rv.setCode(1);
        }else{
            rv.setCode(1);
        }
        return rv;
    }


}
