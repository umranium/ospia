package org.poscomp.eqclinic.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.domain.Video;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.poscomp.eqclinic.service.interfaces.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class TeacherController {

    private static final Logger logger = Logger.getLogger(TeacherController.class);

    private static final String DOCTORPATH = "/stu";
    private static final String PATIENTPATH = "/sp";
    private static final String TUTORPATH = "/tut";

    @Autowired
    private ScenarioService scenarioService;

    @Autowired
    private VideoService videoService;

    @RequestMapping(value = TUTORPATH + "/reviewscreen_tut/{archiveId}", method = { RequestMethod.GET })
    public String redirect_reviewscreen_tut(HttpServletRequest request, @PathVariable String archiveId) {
        System.out.println(archiveId);
        request.setAttribute("redpage", "review_tut");
        Video video = videoService.getVideoByArchiveId(archiveId);
        request.setAttribute("video", video);

        String sceCode = video.getSceCode();
        List<Scenario> sce = scenarioService.getScenarioByCode(sceCode);
        request.setAttribute("scenario", sce.get(0));

        return "tut/33_Tut_SP_screen";
    }

}
