package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.ApplicationDao;
import org.poscomp.eqclinic.domain.Application;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "applicationDao")
public class ApplicationDaoImpl implements ApplicationDao {

    private static final Logger logger = Logger.getLogger(ApplicationDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveApplication(Application app) {
        sessionFactory.getCurrentSession().saveOrUpdate(app);
        logger.info("you have insert an application");
    }
    
    @Override
    public List<Application> findByAptId(int aptId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Application as a where a.aptId=:aptId";
        Query query = session.createQuery(hql);
        query.setInteger("aptId", aptId);
        List<Application> list = (ArrayList<Application>) query.list();
        return list;
    }

    @Override
    public Application findById(int id) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Application as a where a.id=:id";
        Query query = session.createQuery(hql);
        query.setInteger("id", id);
        Application result = (Application) query.uniqueResult();
        return result;
    }

    @Override
    public Application findByAptIdDorctorId(int aptId, int doctorId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Application as a where a.aptId=:aptId and a.doctorId=:doctorId ";
        Query query = session.createQuery(hql);
        query.setInteger("aptId", aptId);
        query.setInteger("doctorId", doctorId);
        Application result = (Application) query.uniqueResult();
        return result;
    }

    @Override
    public List<Application> findByDoctorId(int doctorId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Application as a where a.doctorId=:doctorId ";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        List<Application> list = (ArrayList<Application>) query.list();
        return list;
    }

    @Override
    public List<Application> findPendingByDoctorId(int doctorId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Application as a where a.doctorId=:doctorId and a.result=0 ";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        List<Application> list = (ArrayList<Application>) query.list();
        return list;
    }

    @Override
    public void deleteApplication(Application app) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(app);
    }
    
}
