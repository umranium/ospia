package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.ApplicationDao;
import org.poscomp.eqclinic.dao.interfaces.EmailClickingDao;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.EmailClicking;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "emailClickingDao")
public class EmailClickingDaoImpl implements EmailClickingDao {

    private static final Logger logger = Logger.getLogger(EmailClickingDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveEmailClicking(EmailClicking clicking) {
        sessionFactory.getCurrentSession().saveOrUpdate(clicking);
        logger.info("you have insert an application");
    }
    
}
