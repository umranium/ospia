package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.ApplicationDao;
import org.poscomp.eqclinic.dao.interfaces.LoginLogDao;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.LoginLog;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "loginLogDao")
public class LoginLogDaoImpl implements LoginLogDao {

    private static final Logger logger = Logger.getLogger(LoginLogDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveLoginLog(LoginLog loginLog) {
        sessionFactory.getCurrentSession().saveOrUpdate(loginLog);
        logger.info("you have insert a log log");
    }
    
    
}
