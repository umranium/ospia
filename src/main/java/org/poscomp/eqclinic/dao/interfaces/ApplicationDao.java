package org.poscomp.eqclinic.dao.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Application;


/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface ApplicationDao {
    
    public void saveApplication(Application app);
    
    public void deleteApplication(Application app);
    
    public Application findById(int id);
    
    public List<Application> findByAptId(int aptId);
 
    public Application findByAptIdDorctorId(int aptId, int doctorId);
    
    public List<Application> findByDoctorId(int doctorId);
    
    public List<Application> findPendingByDoctorId(int doctorId);
    
}
