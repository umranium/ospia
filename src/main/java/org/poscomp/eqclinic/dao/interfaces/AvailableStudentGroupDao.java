package org.poscomp.eqclinic.dao.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Application;


/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface AvailableStudentGroupDao {
    
    public String getAllAvailableGroups();
    
}
