package org.poscomp.eqclinic.dao.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.EmailClicking;


/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface EmailClickingDao {
    
    public void saveEmailClicking(EmailClicking clicking);
    
}
