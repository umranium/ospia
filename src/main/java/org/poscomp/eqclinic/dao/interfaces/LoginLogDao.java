package org.poscomp.eqclinic.dao.interfaces;

import org.poscomp.eqclinic.domain.LoginLog;


/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface LoginLogDao {
    
    public void saveLoginLog(LoginLog loginLog);
    
}
