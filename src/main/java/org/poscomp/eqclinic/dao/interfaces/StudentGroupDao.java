package org.poscomp.eqclinic.dao.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.StudentGroup;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface StudentGroupDao {

    public StudentGroup getStudentGroupByStudentId(String studentId);

    public List<StudentGroup> getAllUnParticipatedStudent();
    
}
