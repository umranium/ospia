package org.poscomp.eqclinic.dao.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.UnavailablePeriod;


/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface UnavailablePeriodDao {
    
    public void saveUnavailablePeriod(UnavailablePeriod period);
    
    public UnavailablePeriod getUnavailablePeriodById(int id);

    
    public List<UnavailablePeriod> getUnavailablePeriod();
    
}
