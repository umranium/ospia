package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "application", catalog = "ospia")
public class Application implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer aptId;
    private Integer doctorId;
    private Long applyAt;
    // indicate whether this application is selected
    // 0: default 1: selected -1: not selected
    private Integer result;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "aptId")
    public Integer getAptId() {
        return aptId;
    }

    public void setAptId(Integer aptId) {
        this.aptId = aptId;
    }

    @Column(name = "doctorId")
    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    @Column(name = "applyAt")
    public Long getApplyAt() {
        return applyAt;
    }

    public void setApplyAt(Long applyAt) {
        this.applyAt = applyAt;
    }

    @Column(name = "result")
    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

}
