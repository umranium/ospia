package org.poscomp.eqclinic.domain.pojo;

import java.io.Serializable;

import org.poscomp.eqclinic.domain.Application;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class AppointmentOrderObject implements Comparable<AppointmentOrderObject>, Serializable {

    private static final long serialVersionUID = 1L;

    private Long score;
    private Application application;

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public AppointmentOrderObject(Long score, Application application) {
        this.score = score;
        this.application = application;
    }

    @Override
    public int compareTo(AppointmentOrderObject o) {
        if (this.score - o.getScore() > 0) {
            return 1;
        } else {
            return -1;
        }
    }

}
