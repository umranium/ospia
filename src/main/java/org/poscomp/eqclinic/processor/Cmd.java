package org.poscomp.eqclinic.processor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.util.ParserSmile;
import org.poscomp.eqclinic.util.SoundTool;
import org.poscomp.eqclinic.util.Tools;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class Cmd {

    private static final Logger logger = Logger.getLogger(Cmd.class);

    public static void main(String[] args) {
        
        Cmd cmd = new Cmd();
//        cmd.concatMultipleVideos("c:\\videos\\ffmpeg.exe", "C:/videos/video_1457684926205_doctor",3, "C:/videos/video_1457684926205_doctor");
        
        String exePath = "c:\\videos\\ffmpeg.exe";
        String inputFile = "C:/videos/video_1472104767272_patient";
        
        float audioLength = cmd.getDurationVideo(exePath, inputFile+".wav");
        cmd.cutAudio(exePath, inputFile+".wav", 0, 600, inputFile+"0.wav");
        cmd.cutAudio(exePath, inputFile+".wav", 600, 600, inputFile+"1.wav");
        cmd.cutAudio(exePath, inputFile+".wav", 1200, 600, inputFile+"2.wav");
        cmd.cutAudio(exePath, inputFile+".wav", 1800, audioLength-1800, inputFile+"3.wav");
        
        
    }
    
    public int cutSoundForProcessing(String exePath, String fileDir, String fileName){
        Cmd cmd = new Cmd();
        String inputFile = fileDir +"/"+fileName;
        float audioLength = cmd.getDurationVideo(exePath, inputFile+".wav");
        if(audioLength<=600){
            return 1;
        }else if(audioLength>600 && audioLength<=1200){
            cmd.cutAudio(exePath, inputFile+".wav", 0, 600, inputFile+"0.wav");
            cmd.cutAudio(exePath, inputFile+".wav", 600, audioLength-600, inputFile+"1.wav");
            return 2;
        }else if(audioLength>1200 && audioLength<=1800){
            cmd.cutAudio(exePath, inputFile+".wav", 0, 600, inputFile+"0.wav");
            cmd.cutAudio(exePath, inputFile+".wav", 600, 600, inputFile+"1.wav");
            cmd.cutAudio(exePath, inputFile+".wav", 1200, audioLength-1200, inputFile+"2.wav");
            return 3;
        }else{
            cmd.cutAudio(exePath, inputFile+".wav", 0, 600, inputFile+"0.wav");
            cmd.cutAudio(exePath, inputFile+".wav", 600, 600, inputFile+"1.wav");
            cmd.cutAudio(exePath, inputFile+".wav", 1200, 600, inputFile+"2.wav");
            cmd.cutAudio(exePath, inputFile+".wav", 1800, audioLength-1800, inputFile+"3.wav");
            return 4;
        }
    }
    
    public void mergePitchPowerFiles(String fileDir,String fileName, String type, List<String> inputFiles){
        
        String outputFile = fileDir + "/" +fileName+"_"+type+".txt";
        
        try{
            BufferedWriter out=new BufferedWriter(new FileWriter(outputFile));
            String allLines = "";
            for (int i = 0; i < inputFiles.size(); i++) {
                
                BufferedReader in = new BufferedReader(new FileReader(inputFiles.get(i)));
                String line = in.readLine();
                while (line != null && !"".equals(line.trim())) {
                    allLines += line+",";
                    line = in.readLine();
                }
                in.close();
            }
            out.write(allLines);
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
public void mergePitchPowerFiles(String fileDir,String fileName, String type, List<String> inputFiles, float totalLength){
        
        String outputFile = fileDir + "/" +fileName+"_"+type+".txt";
        
        try{
            BufferedWriter out=new BufferedWriter(new FileWriter(outputFile));
            String allLines = "";
            allLines += totalLength+"\r\n";
            
            for (int i = 0; i < inputFiles.size(); i++) {
                
                BufferedReader in = new BufferedReader(new FileReader(inputFiles.get(i)));
                String line = in.readLine();
                while (line != null && !"".equals(line.trim())) {
                    allLines += line+",";
                    line = in.readLine();
                }
                in.close();
            }
            out.write(allLines);
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void mergeEndpointsFiles(String fileDir,String fileName, String type, List<String> inputFiles){
        
        String outputFile = fileDir + "/" +fileName+"_"+type+".txt";
        
        try{
            BufferedWriter out=new BufferedWriter(new FileWriter(outputFile));
            String allLines = "";
            for (int i = 0; i < inputFiles.size(); i++) {
                
                BufferedReader in = new BufferedReader(new FileReader(inputFiles.get(i)));
                String line = in.readLine();
                while (line != null && !"".equals(line.trim())) {
                    allLines += line+"\r\n";
                    line = in.readLine();
                }
                in.close();
            }
            out.write(allLines);
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
    }
    
    
    
    
    public void processPitchAudio(String exePath, String fileDir, String fileName){
        Cmd cmd = new Cmd();
        String inputFile = fileDir +"/"+fileName;
        float audioLength = cmd.getDurationVideo(exePath, inputFile+".wav");
        
        List<String> inputFilesPitch = new ArrayList<String>();
        
        SoundTool tool = new SoundTool();
        if(audioLength<=600){
            tool.processPitch(fileDir, fileName, fileDir, fileName);
            tool = null;
            return;
        }else if(audioLength>600 && audioLength<=1200){
            cmd.cutAudio(exePath, inputFile+".wav", 0, 600, inputFile+"0.wav");
            cmd.cutAudio(exePath, inputFile+".wav", 600, audioLength-600, inputFile+"1.wav");
            
            tool.processPitch(fileDir, fileName+"0", fileDir, fileName+"0");
            tool.processPitch(fileDir, fileName+"1", fileDir, fileName+"1");
            
            inputFilesPitch.add(fileDir + "/" +fileName+"0_pitch.txt");
            inputFilesPitch.add(fileDir + "/" +fileName+"1_pitch.txt");

        }else if(audioLength>1200 && audioLength<=1800){
            cmd.cutAudio(exePath, inputFile+".wav", 0, 600, inputFile+"0.wav");
            cmd.cutAudio(exePath, inputFile+".wav", 600, 600, inputFile+"1.wav");
            cmd.cutAudio(exePath, inputFile+".wav", 1200, audioLength-1200, inputFile+"2.wav");
            
            tool.processPitch(fileDir, fileName+"0", fileDir, inputFile+"0");
            tool.processPitch(fileDir, fileName+"1", fileDir, inputFile+"1");
            tool.processPitch(fileDir, fileName+"2", fileDir, inputFile+"2");
            
            inputFilesPitch.add(fileDir + "/" +fileName+"0_pitch.txt");
            inputFilesPitch.add(fileDir + "/" +fileName+"1_pitch.txt");
            inputFilesPitch.add(fileDir + "/" +fileName+"2_pitch.txt");

        }else if(audioLength>1800){
            cmd.cutAudio(exePath, inputFile+".wav", 0, 600, inputFile+"0.wav");
            cmd.cutAudio(exePath, inputFile+".wav", 600, 600, inputFile+"1.wav");
            cmd.cutAudio(exePath, inputFile+".wav", 1200, 600, inputFile+"2.wav");
            cmd.cutAudio(exePath, inputFile+".wav", 1800, audioLength-1800, inputFile+"3.wav");
            
            tool.processPitch(fileDir, fileName+"0", fileDir, inputFile+"0");
            tool.processPitch(fileDir, fileName+"1", fileDir, inputFile+"1");
            tool.processPitch(fileDir, fileName+"2", fileDir, inputFile+"2");
            tool.processPitch(fileDir, fileName+"3", fileDir, inputFile+"3");
            
            inputFilesPitch.add(fileDir + "/" +fileName+"0_pitch.txt");
            inputFilesPitch.add(fileDir + "/" +fileName+"1_pitch.txt");
            inputFilesPitch.add(fileDir + "/" +fileName+"2_pitch.txt");
            inputFilesPitch.add(fileDir + "/" +fileName+"3_pitch.txt");
        }
        
        
        String outputFile = fileDir + "/" +fileName+"_pitch.txt";
        
        try{
            BufferedWriter out=new BufferedWriter(new FileWriter(outputFile));
            String allLines = "";
            for (int i = 0; i < inputFilesPitch.size(); i++) {
                
                BufferedReader in = new BufferedReader(new FileReader(inputFilesPitch.get(i)));
                String line = in.readLine();
                while (line != null && !"".equals(line.trim())) {
                    allLines += line+",";
                    line = in.readLine();
                }
                in.close();
            }
            out.write(allLines);
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tool = null;
    }
    
    
    // interval: 120 second
    public boolean splitVideo(String exePath, String inputFile, int interval) {

        float duration = getDurationVideo(exePath, inputFile);
        int splitNumber = (int) (duration / interval);
        float lastPartLength = ((duration / interval) - (int) (duration / interval)) * interval;

        int length = inputFile.length();
        String part1 = inputFile.substring(0, length - 4);
        String part2 = inputFile.substring(length - 4);

        for (int i = 0; i <= splitNumber; i++) {
            String outputFile = part1 + i + part2;

            float startTime = i * interval;
            float endTime = 0;

            if (i == splitNumber) {
                endTime = lastPartLength;
            } else {
                endTime = interval;
            }
            cutVideo(exePath, inputFile, startTime, endTime, outputFile);
        }
        return true;
    }


    // ffmpeg -i V2.webm -ss 00:00:1.74 -t 10 -r 25 VV2.mp4
    public boolean cutVideo(String exePath, String inputFile, float startTime, float stopTime, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-i");
        command.add(inputFile);
        command.add("-ss");
        command.add(startTime + "");
        command.add("-t");
        command.add(stopTime + "");
        command.add("-r");
        command.add("25");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

    public boolean cutAudio(String exePath, String inputFile, float startTime, float stopTime, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-i");
        command.add(inputFile);
        command.add("-ss");
        command.add(startTime + "");
        command.add("-t");
        command.add(stopTime + "");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }
    
    

    // ffmpeg -i aaa.wav -i bbb.wav -i out.wav -filter_complex concat=n=3:v=0:a=1 -f wav -vn -y
    // output.wav
    public boolean concatMultipleAudios(String exePath, String inputFile, int number, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        
        for (int i = 0; i < number; i++) {
            command.add("-i");
            command.add(inputFile+i+".wav");
        }
        command.add("-filter_complex");
        command.add("concat=n="+number+":v=0:a=1");
        command.add("-f");
        command.add("wav");
        command.add("-vn");
        command.add("-y");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }
    
    // ffmpeg -i aaa.wav -i bbb.wav -i out.wav -filter_complex concat=n=3:v=0:a=1 -f wav -vn -y
    // output.wav
    public boolean concatMultipleVideos(String exePath, String inputFile, int number, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        
        for (int i = 0; i < number; i++) {
            command.add("-i");
            command.add(inputFile+i+".mp4");
        }
        
        String str = "";
        for (int i = 0; i < number; i++) {
            if(i==0){
                str += "["+i+":v:0] " + " ["+i+":a:0] ";
            }else{
                str += " ["+i+":v:0] " + " ["+i+":a:0] ";
            }
        }
        
        command.add("-filter_complex");
        command.add("\""+str+" concat=n="+number+":v=1:a=1 [v] [a]\"");
        
        command.add("-map");
        command.add("[v]");
        command.add("-map");
        command.add("[a]");
        
        command.add("-f");
        command.add("mp4");
//        command.add("-vn");
        command.add("-y");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }
    
    
    

    // ffmpeg -i aaa.wav -i bbb.wav -i out.wav -filter_complex concat=n=3:v=0:a=1 -f wav -vn -y
    // output.wav
    public boolean concatTwoAudios(String exePath, String inputFile1, String inputFile2, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-i");
        command.add(inputFile1);
        command.add("-i");
        command.add(inputFile2);

        command.add("-filter_complex");
        command.add("concat=n=2:v=0:a=1");
        command.add("-f");
        command.add("wav");
        command.add("-vn");
        command.add("-y");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }



    // ffmpeg -f lavfi -I anullsrc=r=16000:cl=mono -t 1.5 -q:a 9 acodec pcm_s16le out.wav
    public boolean createSilentWav(String exePath, double length, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-f");
        command.add("lavfi");
        command.add("-i");
        command.add("anullsrc=r=16000:cl=mono");
        command.add("-t");
        command.add(length + "");
        command.add("-q:a");
        command.add("9");
        command.add("-acodec");
        command.add("pcm_s16le");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }


    // ffmpeg -i input1.mp3 -i input2.mp3 -filter_complex amerge -c:a libmp3lame -q:a 4 output.mp3
    public boolean mergeTwoAudios(String exePath, String inputFile1, String inputFile2, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-i");
        command.add(inputFile1);
        command.add("-i");
        command.add(inputFile2);
        command.add("-filter_complex");
        // command.add("amerge");
        // command.add("-c:a");
        // command.add("libmp3lame");
        // command.add("-q:a");
        // command.add("4");
        command.add("amix=inputs=2:duration=shortest");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

    // ffmpeg -vn -i aaa.mp4 -acodec pcm_s16le -ar 16000 -ac 1 aaa.wav
    public boolean extractAudio(String exePath, String inputFile, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-vn");
        command.add("-i");
        command.add(inputFile);
        command.add("-async");
        command.add("1");
        command.add("-acodec");
        command.add("pcm_s16le");
        command.add("-ar");
        command.add("16000");
        command.add("-ac");
        command.add("1");

        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }
    
 // ffmpeg -vn -i aaa.mp4 -acodec pcm_s16le -ar 16000 -ac 1 aaa.wav
    public boolean extractAudioSpecial(String exePath, String inputFile, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-acodec");
        command.add("libopus");
        command.add("-vn");
        command.add("-i");
        command.add(inputFile);
        command.add("-async");
        command.add("1");
        command.add(outputFile);
        
        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }


    /*
     * ffmpeg -i video_1444204793646_doctor.mp4 -vf
     * "[in] scale=iw/2:ih/2, pad=2*iw:ih [left];movie=video_1444204793646_patient.mp4, scale=iw/2:ih/2 [right];[left][right] overlay=main_w/2:0 [out]"
     * -b:v 768k video_1444204793646.mp4
     */
    public boolean mergeTwoVideo(String exePath, String inputFile1, String inputFile2, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-i");
        command.add(inputFile1);
        command.add("-vf");

        String temp =
                        "\"[in] scale=iw/2:ih/2, pad=2*iw:2*ih:0:120 [left];movie=" + inputFile2
                                        + ", scale=iw/2:ih/2 [right];[left][right] overlay=main_w/2:120 [out]\"";

        command.add(temp);
        command.add("-b:v");
        command.add("768k");
        command.add("-an");

        // String temp =
        // "\"[in] scale=iw/2:ih/2, pad=2*iw:2*ih:0:120 [left];movie=" + inputFile2
        // + ", scale=iw/2:ih/2 [right];[left][right] overlay=main_w/2:120 [out]\"";
        // // String temp =
        // //
        // "\"[in] scale=320:240, pad=640:240 [left];movie="+inputFile2+", scale=320:240 [right];[left][right] overlay=320:0 [out]\"";
        // command.add(temp);
        // command.add("-an");
        // command.add("-vcodec");
        // command.add("libx264");
        // command.add("-profile:v");
        // command.add("high");
        // command.add("-level:v");
        // command.add("3.0");
        // command.add("-r");
        // command.add("25");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }

    }
    
    
    /*
     * ffmpeg -i video_1444204793646_doctor.mp4 -vf
     * "[in] scale=iw/2:ih/2, pad=2*iw:ih [left];movie=video_1444204793646_patient.mp4, scale=iw/2:ih/2 [right];[left][right] overlay=main_w/2:0 [out]"
     * -b:v 768k video_1444204793646.mp4
     */
    public boolean mergeTwoVideoAudio(String exePath, String inputFile1, String inputFile2, String audioFile, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-i");
        command.add(audioFile);
        command.add("-i");
        command.add(inputFile1);
        command.add("-vf");

        String temp =
                        "\"[in] scale=iw/2:ih/2, pad=2*iw:2*ih:0:120 [left];movie=" + inputFile2
                                        + ", scale=iw/2:ih/2 [right];[left][right] overlay=main_w/2:120 [out]\"";

        command.add(temp);
        command.add("-b:v");
        command.add("768k");
        // command.add("-an");
        command.add("-map");
        command.add("0:a:0");
        command.add("-map");
        command.add("1:v:0");
        
        // String temp =
        // "\"[in] scale=iw/2:ih/2, pad=2*iw:2*ih:0:120 [left];movie=" + inputFile2
        // + ", scale=iw/2:ih/2 [right];[left][right] overlay=main_w/2:120 [out]\"";
        // // String temp =
        // //
        // "\"[in] scale=320:240, pad=640:240 [left];movie="+inputFile2+", scale=320:240 [right];[left][right] overlay=320:0 [out]\"";
        // command.add(temp);
        // command.add("-an");
        // command.add("-vcodec");
        // command.add("libx264");
        // command.add("-profile:v");
        // command.add("high");
        // command.add("-level:v");
        // command.add("3.0");
        // command.add("-r");
        // command.add("25");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }

    }


    /*
     * ffmpeg -i V2.webm -ss 00:00:1.74 -r 25 VV2.mp4
     */
    public boolean webm2mp4(String exePath, String inputFile, String outputFile, float trimmedLength) {

        System.out.println(inputFile + ": " + trimmedLength);

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-acodec");
        command.add("libopus");
        command.add("-i");
        command.add(inputFile);
        command.add("-ss");
        if (trimmedLength < 10) {
            command.add("00:00:0" + trimmedLength);
        } else {
            
            if(trimmedLength>=60){
                float small = trimmedLength - (int)trimmedLength;
                
                int min = (int)trimmedLength/60;
                float second = (int)trimmedLength%60+small;
                
                if(second<10){
                    command.add("00:0"+min+":0" + second);
                }else{
                    command.add("00:0"+min+":" + second);
                }
                
                
            }else{
                command.add("00:00:" + trimmedLength);
            }
            
        }
        // command.add("-vcodec");
        // command.add("copy");
        command.add("-r");
        command.add("25");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }

    }
    
    
    /*
     * ffmpeg -i V2.webm -ss 00:00:1.74 -r 25 VV2.mp4
     */
    public boolean webm2mp4(String exePath, String inputFile, String outputFile, float trimmedLength, float t) {

        System.out.println(inputFile + ": " + trimmedLength);

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-acodec");
        command.add("libopus");
        command.add("-i");
        command.add(inputFile);
        command.add("-ss");
        command.add(trimmedLength+"");
        command.add("-t");
        command.add(t+"");
        command.add("-r");
        command.add("25");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }

    }
    


    /*
     * command: ffmpeg -i song.mp3 -acodec pcm_u8 -ar 22050 song.wav
     */
    public boolean mp32wav(String exePath, String inputFile, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-i");
        command.add(inputFile);
        command.add("-acodec");
        command.add("pcm_u8");
        command.add("-ar");
        command.add("44100");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }

    }

    // ffmpeg -y -i video_1444204793646.mp4 -i video_1444204793646.wav ooo.mp4
    public boolean mergeVideoAudio(String exePath, String videoInputFile, String audioInputFile, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-i");
        command.add(videoInputFile);
        command.add("-i");
        command.add(audioInputFile);

        // command.add("-vcodec");
        // command.add("copy");
        // command.add("-acodec");
        // command.add("copy");

        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

    /*
     * command: ffmpeg -i filename.flv -qscale 0 -ar 22050 -vcodec libx264 filename.mp4
     */
    public boolean processFLV2MP4(String exePath, String inputFile, String outputFile) {

        List<String> command = new ArrayList<String>();
        command.add(exePath);
        // overwrite
        command.add("-y");
        command.add("-i");
        command.add(inputFile);
        command.add("-q:v");
        command.add("0");
        command.add("-ar");
        command.add("22050");
        command.add("-vcodec");
        command.add("libx264");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

    public boolean process(String exePath, String inputFile, String outputFile) {
        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-y");
        command.add("-i");
        command.add(inputFile);
        command.add("-s");
        // command.add("368x208");
        command.add("640x480");

        command.add("-r");
        command.add("25");
        // command.add("-b");
        // command.add("1500");
        command.add("-b");
        command.add("2000k");
        command.add("-acodec");
        command.add("aac");

        // command.add("-vcodec");
        // command.add("h264");

        command.add("-ac");
        command.add("2");
        command.add("-ar");
        command.add("24000");
        command.add("-ab");
        command.add("128");

        command.add("-vol");
        command.add("200");
        command.add(outputFile);

        try {
            Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            new PrintStream(videoProcess.getErrorStream()).start();

            new PrintStream(videoProcess.getInputStream()).start();

            videoProcess.waitFor();

            return true;

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }


    public float getDurationVideo(String exePath, String inputFile) {
        List<String> command = new ArrayList<String>();
        command.add(exePath);
        command.add("-i");
        command.add(inputFile);

        try {
            // Process videoProcess = new ProcessBuilder(command).redirectErrorStream(true).start();

            Process videoProcess = new ProcessBuilder(command).start();

            InputStream lsOut = lsOut = videoProcess.getErrorStream();
            InputStreamReader isr = new InputStreamReader(lsOut);
            BufferedReader in = new BufferedReader(isr);

            // parsing .exe output to find info for duration:
            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
                if (line.contains("Duration:")) {
                    line = line.replaceFirst("Duration: ", "");
                    line = line.trim();
                    String res = line.substring(0, 11);

                    String[] parts = res.split(":");
                    int hrs = Integer.parseInt(parts[0]);
                    int mins = Integer.parseInt(parts[1]);
                    float secs = Float.parseFloat(parts[2]);
                    float duration = secs + mins * 60 + hrs * 3600;
                    return duration;
                }
            }

            videoProcess.waitFor();

            return 0;

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return 0;
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
    }



}


class PrintStream extends Thread {
    java.io.InputStream is = null;

    private static final Logger logger = Logger.getLogger(PrintStream.class);

    public PrintStream(java.io.InputStream is) {
        this.is = is;
    }

    public void run() {
        try {
            while (this != null) {
                int ch = is.read();
                if (ch != -1) {
                    System.out.print((char) ch);
                } else {
                    break;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
