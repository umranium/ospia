package org.poscomp.eqclinic.processor;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Note;
import org.poscomp.eqclinic.domain.Video;
import org.poscomp.eqclinic.domain.pojo.RecordingFile;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.poscomp.eqclinic.service.interfaces.NoteService;
import org.poscomp.eqclinic.service.interfaces.VideoService;
import org.poscomp.eqclinic.util.ConstantValue;
import org.poscomp.eqclinic.util.JsonTool;
import org.poscomp.eqclinic.util.ParserSmile;
import org.poscomp.eqclinic.util.StringTool;
import org.poscomp.eqclinic.util.ZipTool;

import com.opentok.Archive;
import com.opentok.OpenTok;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class DownloadProcessor implements Runnable {

    private static final Logger logger = Logger.getLogger(DownloadProcessor.class);

    private static Archive processingArchive;
    private VideoService videoService;
    private AppointmentService appointmentService;
    private NoteService noteService;
    private String sessionId;
    private static Archive archiveDoctor;
    private static Archive archivePatient;
    private static Archive archiveDoctorBaseline;
    private static Archive archivePatientBaseline;
    public static final int FPS = 25;
    public static final int THRESHOLD = 25000;
    public static String projectName = ConstantValue.projectName;

    private String archiveId;
    private String patientArchiveIdBaseline;
    private String doctorArchiveIdBaseline;

    public DownloadProcessor(String archiveId, String patientArchiveIdBaseline, String doctorArchiveIdBaseline,
                    VideoService videoService, AppointmentService appointmentService, NoteService noteService,
                    String sessionId) {
        this.archiveId = archiveId;
        this.patientArchiveIdBaseline = patientArchiveIdBaseline;
        this.doctorArchiveIdBaseline = doctorArchiveIdBaseline;
        this.videoService = videoService;
        this.appointmentService = appointmentService;
        this.noteService = noteService;
        this.sessionId = sessionId;
    }

    public static void main(String[] args) {}


    public void run() {

        String apiKey = "45493902";
        String apiSecret = "54d8c8370b3828f633154dc04810e3af4535646f";
        OpenTok opentok;
        opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);

        // String archiveId = "801c7738-6b34-4dbb-bef6-9068c44e2ae1";

        try {

            while (true) {
                processingArchive = opentok.getArchive(archiveId);

                if (processingArchive != null) {
                    String url = processingArchive.getUrl();
                    if (url != null && !"".equals(url)) {
                        int videoLength = processingArchive.getDuration();
                        Video video = videoService.getVideoByArchiveId(archiveId);
                        video.setLength(videoLength);
                        video.setAllowTutorNotes(0);
                        videoService.saveVideo(video);
                        break;
                    } else {
                        Thread.sleep(5000);
                    }
                }
            }

            // below, we download the file, and process the file
            String fileName = processingArchive.getName();

            String saveDir = "../webapps/" + projectName + "/video/" + fileName + "/";

            if (!saveDir.endsWith(File.separator)) {
                saveDir = saveDir + File.separator;
            }
            File dir = new File(saveDir);
            if (!dir.exists()) {
                if (dir.mkdirs()) {
                    // do nothing
                }
            }

            String fileExt = ".mp4";
            // check whether the video already download
            File downFile = new File(saveDir + "/" + fileName + fileExt);
            ArrayList<RecordingFile> files = new ArrayList<RecordingFile>();

            if (!downFile.exists()) {

                String url = processingArchive.getUrl();

                // get doctor's audio file name
                String audioFileBaseDirStr = "../webapps/" + projectName + "/video/" + fileName + "/";
                int result = 0;
                int resultPatientBaseline = 0;
                int resultDoctorBaseline = 0;

                if (url == null || "".equals(url)) {
                    result = 1;
                    resultPatientBaseline = 1;
                    resultDoctorBaseline = 1;
                } else {
                    String zipFileExt = ".zip";
                    result = HttpDownloadUtility.downloadFile(fileName + zipFileExt, url, saveDir);
                    // unzip the file
                    ZipTool.unZipFiles(saveDir + fileName + zipFileExt, saveDir);
                    String jsonFileName = JsonTool.getJsonFileName(saveDir);

                    files = JsonTool.loadJsonObject(saveDir + jsonFileName);

                }
//                if (result == 1) {
//                    // start processing
//                    System.out.println("start processing....");
//
//                    Cmd cmd = new Cmd();
//                    String exePath = "c:\\videos\\ffmpeg.exe";
//
//                    if (files.size() > 2) {
//
//                        List<RecordingFile> filesDoctor = new ArrayList<RecordingFile>();
//                        List<RecordingFile> filesPatient = new ArrayList<RecordingFile>();
//
//                        for (RecordingFile tempFile : files) {
//                            if ("patient".equals(tempFile.getUserType())) {
//                                filesPatient.add(tempFile);
//                            } else if ("doctor".equals(tempFile.getUserType())) {
//                                filesDoctor.add(tempFile);
//                            }
//                        }
//
//                        Collections.sort(filesDoctor);
//                        Collections.sort(filesPatient);
//
//                        List<RecordingFile> filesDoctorTrimmed = new ArrayList<RecordingFile>();
//                        List<RecordingFile> filesPatientTrimmed = new ArrayList<RecordingFile>();
//
//                        RecordingFile rf = new RecordingFile();
//
//                        for (RecordingFile fileDoctor : filesDoctor) {
//                            for (RecordingFile filePatient : filesPatient) {
//                                RecordingFile tempResult = rf.getOverlapping(fileDoctor, filePatient);
//                                if (tempResult != null) {
//                                    filesDoctorTrimmed.add(tempResult);
//                                }
//                            }
//                        }
//
//
//                        for (RecordingFile filePatient : filesPatient) {
//                            for (RecordingFile fileDoctor : filesDoctor) {
//                                RecordingFile tempResult = rf.getOverlapping(filePatient, fileDoctor);
//                                if (tempResult != null) {
//                                    filesPatientTrimmed.add(tempResult);
//                                }
//                            }
//                        }
//
//                        
//                        List<Integer> gaps = new ArrayList<Integer>();
//                        
//                        for (int i = 0; i < filesPatientTrimmed.size(); i++) {
//                            if(i==0){
//                                gaps.add(filesPatientTrimmed.get(i).getStartTimeOffset());
//                            }else{
//                                int diff = filesPatientTrimmed.get(0).getStartTimeOffset();
//                                for (int j = 1; j <= i; j++){
//                                    diff += filesPatientTrimmed.get(j).getStartTimeOffset()-filesPatientTrimmed.get(j-1).getStopTimeOffset();
//                                }
//                                gaps.add(diff);
//                            }
//                        }
//
//                        // access the comments
//                        Video conbinedVideo = videoService.getVideo(fileName);
//                        String conbinedSesseionId = conbinedVideo.getSessionId();
//                        List<Note> notes = noteService.getNoteBySessionId(conbinedSesseionId);
//
//                        for (Note note : notes) {
//                            
//                            long noteTime = note.getRealTime();
//                            int number = -1;
//                            for (int i = 0; i < filesPatientTrimmed.size(); i++){
//                                RecordingFile trimmedFile = filesPatientTrimmed.get(i);
//                                
//                                if(noteTime>=(trimmedFile.getStartTimeOffset()+trimmedFile.getRecordingTime())&&
//                                        noteTime<=(trimmedFile.getStopTimeOffset()+trimmedFile.getRecordingTime())){
//                                    number = i;
//                                }
//                            }
//                            float total = gaps.get(number);
//                            double time = note.getRealTime()-filesPatientTrimmed.get(0).getRecordingTime()-total;
//                            if(time>0){
//                                note.setTime(time/1000.0);
//                            }
//                            noteService.saveNote(note);
//                        }
//                        
//                        String finalFileNamePatient = "";
//                        String finalFileNameDoctor = "";
//                        for (int i = 0; i < filesDoctorTrimmed.size(); i++) {
//
//                            RecordingFile file = filesDoctorTrimmed.get(i);
//
//                            finalFileNameDoctor = file.getFileName();
//                            
//                            String inputFile = saveDir + file.getOpentokName() + ".webm";
//
//                            String outputFile = saveDir + file.getUserType() + i + ".mp4";
//
//                            // covert the webm to mp4
//                            cmd.webm2mp4(exePath, inputFile, outputFile, file.getSs(), file.getT());
//                            float videoLength = cmd.getDurationVideo(exePath, saveDir + file.getUserType() + i + ".mp4");
//
//                            // extract WAV file
//                            cmd.extractAudio(exePath, outputFile, saveDir + file.getUserType() + i + "_ori.wav");
//                            float audioLength = cmd.getDurationVideo(exePath, saveDir + file.getUserType() + i + "_ori.wav");
//
//                            if (audioLength < videoLength) {
//                                double difference = videoLength - audioLength;
//                                cmd.createSilentWav(exePath, difference, saveDir + file.getUserType() + i + "_silent.wav");
//
//                                cmd.concatTwoAudios(exePath, saveDir + file.getUserType() + i + "_silent.wav",
//                                                saveDir + file.getUserType() + i + "_ori.wav", saveDir + file.getUserType() + i
//                                                                + ".wav");
//                            } else {
//                                double difference = 0.001;
//                                cmd.createSilentWav(exePath, difference, saveDir + file.getUserType() + i + "_silent.wav");
//                                cmd.concatTwoAudios(exePath, saveDir + file.getUserType() + i + "_silent.wav",
//                                                saveDir + file.getUserType() + i + "_ori.wav", saveDir + file.getUserType() + i
//                                                                + ".wav");
//                            }
//
//                            new File(saveDir + file.getUserType() + i + "_ori.wav").delete();
//                            new File(saveDir + file.getUserType() + i + "_silent.wav").delete();
//                        }
//
//
//
//                        for (int i = 0; i < filesPatientTrimmed.size(); i++) {
//
//                            RecordingFile file = filesPatientTrimmed.get(i);
//
//                            finalFileNamePatient = file.getFileName();
//                            
//                            String inputFile = saveDir + file.getOpentokName() + ".webm";
//
//                            String outputFile = saveDir + file.getUserType() + i + ".mp4";
//
//                            // covert the webm to mp4
//                            cmd.webm2mp4(exePath, inputFile, outputFile, file.getSs(), file.getT());
//                            float videoLength = cmd.getDurationVideo(exePath, saveDir + file.getUserType() + i + ".mp4");
//
//                            // extract WAV file
//                            cmd.extractAudio(exePath, outputFile, saveDir + file.getUserType() + i + "_ori.wav");
//                            float audioLength = cmd.getDurationVideo(exePath, saveDir + file.getUserType() + i + "_ori.wav");
//
//                            if (audioLength < videoLength) {
//                                double difference = videoLength - audioLength;
//                                cmd.createSilentWav(exePath, difference, saveDir + file.getUserType() + i + "_silent.wav");
//
//                                cmd.concatTwoAudios(exePath, saveDir + file.getUserType() + i + "_silent.wav",
//                                                saveDir + file.getUserType() + i + "_ori.wav", saveDir + file.getUserType() + i
//                                                                + ".wav");
//                            } else {
//                                double difference = 0.001;
//                                cmd.createSilentWav(exePath, difference, saveDir + file.getUserType() + i + "_silent.wav");
//                                cmd.concatTwoAudios(exePath, saveDir + file.getUserType() + i + "_silent.wav",
//                                                saveDir + file.getUserType() + i + "_ori.wav", saveDir + file.getUserType() + i
//                                                                + ".wav");
//                            }
//
//                            new File(saveDir + file.getUserType() + i + "_ori.wav").delete();
//                            new File(saveDir + file.getUserType() + i + "_silent.wav").delete();
//                        }
//
//
//
//                        cmd.concatMultipleAudios(exePath, saveDir + "doctor", filesDoctorTrimmed.size(), saveDir + finalFileNameDoctor+".wav");
//                        cmd.concatMultipleVideos(exePath, saveDir + "doctor", filesDoctorTrimmed.size(), saveDir + finalFileNameDoctor+".mp4");
//                        cmd.concatMultipleAudios(exePath, saveDir + "patient", filesPatientTrimmed.size(), saveDir + finalFileNamePatient+".wav");
//                        cmd.concatMultipleVideos(exePath, saveDir + "patient", filesPatientTrimmed.size(), saveDir + finalFileNamePatient+".mp4");
//
//                        for (int i = 0; i < filesPatientTrimmed.size(); i++) {
//                            RecordingFile file = filesPatientTrimmed.get(i);
//                            new File(saveDir + file.getUserType() + i + ".wav").delete();
//                            new File(saveDir + file.getUserType() + i + ".mp4").delete();
//                        }
//                        
//                        for (int i = 0; i < filesDoctorTrimmed.size(); i++) {
//                            RecordingFile file = filesDoctorTrimmed.get(i);
//                            new File(saveDir + file.getUserType() + i + ".wav").delete();
//                            new File(saveDir + file.getUserType() + i + ".mp4").delete();
//                        }
//                        
//                        
//                        String video1Name = saveDir + finalFileNameDoctor + ".mp4";
//                        String video2Name = saveDir + finalFileNamePatient + ".mp4";
//                        String combinedVideoName = saveDir + fileName + ".mp4";
//
//                        // combine two video
//                        cmd.mergeTwoVideo(exePath, video1Name, video2Name, combinedVideoName);
//
//                        String audio1Name = saveDir + finalFileNameDoctor + ".wav";
//                        String audio2Name = saveDir + finalFileNamePatient + ".wav";
//                        String combinedAudioName = saveDir + fileName + ".wav";
//                        // combine two audio
//                        cmd.mergeTwoAudios(exePath, audio1Name, audio2Name, combinedAudioName);
//
//
//                        // merge the video and audio
//                        String combinedName = saveDir + fileName + "_combined.mp4";
//                        cmd.mergeVideoAudio(exePath, combinedVideoName, combinedAudioName, combinedName);
//
//                        // if the combined file already created
//                        if (new File(combinedName).exists()) {
//                            // delete files
//                            new File(combinedAudioName).delete();
//                            new File(combinedVideoName).delete();
//
//                            File oldCombinedFile = new File(combinedName);
//                            File newCombinedFile = new File(combinedVideoName);
//
//                            oldCombinedFile.renameTo(newCombinedFile);
//                        }
//
//
//
//                    } else {
//
//                        RecordingFile file_doctor = new RecordingFile();
//                        RecordingFile file_patient = new RecordingFile();
//
//                        int startRecOffset = 0;
//                        int startRecOffset1 = files.get(0).getStartTimeOffset();
//                        int startRecOffset2 = files.get(1).getStartTimeOffset();
//                        if (startRecOffset1 >= startRecOffset2) {
//                            startRecOffset = startRecOffset1;
//                        } else {
//                            startRecOffset = startRecOffset2;
//                        }
//
//                        // convert webm files to mp4 files
//                        for (RecordingFile recordingFile : files) {
//                            String fileNameWebm = recordingFile.getFileName();
//                            String opentokName = recordingFile.getOpentokName();
//                            float trimmedLength = recordingFile.getTrimmedLength() / 1000;
//
//                            if (recordingFile.getUserType().equals("doctor")) {
//                                file_doctor = recordingFile;
//                            } else {
//                                file_patient = recordingFile;
//                            }
//
//                            // extract sound from doctor mp4 file
//                            String inputFile = saveDir + opentokName + ".webm";
//                            String outFile = saveDir + fileNameWebm + ".mp4";
//
//                            if (trimmedLength == 0) {
//
//                                long startRecordingTime = recordingFile.getRecordingTime() + startRecOffset;
//
//                                Appointment apt = appointmentService.getAppointmentBySessionId(sessionId);
//                                System.out.println(apt.getAptid());
//                                apt.setStartRecTime(startRecordingTime);
//                                appointmentService.saveApp(apt);
//
//                                // update all the notes and recording time
//                                List<Note> notes = noteService.getNoteBySessionId(sessionId);
//                                for (Note note : notes) {
//                                    note.setStartTime(startRecordingTime);
//                                    if (note.getRealTime() != null && note.getRealTime() != 0) {
//                                        double time = (note.getRealTime() - startRecordingTime) / 1000.0;
//                                        if(time>0){
//                                            note.setTime(time);
//                                        }
//                                    }
//                                    noteService.saveNote(note);
//                                }
//                            }
//
//                            // trimmedLength += 1;
//
//                            // here we need to trim the mp4 file, based on the offsite length
//                            cmd.webm2mp4(exePath, inputFile, outFile, trimmedLength);
//
//                            float videoLength = cmd.getDurationVideo(exePath, saveDir + fileNameWebm + ".mp4");
//
//                            // extract WAV file
//                            cmd.extractAudio(exePath, outFile, saveDir + fileNameWebm + ".wav");
//
//                            // File wavFile = new File(saveDir + fileNameWebm + "_ori.wav");
//                            // AudioInputStream audioInputStream =
//                            // AudioSystem.getAudioInputStream(wavFile);
//                            // AudioFormat format = audioInputStream.getFormat();
//                            // long frames = audioInputStream.getFrameLength();
//                            // double audioLength = (frames+0.0)/format.getFrameRate();
//                            float audioLength = cmd.getDurationVideo(exePath, saveDir + fileNameWebm + ".wav");
//
////                            // System.out.println("video length : " + videoLength);
////                            // System.out.println("audio length : " + audioLength);
////
////                            if (audioLength < videoLength) {
////                                double difference = videoLength - audioLength;
////                                cmd.createSilentWav(exePath, difference, saveDir + fileNameWebm + "_silent.wav");
////                                cmd.concatTwoAudios(exePath, saveDir + fileNameWebm + "_silent.wav", saveDir
////                                                + fileNameWebm + "_ori.wav", saveDir + fileNameWebm + ".wav");
////                            } else {
////                                double difference = 0.001;
////                                cmd.createSilentWav(exePath, difference, saveDir + fileNameWebm + "_silent.wav");
////                                cmd.concatTwoAudios(exePath, saveDir + fileNameWebm + "_silent.wav", saveDir
////                                                + fileNameWebm + "_ori.wav", saveDir + fileNameWebm + ".wav");
////                            }
////
////                            new File(saveDir + fileNameWebm + "_ori.wav").delete();
////                            new File(saveDir + fileNameWebm + "_silent.wav").delete();
//                        }
//
//                        String video1Name = saveDir + file_doctor.getFileName() + ".mp4";
//                        String video2Name = saveDir + file_patient.getFileName() + ".mp4";
//                        String combinedVideoName = saveDir + fileName + ".mp4";
//
//                        // combine two video
//                        cmd.mergeTwoVideo(exePath, video1Name, video2Name, combinedVideoName);
//
//                        String audio1Name = saveDir + file_doctor.getFileName() + ".wav";
//                        String audio2Name = saveDir + file_patient.getFileName() + ".wav";
//                        String combinedAudioName = saveDir + fileName + ".wav";
//                        // combine two audio
//                        cmd.mergeTwoAudios(exePath, audio1Name, audio2Name, combinedAudioName);
//
//
//                        // merge the video and audio
//                        String combinedName = saveDir + fileName + "_combined.mp4";
//                        cmd.mergeVideoAudio(exePath, combinedVideoName, combinedAudioName, combinedName);
//
//                        // if the combined file already created
//                        if (new File(combinedName).exists()) {
//                            // delete files
//                            new File(combinedAudioName).delete();
//                            new File(combinedVideoName).delete();
//
//                            File oldCombinedFile = new File(combinedName);
//                            File newCombinedFile = new File(combinedVideoName);
//
//                            oldCombinedFile.renameTo(newCombinedFile);
//                        }
//                    }
//
//                    // cmd.splitVideo(exePath, saveDir + fileName + "_doctor.mp4", 30);
//                }

            } else {
                // do nothing
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);

        }

    }
}
