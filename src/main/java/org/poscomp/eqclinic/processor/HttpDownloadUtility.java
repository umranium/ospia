package org.poscomp.eqclinic.processor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.util.ParserSmile;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class HttpDownloadUtility {


    private static final Logger logger = Logger.getLogger(HttpDownloadUtility.class);

    private static final int BUFFER_SIZE = 4096;

    public static int downloadFile(String fileName, String fileUrl, String saveDir) throws IOException {

        URL url = new URL(fileUrl);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        int responseCode = httpConn.getResponseCode();

        // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {

            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();
            String saveFilePath = saveDir + File.separator + fileName;

            // opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(saveFilePath);

            int bytesRead = -1;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();
            httpConn.disconnect();
            
            return 1;
        } else {
            httpConn.disconnect();
            System.out.println("No file to download. Server replied HTTP code: " + responseCode);
            return 0;
        }

    }

}
