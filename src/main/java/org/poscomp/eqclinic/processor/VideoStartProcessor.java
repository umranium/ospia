package org.poscomp.eqclinic.processor;

import java.util.concurrent.Callable;

import com.opentok.Archive;
import com.opentok.Archive.OutputMode;
import com.opentok.ArchiveMode;
import com.opentok.ArchiveProperties;
import com.opentok.OpenTok;
import com.opentok.exception.OpenTokException;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class VideoStartProcessor implements Callable {

    private String sessionId;
    private String archiveName;
    private OpenTok opentok;

    public VideoStartProcessor(OpenTok opentok, String sessionId, String archiveName) {
        this.opentok = opentok;
        this.sessionId = sessionId;
        this.archiveName = archiveName;
    }

    @Override
    public Archive call() throws Exception {
        try {
             
//            Archive archive = opentok.startArchive(sessionId, archiveName);

            Archive archive = opentok.startArchive(sessionId,  new ArchiveProperties.Builder()
                                                                .name(archiveName)
                                                                .hasAudio(true)
                                                                .hasVideo(true)
                                                                .outputMode(OutputMode.INDIVIDUAL)
                                                                .build());
            return archive;
        } catch (OpenTokException e) {
            System.out.println("Could not start an OpenTok Archive");
        }
        return null;
    }

}
