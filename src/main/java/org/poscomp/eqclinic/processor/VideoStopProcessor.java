package org.poscomp.eqclinic.processor;

import java.util.concurrent.Callable;

import com.opentok.Archive;
import com.opentok.OpenTok;
import com.opentok.exception.OpenTokException;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class VideoStopProcessor implements Callable {

    private String archiveId;
    private OpenTok opentok;

    public VideoStopProcessor(OpenTok opentok, String archiveId) {
        this.opentok = opentok;
        this.archiveId = archiveId;
    }

    @Override
    public Archive call() throws Exception {
        try {
            Archive archive = opentok.stopArchive(archiveId);
            return archive;
        } catch (OpenTokException e) {
            System.out.println("Could not stop an OpenTok Archive");
        }
        return null;
    }

}
