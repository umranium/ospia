package org.poscomp.eqclinic.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.ApplicationDao;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.service.interfaces.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "applicationService")
public class ApplicationServiceImpl implements ApplicationService {

    private static final Logger logger = Logger.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private ApplicationDao applicationDao;

    @Override
    public void saveApplication(Application app) {
        applicationDao.saveApplication(app);
    }

    @Override
    public Application findById(int id) {
        return applicationDao.findById(id);
    }

    @Override
    public List<Application> findByAptId(int aptId) {
        return applicationDao.findByAptId(aptId);
    }

    @Override
    public Application findByAptIdDorctorId(int aptId, int doctorId) {
        return applicationDao.findByAptIdDorctorId(aptId, doctorId);
    }

    @Override
    public List<Application> findByDoctorId(int doctorId) {
        return applicationDao.findByDoctorId(doctorId);
    }

    @Override
    public List<Application> findPendingByDoctorId(int doctorId) {
        return applicationDao.findPendingByDoctorId(doctorId);
    }


}
