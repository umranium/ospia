package org.poscomp.eqclinic.service;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.CancelledOperationDao;
import org.poscomp.eqclinic.domain.CancelledOperation;
import org.poscomp.eqclinic.service.interfaces.CancelledOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "cancelledOperationService")
public class CancelledOperationServiceImpl implements CancelledOperationService {

    private static final Logger logger = Logger.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private CancelledOperationDao cancelledOperationDao;

    @Override
    public void saveCancelledOperation(CancelledOperation operation) {
        cancelledOperationDao.saveCancelledOperation(operation);
    }

}
