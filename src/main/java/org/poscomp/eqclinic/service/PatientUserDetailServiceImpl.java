package org.poscomp.eqclinic.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.PatientDao;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.pojo.MyUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service
public class PatientUserDetailServiceImpl implements UserDetailsService {

    private static final Logger logger = Logger.getLogger(PatientUserDetailServiceImpl.class);

    @Autowired
    private PatientDao patientDao;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        MyUserPrincipal user = null;  
        try {  
            Patient patient = patientDao.getPatientByUsername(username);
            if(patient==null || patient.getStatus()!=1){
                return null;
            }
            List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(2);  
            authList.add(new GrantedAuthorityImpl("ROLE_PATIENT"));  
            user = new MyUserPrincipal(patient.getUsername(), patient.getPassword().toLowerCase(),authList, patient, null, null, null);  
        } catch (Exception e) {  
            e.printStackTrace();
            logger.error("Error in retrieving user");  
            throw new UsernameNotFoundException("Error in retrieving user");  
        }  
        return user;  
    }
    
}
