package org.poscomp.eqclinic.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.ScenarioDao;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;
import org.poscomp.eqclinic.service.interfaces.ScenarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "scenarioService")
public class ScenarioServiceImpl implements ScenarioService {

    private static final Logger logger = Logger.getLogger(ScenarioServiceImpl.class);

    @Autowired
    private ScenarioDao scenarioDao;

    @Override
    public List<Scenario> getAllScenario() {
        return scenarioDao.getAllScenario();
    }

    @Override
    public Scenario getScenarioById(int id) {
        return scenarioDao.getScenarioById(id);
    }

    @Override
    public List<Scenario> getScenarioBySearch(Scenario sce) {
        return scenarioDao.getScenarioBySearch(sce);
    }

    @Override
    public List<Scenario> getScenarioByPatient(Patient pat) {
        Scenario sce = new Scenario();
        if (pat.getGender() == 1) {
            sce.setGender("female");
        } else if (pat.getGender() == 2) {
            sce.setGender("male");
        }
        if(pat.getAge()>=50){
            sce.setAge(50);
        }else{
            sce.setAge(20);
        }
//        long birthday = pat.getBirthday();
//        double age = (System.currentTimeMillis() - birthday) / (365 * 24 * 60 * 60 * 1000L);
//        sce.setAge((int) age / 10 * 10);
        return scenarioDao.getScenarioBySearch(sce);
    }

    @Override
    public List<Scenario> getMultiScenarioByIds(String ids, Patient patient) {
        List<Scenario> sces = scenarioDao.getMultiScenarioByIds(ids);

        String trainedId = patient.getTrainedsceids();
        if (trainedId != null && !"".equals(trainedId)) {

            String[] trainedIdsStr = trainedId.split(",");
            ArrayList<Integer> trainedIds = new ArrayList<Integer>();

            for (String id : trainedIdsStr) {
                trainedIds.add(Integer.parseInt(id));
            }

            for (Scenario scenario : sces) {
                scenario.setTrained(0);
                int sceId = scenario.getId();

                for (Integer id : trainedIds) {
                    if (sceId == id) {
                        scenario.setTrained(1);
                    }
                }
            }
        }

        return sces;
    }

    @Override
    public List<Scenario> getScenarioByCode(String code) {
        return scenarioDao.getScenarioByCode(code);
    }

}
