package org.poscomp.eqclinic.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.StudentGroupDao;
import org.poscomp.eqclinic.domain.StudentGroup;
import org.poscomp.eqclinic.service.interfaces.StudentGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "studentGroupService")
public class StudentGroupServiceImpl implements StudentGroupService {

    private static final Logger logger = Logger.getLogger(StudentGroupServiceImpl.class);

    @Autowired
    private StudentGroupDao studentGroupDao;

    @Override
    public StudentGroup getStudentGroupByStudentId(String studentId) {
        return studentGroupDao.getStudentGroupByStudentId(studentId);
    }

	@Override
	public List<StudentGroup> getAllUnParticipatedStudent() {
		return studentGroupDao.getAllUnParticipatedStudent();
	}



}
