package org.poscomp.eqclinic.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.VideoDao;
import org.poscomp.eqclinic.domain.Video;
import org.poscomp.eqclinic.service.interfaces.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "videoService")
public class VideoServiceImpl implements VideoService {

    private static final Logger logger = Logger.getLogger(VideoServiceImpl.class);

    @Autowired
    private VideoDao videoDao;

    public VideoDao getVideoDao() {
        return videoDao;
    }

    public void setVideoDao(VideoDao videoDao) {
        this.videoDao = videoDao;
    }

    @Override
    public void saveVideo(Video video) {
        videoDao.saveVideo(video);
    }

    public Video getVideo(String videoName) {
        return videoDao.getVideo(videoName);
    }

    @Override
    public List<Video> getVideoByDoctorId(int doctorId) {
        return videoDao.getVideoByDoctorId(doctorId);
    }

    public Video getVideoByArchiveId(String archiveId) {
        return videoDao.getVideoByArchiveId(archiveId);
    }

    @Override
    public List<Object> getVideoByDoctorIdWithProgress(int doctorId) {
        return videoDao.getVideoByDoctorIdWithProgress(doctorId);
    }

    @Override
    public List<Object> getVideoByPatientIdWithProgress(int patientId) {
        return videoDao.getVideoByPatientIdWithProgress(patientId);
    }

    @Override
    public List<Video> getVideoBySessionId(String sessionId) {
       return videoDao.getVideoBySessionId(sessionId);
    }

    @Override
    public List<Video> getVideoByDate() {
        return videoDao.getVideoByDate();
    }

    @Override
    public Video getVideoById(int videoId) {
        return videoDao.getVideoById(videoId);
    }

    @Override
    public List<Video> getVideoByDate(String dateStr) {
        return videoDao.getVideoByDate(dateStr);
    }

    @Override
    public List<Video> getAllUnreviewedVideo() {
        return videoDao.getAllUnreviewedVideo();
    }

}
