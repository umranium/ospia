package org.poscomp.eqclinic.service.interfaces;

import org.poscomp.eqclinic.domain.AppointmentProgress;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface AppointmentProgressService {
    
    public void saveAppointmentProgress(AppointmentProgress appPro);

    public AppointmentProgress getAppointmentProgressById(int id);
    
    public AppointmentProgress getAppointmentProgressByAptIdUserType(int aptId, String userType);

    public AppointmentProgress getAppointmentProgressByAptIdUserId(int aptId, String userType, int userId);
    
    public int updateAppointmentProgressState(String sessionId, String userType, int userId, String step);

}
