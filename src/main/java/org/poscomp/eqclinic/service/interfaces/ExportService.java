package org.poscomp.eqclinic.service.interfaces;

import java.util.List;


/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface ExportService {

    public List<Object> exportSPRegistration(long timestamp);

    public List<Object> exportAptOffered(long timestamp);

    public List<Object> exportAptCompleted(long timestamp);

    public List<Object> exportAptFailed(long timestamp);

    public List<Object> exportAptCanceledStu(long timestamp);

    public List<Object> exportAptCanceledSP(long timestamp);

    public List<Object> exportAptCompletedSP(long timestamp);

    public List<Object> exportAptCompletedStu(long timestamp);

    public List<Object> exportAptImappropriate(long timestamp);

    public List<Object> exportAptConclusion(int status, long timestamp);
    
}
