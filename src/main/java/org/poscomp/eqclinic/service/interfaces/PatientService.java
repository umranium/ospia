package org.poscomp.eqclinic.service.interfaces;

import java.util.Date;
import java.util.List;

import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface PatientService {

    public Patient patientLogin(String username, String password);

    public Patient getPatientByPatientId(int patientid);
    
    public Patient getPatientByUsername(String username);
    
    public Patient getPatientByPhoneNo(String phoneNo);

    public void savePatient(Patient patient);

    public void createPatient(Patient patient, List<Scenario> scenarios);
    
    public List<Patient> getAllPatients();
    
    public List<Patient> searchPatient(String searchContent);
    
    public List<Patient> getPatientInPeriod(Long start, Long end);
    
}
