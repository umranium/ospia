package org.poscomp.eqclinic.service.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Scenario;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface ScenarioService {
    public List<Scenario> getAllScenario();

    public Scenario getScenarioById(int id);

    public List<Scenario> getScenarioByCode(String code);

    public List<Scenario> getScenarioBySearch(Scenario sce);

    public List<Scenario> getScenarioByPatient(Patient pat);

    public List<Scenario> getMultiScenarioByIds(String ids, Patient patient);
}
