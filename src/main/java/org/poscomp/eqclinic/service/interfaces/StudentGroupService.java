package org.poscomp.eqclinic.service.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.StudentGroup;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface StudentGroupService {
    public StudentGroup getStudentGroupByStudentId(String studentId);
    
    public List<StudentGroup> getAllUnParticipatedStudent();
}
