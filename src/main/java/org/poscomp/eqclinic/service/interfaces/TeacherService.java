package org.poscomp.eqclinic.service.interfaces;

import org.poscomp.eqclinic.domain.Teacher;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface TeacherService {
    public Teacher teacherLogin(String username, String password);

    public Teacher getTeacherByUsername(String username);

    public Teacher getTeacherByTeacherId(int teacherId);

    public void saveTeacher(Teacher teacher);
}
