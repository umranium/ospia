package org.poscomp.eqclinic.svm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.processor.DownloadProcessor;
import org.poscomp.eqclinic.util.Tools;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class FileParser {

    private static final Logger logger = Logger.getLogger(FileParser.class);
    
    public static void main(String[] args) {

        // 0-neutral 1-anger 2-contempt 3-disgust 4-fear 5-happy 6-sadness
        // 7-surprise
        FileParser pf = new FileParser();
        pf.parse("C:/resources/video_1423969560620_right_allpoints.txt",
                        "C:/resources/video_1423969560620_right_allfeatures.txt", 0);

        // pf.parse("asmlib/emo_neutral_lm", "emo_lm", 0);
        // pf.parse("asmlib/emo_anger_lm", "emo_lm", 1);
        // pf.parse("asmlib/emo_contempt_lm", "emo_lm", 2);
        // pf.parse("asmlib/emo_disgust_lm", "emo_lm", 3);
        // pf.parse("asmlib/emo_fear_lm", "emo_lm", 4);
        // pf.parse("asmlib/emo_happy_lm", "emo_lm", 5);
        // pf.parse("asmlib/emo_sadness_lm", "emo_lm", 6);
        // pf.parse("asmlib/emo_surprise_lm", "emo_lm", 7);

    }

    public void parse(String fileName, String writeFileName, int cat) {

        File file = new File(fileName);
        BufferedReader reader = null;
        BufferedWriter writer = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            writer = new BufferedWriter(new FileWriter(writeFileName, false));
            String line = null;
            // writer.write("f1,f2,f3,f4,f5,f6,f7,f8,c\r\n");

            while ((line = reader.readLine()) != null && !"".equals(line.trim())) {
                String[] linestr = line.split(",");
                float[] lineFloat = new float[linestr.length];
                for (int i = 0; i < linestr.length; i++) {
                    lineFloat[i] = Float.parseFloat(linestr[i]);
                }

                // String newLine = cat+"";
                // for (int i = 0; i < lineFloat.length; i++) {
                // newLine += " "+(i+1)+":"+lineFloat[i];
                // }
                // writer.write(newLine+"\r\n");

                // feature 1 48,54
                float f1 = Tools.getDistance(lineFloat, 48, 54);
                // feature 1 48,29
                float f2 = Tools.getDistance(lineFloat, 48, 29);
                // feature 1 48,54
                float f3 = Tools.getDistance(lineFloat, 54, 34);
                // feature 1 58,51
                float f4 = Tools.getDistance(lineFloat, 58, 51);
                // feature 1 58,41
                float f5 = Tools.getDistance(lineFloat, 58, 41);
                // feature 1 51,41
                float f6 = Tools.getDistance(lineFloat, 51, 41);
                // feature 1 24,18
                float f7 = Tools.getDistance(lineFloat, 24, 18);
                // feature 1 28,30
                float f8 = Tools.getDistance(lineFloat, 28, 30);
                // feature 1 33,35
                float f9 = Tools.getDistance(lineFloat, 33, 35);
                // feature 1 61,64
                float f10 = Tools.getDistance(lineFloat, 61, 64);

                String newLine =
                                cat + " 1:" + f1 + " 2:" + f2 + " 3:" + f3 + " 4:" + f4 + " 5:" + f5 + " 6:" + f6
                                                + " 7:" + f7 + " 8:" + f8 + " 9:" + f9 + " 10:" + f10;
                writer.write(newLine + "\r\n");
                // System.out.println(newLine);
            }
            reader.close();
            writer.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }

    }

    public void parseToWekaStyle(String fileName, String writeFileName, int cat) {

        File file = new File(fileName);
        BufferedReader reader = null;
        BufferedWriter writer = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            writer = new BufferedWriter(new FileWriter(writeFileName, false));
            String line = null;
            writer.write("@relation naturalclass\r\n");
            writer.write("\r\n");

            for (int i = 1; i <= 10; i++) {
                writer.write("@attribute a" + i + " numeric\r\n");
            }
            writer.write("@attribute natural {0,1}\r\n");

            writer.write("\r\n");
            writer.write("@data\r\n");
            writer.write("\r\n");

            while ((line = reader.readLine()) != null && !"".equals(line.trim())) {
                String[] linestr = line.split(",");
                float[] lineFloat = new float[linestr.length];
                for (int i = 0; i < linestr.length; i++) {
                    lineFloat[i] = Float.parseFloat(linestr[i]);
                }

                // String newLine = cat+"";
                // for (int i = 0; i < lineFloat.length; i++) {
                // newLine += " "+(i+1)+":"+lineFloat[i];
                // }
                // writer.write(newLine+"\r\n");

                // feature 1 48,54
                float f1 = Tools.getDistance(lineFloat, 48, 54);
                // feature 1 48,29
                float f2 = Tools.getDistance(lineFloat, 48, 29);
                // feature 1 48,54
                float f3 = Tools.getDistance(lineFloat, 54, 34);
                // feature 1 58,51
                float f4 = Tools.getDistance(lineFloat, 58, 51);
                // feature 1 58,41
                float f5 = Tools.getDistance(lineFloat, 58, 41);
                // feature 1 51,41
                float f6 = Tools.getDistance(lineFloat, 51, 41);
                // feature 1 24,18
                float f7 = Tools.getDistance(lineFloat, 24, 18);
                // feature 1 28,30
                float f8 = Tools.getDistance(lineFloat, 28, 30);
                // feature 1 33,35
                float f9 = Tools.getDistance(lineFloat, 33, 35);
                // feature 1 61,64
                float f10 = Tools.getDistance(lineFloat, 61, 64);

                String newLine =
                                f1 + "," + f2 + "," + f3 + "," + f4 + "," + f5 + "," + f6 + "," + f7 + "," + f8 + ","
                                                + f9 + "," + f10 + "," + cat;
                writer.write(newLine + "\r\n");
            }
            reader.close();
            writer.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
    }

    public void parseToWekaStyle_NonNeu(String fileName, String writeFileName, int cat) {

        File file = new File(fileName);
        BufferedReader reader = null;
        BufferedWriter writer = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            writer = new BufferedWriter(new FileWriter(writeFileName, false));
            String line = null;
            writer.write("@relation naturalclass\r\n");
            writer.write("\r\n");

            for (int i = 1; i <= 10; i++) {
                writer.write("@attribute a" + i + " numeric\r\n");
            }
            writer.write("@attribute natural {1,2,3,4,5,6,7}\r\n");

            writer.write("\r\n");
            writer.write("@data\r\n");
            writer.write("\r\n");

            while ((line = reader.readLine()) != null && !"".equals(line.trim())) {
                String[] linestr = line.split(",");
                float[] lineFloat = new float[linestr.length];
                for (int i = 0; i < linestr.length; i++) {
                    lineFloat[i] = Float.parseFloat(linestr[i]);
                }

                // feature 1 48,54
                float f1 = Tools.getDistance(lineFloat, 48, 54);
                // feature 1 48,29
                float f2 = Tools.getDistance(lineFloat, 48, 29);
                // feature 1 48,54
                float f3 = Tools.getDistance(lineFloat, 54, 34);
                // feature 1 58,51
                float f4 = Tools.getDistance(lineFloat, 58, 51);
                // feature 1 58,41
                float f5 = Tools.getDistance(lineFloat, 58, 41);
                // feature 1 51,41
                float f6 = Tools.getDistance(lineFloat, 51, 41);
                // feature 1 24,18
                float f7 = Tools.getDistance(lineFloat, 24, 18);
                // feature 1 28,30
                float f8 = Tools.getDistance(lineFloat, 28, 30);
                // feature 1 33,35
                float f9 = Tools.getDistance(lineFloat, 33, 35);
                // feature 1 61,64
                float f10 = Tools.getDistance(lineFloat, 61, 64);

                String newLine =
                                f1 + "," + f2 + "," + f3 + "," + f4 + "," + f5 + "," + f6 + "," + f7 + "," + f8 + ","
                                                + f9 + "," + f10 + "," + cat;
                writer.write(newLine + "\r\n");
            }
            reader.close();
            writer.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
    }

    public void writeEmotions(String outputFile, ArrayList<ArrayList<Float>> emotions) {

        // 1-anger 2-contempt 3-disgust 4-fear 5-happy 6-sadness 7-surprise

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(outputFile, false));

            for (int i = 0; i < emotions.size(); i++) {

                String line = "";

                ArrayList<Float> emo = emotions.get(i);
                if (i == 0) {
                    line += "anger:";
                } else if (i == 1) {
                    line += "contempt:";
                } else if (i == 2) {
                    line += "disgust:";
                } else if (i == 3) {
                    line += "fear:";
                } else if (i == 4) {
                    line += "happy:";
                } else if (i == 5) {
                    line += "sadness:";
                } else if (i == 6) {
                    line += "surprise:";
                }
                for (int j = 0; j < emo.size(); j++) {
                    if (j == 0) {
                        line += emo.get(j);
                    } else {
                        line += "," + emo.get(j);
                    }
                }
                writer.write(line + "\r\n");
            }

            writer.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }

}
