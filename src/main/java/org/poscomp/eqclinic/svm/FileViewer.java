package org.poscomp.eqclinic.svm;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class FileViewer {

    public static void main(String[] args) {

        // 0-neutral 1-anger 2-contempt 3-disgust 4-fear 5-happy 6-sadness 7-surprise

        String path0 = "C:/Users/Chunfeng/Desktop/stasm4.1.0/data/emotions/0-neutral";
        String path1 = "C:/Users/Chunfeng/Desktop/stasm4.1.0/data/emotions/1-anger";
        String path2 = "C:/Users/Chunfeng/Desktop/stasm4.1.0/data/emotions/2-contempt";
        String path3 = "C:/Users/Chunfeng/Desktop/stasm4.1.0/data/emotions/3-disgust";
        String path4 = "C:/Users/Chunfeng/Desktop/stasm4.1.0/data/emotions/4-fear";
        String path5 = "C:/Users/Chunfeng/Desktop/stasm4.1.0/data/emotions/5-happy";
        String path6 = "C:/Users/Chunfeng/Desktop/stasm4.1.0/data/emotions/6-sadness";
        String path7 = "C:/Users/Chunfeng/Desktop/stasm4.1.0/data/emotions/7-surprise";

        List<String> paths = new ArrayList<String>();
        paths.add(path0);
        paths.add(path1);
        paths.add(path2);
        paths.add(path3);
        paths.add(path4);
        paths.add(path5);
        paths.add(path6);
        paths.add(path7);

        for (String path : paths) {
            String pathName[] = path.split("/");
            String filePath = pathName[6] + "/" + pathName[7] + "/";

            String suffix = "png";
            boolean isdepth = false;
            List<String> names = getListFiles(path, suffix, isdepth);

            String returnVal = "";
            for (String string : names) {
                returnVal += filePath + string + " ";
            }
            System.out.println(returnVal);
        }

    }

    public static List<String> getListFiles(String path, String suffix, boolean isdepth) {
        List<String> lstFileNames = new ArrayList<String>();
        File file = new File(path);
        return FileViewer.listFile(lstFileNames, file, suffix, isdepth);
    }

    private static List<String> listFile(List<String> lstFileNames, File f, String suffix, boolean isdepth) {
        if (f.isDirectory()) {
            File[] t = f.listFiles();
            if (t != null) {
                for (int i = 0; i < t.length; i++) {
                    if (isdepth || t[i].isFile()) {
                        listFile(lstFileNames, t[i], suffix, isdepth);
                    }
                }
            }
        } else {
            // String filePath = f.getAbsolutePath();
            String filePath = f.getName();
            if (!suffix.equals("")) {
                int begIndex = filePath.lastIndexOf(".");
                String tempsuffix = "";

                if (begIndex != -1) {
                    tempsuffix = filePath.substring(begIndex + 1, filePath.length());
                    if (tempsuffix.equals(suffix)) {
                        lstFileNames.add(filePath);
                    }
                }
            } else {
                lstFileNames.add(filePath);
            }
        }
        return lstFileNames;
    }
}
