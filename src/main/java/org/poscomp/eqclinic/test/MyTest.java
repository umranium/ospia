package org.poscomp.eqclinic.test;


/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class MyTest {

    public static void main(String[] args) throws Exception {
        
        Runtime rt = Runtime.getRuntime();
        
        System.out.println(rt.freeMemory()/1024/1024);
        
        System.out.println(rt.totalMemory()/1024/1024);
        
    }

}
