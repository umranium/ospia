package org.poscomp.eqclinic.test;

import heartrate.HeartRate;

import com.mathworks.toolbox.javabuilder.MWArray;
import com.mathworks.toolbox.javabuilder.MWException;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class TestHeartRate {

    public static void main(String[] args) {

        Object[] result = null;

        try {
            HeartRate hr = new HeartRate();
            result = hr.heartrate(1, "C:/Users/roclatte/Desktop/self_noglass_2min.txt;30;30;146");
            MWNumericArray temp = (MWNumericArray) result[0];
            float[][] weights = (float[][]) temp.toFloatArray();

            for (int i = 0; i < weights.length; i++) {

                for (int j = 0; j < weights[i].length; j++) {
                    System.out.print(weights[i][j] + "\t");
                }
                System.out.println();
            }

        } catch (MWException e) {
            e.printStackTrace();
        } finally {
            MWArray.disposeArray(result);
        }
    }

}
