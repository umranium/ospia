package org.poscomp.eqclinic.test;

import pitch.Pitch;

import com.mathworks.toolbox.javabuilder.MWArray;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class TestPitch {

    public static void main(String[] args) {

        Object[] result = null;
        Pitch pitch = null;

        try {
            pitch = new Pitch();
            result = pitch.pitch(2, "C:/videos/video_1457585459606_doctor.wav");

            MWNumericArray temp = (MWNumericArray) result[0];
            float[][] weights = (float[][]) temp.toFloatArray();

            for (int i = 0; i < weights[0].length; i++) {
                if (Float.isNaN(weights[0][i])) {
                    System.out.println(weights[0][i]);
                }
            }

            pitch.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            MWArray.disposeArray(result);
        }

    }

}
