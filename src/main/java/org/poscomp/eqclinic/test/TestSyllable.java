package org.poscomp.eqclinic.test;

import syllable.Syllable;

import com.mathworks.toolbox.javabuilder.MWArray;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class TestSyllable {

    public static void main(String[] args) {

        Object[] result = null;
        Syllable syllable = null;

        try {
            syllable = new Syllable();
            result = syllable.syllable(1, "C:/resources/aa.wav");

            MWNumericArray temp = (MWNumericArray) result[0];
            float[][] weights = (float[][]) temp.toFloatArray();

            System.out.println(weights.length);
            syllable.dispose();
        } catch (Exception e) {
            System.out.println("Exception: " + e.toString());
        } finally {
            MWArray.disposeArray(result);
        }

    }

}
