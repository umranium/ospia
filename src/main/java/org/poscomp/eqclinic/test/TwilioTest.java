package org.poscomp.eqclinic.test;

// You may want to be more specific in your imports
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class TwilioTest {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "ACe868067d3aeb0d297b88a9d9fef7b7c0";
    public static final String AUTH_TOKEN = "1d889b6ca039e7f415dc04111d04177b";

    public static void main(String[] args) {

        try {
            TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);

            // Build the parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("To", "+61416408826"));
            params.add(new BasicNameValuePair("From", "+61428185790"));
            params.add(new BasicNameValuePair("Body", "this is a test msg"));

            MessageFactory messageFactory = client.getAccount().getMessageFactory();
            Message message = messageFactory.create(params);
            System.out.println(message.getSid());
        } catch (TwilioRestException e) {
            System.out.println(e.getErrorMessage());
        }

    }
}
