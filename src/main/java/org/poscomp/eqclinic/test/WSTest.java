//package org.poscomp.eqclinic.test;
//
//import javax.xml.namespace.QName;
//
//import org.apache.axis2.AxisFault;
//import org.apache.axis2.addressing.EndpointReference;
//import org.apache.axis2.client.Options;
//import org.apache.axis2.rpc.client.RPCServiceClient;
//
//public class WSTest {
//    public void test() throws Exception {
//        String serviceUrl = "https://emed.med.unsw.edu.au/assessments.nsf/ws?WSDL";
//        RPCServiceClient serviceClient = null;
//        String resultString = "";
//        serviceClient = getServiceClient(serviceUrl);
//        String wsFunction = "helloWebService";
//        System.out.println(wsFunction);
//        String jsonString = "你好";
//        resultString = login(serviceUrl, serviceClient, jsonString, wsFunction);
//        System.out.println("resultString=" + resultString);
//    }
//
//    public RPCServiceClient getServiceClient(String wsdlUrl) {
//        RPCServiceClient serviceClient = null;
//        try {
//            serviceClient = new RPCServiceClient();
//            Options options = serviceClient.getOptions();
//            EndpointReference targetEPR = new EndpointReference(wsdlUrl);
//            options.setTo(targetEPR);
//        } catch (AxisFault e) {
//            e.printStackTrace();
//        }
//        return serviceClient;
//    }
//
//    public String login(String serviceUrl, RPCServiceClient serviceClient, String jsonString, String wsFunction)
//                    throws AxisFault {
//        QName opLogin = new QName("http://ws.apache.org/axis2", wsFunction);
//        Object[] inputArgs = new Object[] {};
//        if (jsonString != null && !"".equals(jsonString)) {
//            inputArgs = new Object[] {jsonString};
//        }
//
//
//        Class[] returnTypes = new Class[] {String.class};
//        Object[] response = serviceClient.invokeBlocking(opLogin, inputArgs, returnTypes);
//        String result = (String) response[0];
//        // display results
//        if (result == null) {
//            System.out.println("result is null!");
//        } else {
//            System.out.println(wsFunction + "               : " + result);
//        }
//        return result;
//    }
//
//}
