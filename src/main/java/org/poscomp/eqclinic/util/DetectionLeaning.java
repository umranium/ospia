package org.poscomp.eqclinic.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class DetectionLeaning {

    public void generateLeaningDurationFile(String inputFileName, String outputFileName) throws Exception {

        BufferedReader br = new BufferedReader(new FileReader(inputFileName));
        PrintWriter writer = new PrintWriter(outputFileName, "UTF-8");

        int startLineNo = 1;
        int preValue = 100;

        int currentLineNo = 1;
        int totalCount = 0;
        double totalValue = 0;
        try {
            String line = br.readLine();

            while (line != null) {

                if (!"0".equals(line)) {
                    String[] values = line.split(":");
                    int currentValue = Integer.parseInt(values[0]);
                    double realValue = Double.parseDouble(values[1]);
                    if (preValue == 100) {
                        startLineNo = currentLineNo;
                        preValue = currentValue;

                        if (currentValue != 0) {
                            totalValue += realValue;
                            totalCount++;
                        }

                    } else {
                        if (currentValue == 1 || currentValue == -1) {
                            if (currentValue == -preValue) {
                                writer.println(startLineNo + "," + (currentLineNo - 1) + ","
                                                + (currentLineNo - startLineNo) + ","
                                                + (currentLineNo - startLineNo) / 25f + "," + preValue + ","
                                                + totalValue / totalCount);
                                preValue = currentValue;
                                startLineNo = currentLineNo;
                                totalValue = 0;
                                totalCount = 0;
                                totalValue += realValue;
                                totalCount++;
                            } else {
                                totalValue += realValue;
                                totalCount++;
                            }
                        }
                    }
                }

                currentLineNo++;

                line = br.readLine();
            }

            writer.println(startLineNo + "," + (currentLineNo - 1) + "," + (currentLineNo - startLineNo) + ","
                            + (currentLineNo - startLineNo) / 25f + "," + preValue + "," + totalValue / totalCount);

        } finally {
            br.close();
            writer.close();
        }

    }

    public void generateLeaningFilethrehold(String inputFileName, String outputFileName, int fps, int threshold)
                    throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(inputFileName));
        PrintWriter writer = new PrintWriter(outputFileName, "UTF-8");

        try {
            String line = br.readLine();

            while (line != null) {
                String[] numbers = line.split(",");
                if (Integer.parseInt(numbers[4]) == 0) {
                    writer.println(0);
                } else {
                    if (Integer.parseInt(numbers[4]) - threshold > 0) {
                        writer.println(1 + ":" + (Integer.parseInt(numbers[4]) - threshold));
                    } else {
                        writer.println(-1 + ":" + (Integer.parseInt(numbers[4]) - threshold));
                    }
                }

                line = br.readLine();
            }

        } finally {
            writer.close();
            br.close();
        }
    }

    public void generateLeaningFile(String inputFileName, String outputFileName, int totalLines, int fps)
                    throws Exception {

        BufferedReader br = new BufferedReader(new FileReader(inputFileName));
        PrintWriter writer = new PrintWriter(outputFileName, "UTF-8");

        try {
            String line = br.readLine();
            int lineNo = 1;
            int startLine = 1;
            int stopLine = totalLines;

            while (line != null && lineNo <= stopLine) {

                if (lineNo >= startLine) {
                    // if this line has a zero, skip this line
                    String[] numbers = line.split("\\t");
                    boolean hasZero = false;
                    for (int i = 0; i < numbers.length; i++) {
                        if ("0".equals(numbers[i])) {
                            hasZero = true;
                        }
                    }

                    if (hasZero == false) {
                        writer.println(numbers[0] + "," + numbers[1] + "," + numbers[2] + "," + numbers[3] + ","
                                        + numbers[4]);
                    } else {
                        writer.println("0,0,0,0,0");
                    }
                }
                lineNo++;
                line = br.readLine();
            }

        } finally {
            writer.close();
            br.close();
        }

    }

    public int getFileLineNos(String inputFileName) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(inputFileName));
        int lineNo = 0;
        try {
            String line = br.readLine();
            while (line != null) {
                lineNo++;
                line = br.readLine();
            }

            br.close();
            return lineNo;

        } catch (Exception e) {
            return 0;
        } finally {
            br.close();
        }

    }

    public static final int FPS = 25;
    public static final int THRESHOLD = 14000;

    public static void main(String[] args) throws Exception {
        DetectionLeaning td = new DetectionLeaning();
        String catName = "chat4_doctor";
        int totalLineNo = td.getFileLineNos("files/" + catName + "/" + catName + ".txt");
        td.generateLeaningFile("files/" + catName + "/" + catName + ".txt",
                        "files/" + catName + "/" + catName + "_lean", totalLineNo, FPS);
        td.generateLeaningFilethrehold("files/" + catName + "/" + catName + "_lean",
                        "files/" + catName + "/" + catName + "_lean_threshold", FPS, THRESHOLD);
        td.generateLeaningDurationFile("files/" + catName + "/" + catName + "_lean_threshold",
                        "files/" + catName + "/" + catName + "_lean_threshold_duration");

        // for (int minute = 0; minute < totalMinutes; minute++) {
        // td.generateLeaningFile("files/"+catName+"/"+catName+".txt",
        // "files/"+catName+"/"+catName+"_lean_min" + minute, minute, FPS);
        // }

        // for (int minute = 0; minute < totalMinutes; minute++) {
        // td.generateLeaningFilethrehold("files/"+catName+"/"+catName+"_lean_min"
        // + minute,
        // "files/"+catName+"/"+catName+"_lean_threshold_min" + minute, FPS,
        // THRESHOLD);
        // }
        //
        // for (int minute = 0; minute < totalMinutes; minute++) {
        // td.generateLeaningDurationFile("files/"+catName+"/"+catName+"_lean_threshold_min"
        // + minute,
        // "files/"+catName+"/"+catName+"_lean_threshold_duration_min" +
        // minute);
        // }

    }

}
