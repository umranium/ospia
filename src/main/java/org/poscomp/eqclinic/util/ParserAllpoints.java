package org.poscomp.eqclinic.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.controller.AptController;

import com.sun.xml.internal.rngom.parse.Parseable;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class ParserAllpoints {

    private static final Logger logger = Logger.getLogger(ParserAllpoints.class);

    /*
     * this function is used for extracting the size of face
     */
    public void getFaceSizeFromAll(String allpointsFile, String faceSizeFile) {

        try {
            FileReader reader = new FileReader(new File(allpointsFile));
            BufferedReader br = new BufferedReader(reader);

            FileWriter writer = new FileWriter(new File(faceSizeFile));
            BufferedWriter bw = new BufferedWriter(writer);

            String line = br.readLine();
            while (line != null && !"".equals(line)) {

                String[] temp = line.split(",");
                if (temp.length != 68) {
                    bw.write("0");
                    bw.newLine();
                    line = br.readLine();
                } else {
                    float point1X = Float.parseFloat(temp[0]);
                    float point1Y = Float.parseFloat(temp[1]);
                    float point13X = Float.parseFloat(temp[8]);
                    float point13Y = Float.parseFloat(temp[9]);
                    
                    float point67X = Float.parseFloat(temp[24]);
                    float point67Y = Float.parseFloat(temp[25]);
                    float point7X = Float.parseFloat(temp[4]);
                    float point7Y = Float.parseFloat(temp[5]);

                    float horizontalDistance = Tools.getDistance(point1X, point1Y, point13X, point13Y);
                    float verticleDistance = Tools.getDistance(point67X, point67Y, point7X, point7Y);
                    float faceSize = horizontalDistance * verticleDistance;
                    bw.write(faceSize+"");
                    bw.newLine();
                    line = br.readLine();
                }
            }

            br.close();
            bw.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);;
        }

    }

    
    
    /*
     * this function is used for extracting the nose points from the video points file
     */
    public void getNosePointFromAll(String allpointsFile, String nosePoints) {

        try {
            FileReader reader = new FileReader(new File(allpointsFile));
            BufferedReader br = new BufferedReader(reader);

            FileWriter writer = new FileWriter(new File(nosePoints));
            BufferedWriter bw = new BufferedWriter(writer);

            String line = br.readLine();
            while (line != null && !"".equals(line)) {

                String[] temp = line.split(",");
                if (temp.length != 68) {
                    bw.write("0,0");
                    bw.newLine();
                    line = br.readLine();
                } else {
//                    float point29X = Float.parseFloat(temp[56]);
//                    float point29Y = Float.parseFloat(temp[57]);
//                    float point34X = Float.parseFloat(temp[66]);
//                    float point34Y = Float.parseFloat(temp[67]);

                    // String nosePoint = (point29X + point34X) / 2 + "," + (point29Y + point34Y) / 2;
                    String nosePoint = temp[24]+","+temp[25];
                    bw.write(nosePoint);
                    bw.newLine();
                    line = br.readLine();
                }
            }

            br.close();
            bw.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);;
        }

    }
    
    
    /*
     * this function is used for extracting the nose points from the video points file
     */
    public void getFaceMovement(String allpointsFile, String movementFile) {

        try {
            FileReader reader = new FileReader(new File(allpointsFile));
            BufferedReader br = new BufferedReader(reader);

            FileWriter writer = new FileWriter(new File(movementFile));
            BufferedWriter bw = new BufferedWriter(writer);

            String previousLine = br.readLine();
            int i=0;
            float totalPerSecond = 0;
            while ( previousLine!= null && !"".equals(previousLine)) {
                i++;
                
                String currentLine = br.readLine();
                if(currentLine!=null && !"".equals(currentLine)){
                    String[] prePoints = previousLine.split(",");
                    String[] curPoints = currentLine.split(",");
                    
                    float totalDistance = 0;
                    if(prePoints.length!=68 || curPoints.length!=68){
                        totalDistance = 0;
                    }else{
                        totalDistance = getTotalDistance(prePoints, curPoints, 34);
                    }
                    
                    if(i==25){
                        i=0;
                        totalPerSecond =  totalDistance;
                        bw.write(totalPerSecond+"");
                        bw.newLine();
                    }else{
                        totalPerSecond += totalDistance;
                    }
                    previousLine = currentLine;
                }else{
                    bw.write(totalPerSecond+"");
                    bw.newLine();
                    break;
                }
            }
            br.close();
            bw.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);;
        }

    }
    
    public int getTotalDistance(String[] prePoints, String[] curPoints, int pointNum){
        
        double totalDistance = 0;
        
        for (int i = 0; i < pointNum; i++) {
            
            float prePointX = Float.parseFloat(prePoints[i*2]);
            float prePointY = Float.parseFloat(prePoints[i*2+1]);
            
            float curPointX = Float.parseFloat(curPoints[i*2]);
            float curPointY = Float.parseFloat(curPoints[i*2+1]);
            
            totalDistance += Math.sqrt((prePointX-curPointX)*(prePointX-curPointX) + (prePointY-curPointY)*(prePointY-curPointY)); 
        }
        return (int)totalDistance;
    }
    
    public static void main(String[] args) {
        
        ParserAllpoints pa = new ParserAllpoints();
        pa.getFaceMovement("E:\\apache-tomcat-7.0.63\\webapps\\ospiausyd\\video\\video_1475449058370\\video_1475449058370_doctor_allpoints.txt",
                        "E:\\apache-tomcat-7.0.63\\webapps\\ospiausyd\\video\\video_1475449058370\\video_1475449058370_doctor_movement.txt");
        
        
    }
    

}
