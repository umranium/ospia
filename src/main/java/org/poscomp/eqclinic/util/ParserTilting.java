package org.poscomp.eqclinic.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class ParserTilting {

    public static final int FPS = 25;

    public static double CalulateXYAnagle(double startx, double starty, double endx, double endy) {

        double tan = Math.atan(Math.abs((endy - starty) / (endx - startx))) * 180 / Math.PI;
        if (endx > startx && endy > starty) {
            return -tan;
        } else if (endx > startx && endy < starty) {
            return tan;
        } else if (endx < startx && endy > starty) {
            return tan - 180;
        } else {
            return 180 - tan;
        }

    }

    /*
     * minute start from zero (0-59 seconds)
     */
    public void generateTiltingAngleFile(String inputFileName, String outputFileName, int totalLines, int fps)
                    throws Exception {

        BufferedReader br = new BufferedReader(new FileReader(inputFileName));
        PrintWriter writer = new PrintWriter(outputFileName, "UTF-8");
        PrintWriter writer1 = new PrintWriter(outputFileName + "_posnag", "UTF-8");

        try {
            String line = br.readLine();

            int lineNo = 1;

            int startLine = 1;
            int stopLine = totalLines;

            while (line != null && lineNo <= stopLine) {

                if (lineNo >= startLine) {
                    // if this line has a zero, skip this line
                    String[] numbers = line.split(",");
                    boolean hasZero = false;
                    for (int i = 0; i < numbers.length; i++) {
                        if ("0".equals(numbers[i])) {
                            hasZero = true;
                        }
                    }

                    if (hasZero == false && numbers.length>34) {
                        // calculate the middle point of two eyes
                        float leftEyeX = Float.parseFloat(numbers[34]);
                        float leftEyeY = Float.parseFloat(numbers[35]);
                        float rightEyeX = Float.parseFloat(numbers[36]);
                        float rightEyeY = Float.parseFloat(numbers[37]);

                        float middleEyeX = (leftEyeX + rightEyeX) / 2f;
                        float middleEyeY = (leftEyeY + rightEyeY) / 2f;

                        float noseX = Float.parseFloat(numbers[24]);
                        float noseY = Float.parseFloat(numbers[25]);

                        double angle = CalulateXYAnagle(noseX, noseY, middleEyeX, middleEyeY);
                        angle = angle - 90;
                        if (angle < -90 || angle > 90) {
                            writer.println("10000");
                            writer1.println("10000");
                        } else {
                            writer.println(angle);

                            if (angle < 0) {
                                writer1.println(-1 + ":" + angle);
                            } else if (angle > 0) {
                                writer1.println(1 + ":" + angle);
                            } else if (angle == 0) {
                                writer1.println(0 + ":" + angle);
                            }

                        }

                    } else {
                        writer.println("10000");
                        writer1.println("10000");
                    }
                }
                lineNo++;
                line = br.readLine();
            }

        } finally {
            writer1.close();
            writer.close();
            br.close();
        }

    }

    public void generateDurationFile(String inputFileName, String outputFileName) throws Exception {

        BufferedReader br = new BufferedReader(new FileReader(inputFileName));
        PrintWriter writer = new PrintWriter(outputFileName, "UTF-8");

        int startLineNo = 1;
        int preValue = 100;

        int currentLineNo = 1;
        int totalCount = 0;
        double totalValue = 0;
        try {
            String line = br.readLine();

            while (line != null) {

                if (!"10000".equals(line) && !"0:0.0".equals(line)) {
                    String[] values = line.split(":");
                    int currentValue = Integer.parseInt(values[0]);
                    double realValue = Double.parseDouble(values[1]);
                    if (preValue == 100) {
                        startLineNo = currentLineNo;
                        preValue = currentValue;

                        if (currentValue != 0) {
                            totalValue += realValue;
                            totalCount++;
                        }

                    } else {
                        if (currentValue == 1 || currentValue == -1) {
                            if (currentValue == -preValue) {
                                writer.println(startLineNo + "," + (currentLineNo - 1) + ","
                                                + (currentLineNo - startLineNo) + ","
                                                + (currentLineNo - startLineNo) / 25f + "," + preValue + ","
                                                + totalValue / totalCount);
                                preValue = currentValue;
                                startLineNo = currentLineNo;
                                totalValue = 0;
                                totalCount = 0;
                                totalValue += realValue;
                                totalCount++;
                            } else {
                                totalValue += realValue;
                                totalCount++;
                            }
                        }
                    }
                }

                currentLineNo++;
                line = br.readLine();
            }

            writer.println(startLineNo + "," + (currentLineNo - 1) + "," + (currentLineNo - startLineNo) + ","
                            + (currentLineNo - startLineNo) / 25f + "," + preValue + "," + totalValue / totalCount);

        } finally {
            br.close();
            writer.close();
        }

    }

    public int getFileLineNos(String inputFileName) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(inputFileName));
        int lineNo = 0;
        try {
            String line = br.readLine();
            while (line != null) {
                lineNo++;
                line = br.readLine();
            }

            br.close();
            return lineNo;

        } catch (Exception e) {
            return 0;
        } finally {
            br.close();
        }

    }

}
