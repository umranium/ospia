package org.poscomp.eqclinic.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class SMSTool {

    private static final Logger logger = Logger.getLogger(SMSTool.class);
    
    private static String USER_AGENT = "Mozilla/5.0";
    public static final String ACCOUNT_SID = "ACee4e87cac827bfc8563885f759831a89";
    public static final String AUTH_TOKEN = "7a9a5348ba68aac6d47ea0b5b381be10";
    public static final String EQCLININUMBER = "+61427806356";
    
    
    public static void main(String[] args) throws Exception {
       
    }
    
    public static void sendReplySMS(Patient patient){
        try {
            TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);

            // Build the parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("To", patient.getPhone()));
            params.add(new BasicNameValuePair("From", EQCLININUMBER));
            params.add(new BasicNameValuePair("Body", "Thanks for your reply. You have confirmed your appointment."));

            MessageFactory messageFactory = client.getAccount().getMessageFactory();
            Message message = messageFactory.create(params);
            System.out.println(message.getSid());
        } catch (TwilioRestException e) {
            System.out.println(e.getErrorMessage());
        }
        
    }
    

    public static void sendSMS(Appointment apt, Doctor doctor, Patient patient) {

        String dateInfo = TimeTool.convertLongToString(apt.getStarttime());
        String doctorName = doctor.getFirstname();
        try {
            TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);

            // Build the parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("To", patient.getPhone()));
            params.add(new BasicNameValuePair("From", EQCLININUMBER));
            params.add(new BasicNameValuePair("Body", "Thanks for offering your time. There has been a request for the following appointment: ("+dateInfo+", Sydney Time) from "+doctorName+". Please reply '"+apt.getAptid()+"' to confirm it."));

            MessageFactory messageFactory = client.getAccount().getMessageFactory();
            Message message = messageFactory.create(params);
            System.out.println(message.getSid());
        } catch (TwilioRestException e) {
            System.out.println(e.getErrorMessage());
        }

    }
    
    public static void sendSMSCancelled(Appointment apt, Patient patient) {

        String dateInfo = TimeTool.convertLongToString(apt.getStarttime());
        try {
            TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);

            // Build the parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("To", patient.getPhone()));
            params.add(new BasicNameValuePair("From", EQCLININUMBER));
            String messageContent = "";
            if(apt.getStarttime()-System.currentTimeMillis()<48*60*60*1000){
                messageContent = "Unfortunately, the following OSPIA appointment ("+dateInfo+", Sydney Time) has been cancelled by the student. Feel free to return to the website to indicate other times you are available."; 
            }else{
                messageContent = "Unfortunately, the following OSPIA appointment ("+dateInfo+", Sydney Time) has been cancelled by the student. Another student may book this appointment. Please await further updates.";
            }
            
            params.add(new BasicNameValuePair("Body", messageContent));

            MessageFactory messageFactory = client.getAccount().getMessageFactory();
            Message message = messageFactory.create(params);
            System.out.println(message.getSid());
        } catch (TwilioRestException e) {
            System.out.println(e.getErrorMessage());
        }

    }
    

}
