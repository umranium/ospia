package org.poscomp.eqclinic.util;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class StringTool {

    public static boolean isValidString(String input) {

        if (input != null && !"".equals(input)) {
            return true;
        }
        return false;
    }
    
    public static String filterString(String input) {
        input = input.trim();
        if (input == null) {
            return null;
        }
        if (input.length() == 0) {
            return input;
        }
        
        input.replaceAll("\t", " ");
        return input.replaceAll("\n", " ");

    }
    

    public static String filterHtml(String input) {
        input = input.trim();
        if (input == null) {
            return null;
        }
        if (input.length() == 0) {
            return input;
        }
        input = input.replaceAll("&", "&amp;");
        input = input.replaceAll("<", "&lt;");
        input = input.replaceAll(">", "&gt;");
        input = input.replaceAll(" ", "&nbsp;");
        input = input.replaceAll("'", "&#39;");
        input = input.replaceAll("\"", "&#39;");
        return input.replaceAll("\n", "<br>");

    }

}
