package org.poscomp.eqclinic.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.pojo.TouchObj;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class TouchTool {

    private static final Logger logger = Logger.getLogger(TouchTool.class);

    public void getTouches(String inputFileName, String outputFileName) {
        float avg;
        List<TouchObj> touches = new ArrayList<TouchObj>();
        try {
            avg = getAvgPersentage(inputFileName);
            touches = readTouchPersentage(inputFileName, avg);
            touches = findSuspects1(touches, 0.02, 0.04);
            touches = shrinkTouches(touches);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        BufferedWriter bw;

        try {
            bw = new BufferedWriter(new FileWriter(outputFileName));
            for (TouchObj touchObj : touches) {
                bw.write(touchObj.getLineNo() / 25.0 + "," + (touchObj.getLineNo() / 25.0 + 1.03));
                bw.newLine();
            }
            bw.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }

    public List<TouchObj> shrinkTouches(List<TouchObj> touches) {

        List<TouchObj> result = new ArrayList<TouchObj>();

        double last = -1;

        for (TouchObj touchObj : touches) {
            if (Math.abs(last + 1) < 0.000000001) {
                last = touchObj.getLineNo() / 25.0;
                result.add(touchObj);
            } else {
                if (Math.abs(touchObj.getLineNo() / 25.0 - last) > 1) {
                    last = touchObj.getLineNo() / 25.0;
                    result.add(touchObj);
                }
            }
        }
        return result;
    }

    public void convert(String fileName, String outputFileName) throws Exception {

        BufferedReader br = new BufferedReader(new FileReader(fileName));
        BufferedWriter bw = new BufferedWriter(new FileWriter(outputFileName));

        try {
            String line = br.readLine();
            while (line != null) {
                String[] values = line.split(":");
                int lineNo = Integer.parseInt(values[0]);

                if (values.length == 2) {
                    bw.write(line);
                    bw.newLine();
                } else {
                    float percentage = Integer.parseInt(values[1]) * 1.0f / Integer.parseInt(values[2]);
                    bw.write(lineNo + ":" + percentage);
                    bw.newLine();
                }
                line = br.readLine();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            br.close();
            bw.close();
        }

    }

    public List<TouchObj> findSuspects1(List<TouchObj> touches, double prethreshold, double preprethreshold) {

        TouchObj preTouch = null;
        TouchObj prepreTouch = null;

        List<TouchObj> results = new ArrayList<TouchObj>();

        for (TouchObj touchObj : touches) {

            if (touchObj.getPersentage() != 0) {

                if (preTouch == null) {
                    preTouch = touchObj;
                } else if (prepreTouch == null) {
                    prepreTouch = preTouch;
                    preTouch = touchObj;
                } else {
                    if ((touchObj.getPersentage() - preTouch.getPersentage()) > prethreshold
                                    && (touchObj.getPersentage() - prepreTouch.getPersentage()) > preprethreshold) {
                        results.add(touchObj);
                    }
                    prepreTouch = preTouch;
                    preTouch = touchObj;

                }

            }

        }

        return results;
    }

    public List<TouchObj> readTouchPersentage(String fileName, float avg) throws Exception {

        List<TouchObj> touches = new ArrayList<TouchObj>();

        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            String line = br.readLine();
            while (line != null) {
                String[] values = line.split(":");
                int lineNo = Integer.parseInt(values[0]);
                double percentage = Double.parseDouble(values[1]);

                if (values.length == 2) {
                    TouchObj to = new TouchObj();
                    to.setLineNo(lineNo);
                    to.setPersentage(0);
                    touches.add(to);
                } else {

                    percentage = Integer.parseInt(values[1]) * 1.0f / Integer.parseInt(values[2]);
                    TouchObj to = new TouchObj();
                    to.setLineNo(lineNo);
                    to.setPersentage(percentage);
                    touches.add(to);
                }

                line = br.readLine();
            }
            return touches;

        } finally {
            br.close();
        }

    }

    public double maxPer = -1000;
    public double minPer = 1000;

    public float getAvgPersentage(String fileName) throws Exception {

        float totalPersentage = 0;
        int totalLines = 0;

        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            String line = br.readLine();
            while (line != null) {
                String[] values = line.split(":");
                float percentage = Float.parseFloat(values[1]);

                if (percentage != 0) {

                    percentage = Integer.parseInt(values[1]) * 1.0f / Integer.parseInt(values[2]);

                    if (percentage > maxPer) {
                        maxPer = percentage;
                    }

                    if (percentage < minPer) {
                        minPer = percentage;
                    }

                    totalLines++;
                    totalPersentage += percentage;
                }
                line = br.readLine();
            }

            return totalPersentage / totalLines;

        } finally {
            br.close();
        }

    }

}
