/**
 * AllData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public interface AllData extends java.rmi.Remote {
    public org.poscomp.eqclinic.ws.User getUser() throws java.rmi.RemoteException;
    public org.poscomp.eqclinic.ws.InitMessage getInitMessage() throws java.rmi.RemoteException;
    public org.poscomp.eqclinic.ws.AssessmentTemplateList getAssessmentTemplates() throws java.rmi.RemoteException;
    public org.poscomp.eqclinic.ws.AssessmentList getAssessments() throws java.rmi.RemoteException;
    public org.poscomp.eqclinic.ws.AssessmentList getAssessments(java.lang.String studentId, java.lang.String phase) throws java.rmi.RemoteException;
    public org.poscomp.eqclinic.ws.SubmitAssessmentResponse submitAssessment(org.poscomp.eqclinic.ws.Assessment assessment) throws java.rmi.RemoteException;
}
