package org.poscomp.eqclinic.ws;

public class AllDataProxy implements org.poscomp.eqclinic.ws.AllData {
  private String _endpoint = null;
  private org.poscomp.eqclinic.ws.AllData allData = null;
  
  public AllDataProxy() {
    _initAllDataProxy();
  }
  
  public AllDataProxy(String endpoint) {
    _endpoint = endpoint;
    _initAllDataProxy();
  }
  
  private void _initAllDataProxy() {
    try {
      allData = (new org.poscomp.eqclinic.ws.AllDataServiceLocator()).getDomino();
      if (allData != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)allData)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)allData)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (allData != null)
      ((javax.xml.rpc.Stub)allData)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.poscomp.eqclinic.ws.AllData getAllData() {
    if (allData == null)
      _initAllDataProxy();
    return allData;
  }
  
  public org.poscomp.eqclinic.ws.User getUser() throws java.rmi.RemoteException{
    if (allData == null)
      _initAllDataProxy();
    return allData.getUser();
  }
  
  public org.poscomp.eqclinic.ws.InitMessage getInitMessage() throws java.rmi.RemoteException{
    if (allData == null)
      _initAllDataProxy();
    return allData.getInitMessage();
  }
  
  public org.poscomp.eqclinic.ws.AssessmentTemplateList getAssessmentTemplates() throws java.rmi.RemoteException{
    if (allData == null)
      _initAllDataProxy();
    return allData.getAssessmentTemplates();
  }
  
  public org.poscomp.eqclinic.ws.AssessmentList getAssessments() throws java.rmi.RemoteException{
    if (allData == null)
      _initAllDataProxy();
    return allData.getAssessments();
  }
  
  public org.poscomp.eqclinic.ws.AssessmentList getAssessments(java.lang.String studentId, java.lang.String phase) throws java.rmi.RemoteException{
    if (allData == null)
      _initAllDataProxy();
    return allData.getAssessments(studentId, phase);
  }
  
  public org.poscomp.eqclinic.ws.SubmitAssessmentResponse submitAssessment(org.poscomp.eqclinic.ws.Assessment assessment) throws java.rmi.RemoteException{
    if (allData == null)
      _initAllDataProxy();
    return allData.submitAssessment(assessment);
  }
  
  
}