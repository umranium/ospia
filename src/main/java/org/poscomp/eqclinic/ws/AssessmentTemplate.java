/**
 * AssessmentTemplate.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public class AssessmentTemplate  implements java.io.Serializable {
    private java.lang.String uniqueKey;

    private java.lang.String assessmentType;

    private java.lang.String assessmentTitle;

    private java.lang.String assessmentName;

    private java.lang.String assessmentPhase;

    private java.lang.String assessmentCourse;

    private boolean userCanCreate;

    private org.poscomp.eqclinic.ws.TemplateField assessmentDetail;

    private org.poscomp.eqclinic.ws.StudentDetails studentDetails;

    private org.poscomp.eqclinic.ws.AssessorDetails assessorDetails;

    private org.poscomp.eqclinic.ws.Section[] sectionList;

    private org.poscomp.eqclinic.ws.CaseDescription caseDescription;

    private org.poscomp.eqclinic.ws.Grading grading;

    private org.poscomp.eqclinic.ws.Feedback[] feedbackList;

    private org.poscomp.eqclinic.ws.Information[] informationList;

    private boolean showTemplate;

    private boolean allowWithdraw;

    private boolean reviewWorkflow;

    public AssessmentTemplate() {
    }

    public AssessmentTemplate(
           java.lang.String uniqueKey,
           java.lang.String assessmentType,
           java.lang.String assessmentTitle,
           java.lang.String assessmentName,
           java.lang.String assessmentPhase,
           java.lang.String assessmentCourse,
           boolean userCanCreate,
           org.poscomp.eqclinic.ws.TemplateField assessmentDetail,
           org.poscomp.eqclinic.ws.StudentDetails studentDetails,
           org.poscomp.eqclinic.ws.AssessorDetails assessorDetails,
           org.poscomp.eqclinic.ws.Section[] sectionList,
           org.poscomp.eqclinic.ws.CaseDescription caseDescription,
           org.poscomp.eqclinic.ws.Grading grading,
           org.poscomp.eqclinic.ws.Feedback[] feedbackList,
           org.poscomp.eqclinic.ws.Information[] informationList,
           boolean showTemplate,
           boolean allowWithdraw,
           boolean reviewWorkflow) {
           this.uniqueKey = uniqueKey;
           this.assessmentType = assessmentType;
           this.assessmentTitle = assessmentTitle;
           this.assessmentName = assessmentName;
           this.assessmentPhase = assessmentPhase;
           this.assessmentCourse = assessmentCourse;
           this.userCanCreate = userCanCreate;
           this.assessmentDetail = assessmentDetail;
           this.studentDetails = studentDetails;
           this.assessorDetails = assessorDetails;
           this.sectionList = sectionList;
           this.caseDescription = caseDescription;
           this.grading = grading;
           this.feedbackList = feedbackList;
           this.informationList = informationList;
           this.showTemplate = showTemplate;
           this.allowWithdraw = allowWithdraw;
           this.reviewWorkflow = reviewWorkflow;
    }


    /**
     * Gets the uniqueKey value for this AssessmentTemplate.
     * 
     * @return uniqueKey
     */
    public java.lang.String getUniqueKey() {
        return uniqueKey;
    }


    /**
     * Sets the uniqueKey value for this AssessmentTemplate.
     * 
     * @param uniqueKey
     */
    public void setUniqueKey(java.lang.String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }


    /**
     * Gets the assessmentType value for this AssessmentTemplate.
     * 
     * @return assessmentType
     */
    public java.lang.String getAssessmentType() {
        return assessmentType;
    }


    /**
     * Sets the assessmentType value for this AssessmentTemplate.
     * 
     * @param assessmentType
     */
    public void setAssessmentType(java.lang.String assessmentType) {
        this.assessmentType = assessmentType;
    }


    /**
     * Gets the assessmentTitle value for this AssessmentTemplate.
     * 
     * @return assessmentTitle
     */
    public java.lang.String getAssessmentTitle() {
        return assessmentTitle;
    }


    /**
     * Sets the assessmentTitle value for this AssessmentTemplate.
     * 
     * @param assessmentTitle
     */
    public void setAssessmentTitle(java.lang.String assessmentTitle) {
        this.assessmentTitle = assessmentTitle;
    }


    /**
     * Gets the assessmentName value for this AssessmentTemplate.
     * 
     * @return assessmentName
     */
    public java.lang.String getAssessmentName() {
        return assessmentName;
    }


    /**
     * Sets the assessmentName value for this AssessmentTemplate.
     * 
     * @param assessmentName
     */
    public void setAssessmentName(java.lang.String assessmentName) {
        this.assessmentName = assessmentName;
    }


    /**
     * Gets the assessmentPhase value for this AssessmentTemplate.
     * 
     * @return assessmentPhase
     */
    public java.lang.String getAssessmentPhase() {
        return assessmentPhase;
    }


    /**
     * Sets the assessmentPhase value for this AssessmentTemplate.
     * 
     * @param assessmentPhase
     */
    public void setAssessmentPhase(java.lang.String assessmentPhase) {
        this.assessmentPhase = assessmentPhase;
    }


    /**
     * Gets the assessmentCourse value for this AssessmentTemplate.
     * 
     * @return assessmentCourse
     */
    public java.lang.String getAssessmentCourse() {
        return assessmentCourse;
    }


    /**
     * Sets the assessmentCourse value for this AssessmentTemplate.
     * 
     * @param assessmentCourse
     */
    public void setAssessmentCourse(java.lang.String assessmentCourse) {
        this.assessmentCourse = assessmentCourse;
    }


    /**
     * Gets the userCanCreate value for this AssessmentTemplate.
     * 
     * @return userCanCreate
     */
    public boolean isUserCanCreate() {
        return userCanCreate;
    }


    /**
     * Sets the userCanCreate value for this AssessmentTemplate.
     * 
     * @param userCanCreate
     */
    public void setUserCanCreate(boolean userCanCreate) {
        this.userCanCreate = userCanCreate;
    }


    /**
     * Gets the assessmentDetail value for this AssessmentTemplate.
     * 
     * @return assessmentDetail
     */
    public org.poscomp.eqclinic.ws.TemplateField getAssessmentDetail() {
        return assessmentDetail;
    }


    /**
     * Sets the assessmentDetail value for this AssessmentTemplate.
     * 
     * @param assessmentDetail
     */
    public void setAssessmentDetail(org.poscomp.eqclinic.ws.TemplateField assessmentDetail) {
        this.assessmentDetail = assessmentDetail;
    }


    /**
     * Gets the studentDetails value for this AssessmentTemplate.
     * 
     * @return studentDetails
     */
    public org.poscomp.eqclinic.ws.StudentDetails getStudentDetails() {
        return studentDetails;
    }


    /**
     * Sets the studentDetails value for this AssessmentTemplate.
     * 
     * @param studentDetails
     */
    public void setStudentDetails(org.poscomp.eqclinic.ws.StudentDetails studentDetails) {
        this.studentDetails = studentDetails;
    }


    /**
     * Gets the assessorDetails value for this AssessmentTemplate.
     * 
     * @return assessorDetails
     */
    public org.poscomp.eqclinic.ws.AssessorDetails getAssessorDetails() {
        return assessorDetails;
    }


    /**
     * Sets the assessorDetails value for this AssessmentTemplate.
     * 
     * @param assessorDetails
     */
    public void setAssessorDetails(org.poscomp.eqclinic.ws.AssessorDetails assessorDetails) {
        this.assessorDetails = assessorDetails;
    }


    /**
     * Gets the sectionList value for this AssessmentTemplate.
     * 
     * @return sectionList
     */
    public org.poscomp.eqclinic.ws.Section[] getSectionList() {
        return sectionList;
    }


    /**
     * Sets the sectionList value for this AssessmentTemplate.
     * 
     * @param sectionList
     */
    public void setSectionList(org.poscomp.eqclinic.ws.Section[] sectionList) {
        this.sectionList = sectionList;
    }

    public org.poscomp.eqclinic.ws.Section getSectionList(int i) {
        return this.sectionList[i];
    }

    public void setSectionList(int i, org.poscomp.eqclinic.ws.Section _value) {
        this.sectionList[i] = _value;
    }


    /**
     * Gets the caseDescription value for this AssessmentTemplate.
     * 
     * @return caseDescription
     */
    public org.poscomp.eqclinic.ws.CaseDescription getCaseDescription() {
        return caseDescription;
    }


    /**
     * Sets the caseDescription value for this AssessmentTemplate.
     * 
     * @param caseDescription
     */
    public void setCaseDescription(org.poscomp.eqclinic.ws.CaseDescription caseDescription) {
        this.caseDescription = caseDescription;
    }


    /**
     * Gets the grading value for this AssessmentTemplate.
     * 
     * @return grading
     */
    public org.poscomp.eqclinic.ws.Grading getGrading() {
        return grading;
    }


    /**
     * Sets the grading value for this AssessmentTemplate.
     * 
     * @param grading
     */
    public void setGrading(org.poscomp.eqclinic.ws.Grading grading) {
        this.grading = grading;
    }


    /**
     * Gets the feedbackList value for this AssessmentTemplate.
     * 
     * @return feedbackList
     */
    public org.poscomp.eqclinic.ws.Feedback[] getFeedbackList() {
        return feedbackList;
    }


    /**
     * Sets the feedbackList value for this AssessmentTemplate.
     * 
     * @param feedbackList
     */
    public void setFeedbackList(org.poscomp.eqclinic.ws.Feedback[] feedbackList) {
        this.feedbackList = feedbackList;
    }

    public org.poscomp.eqclinic.ws.Feedback getFeedbackList(int i) {
        return this.feedbackList[i];
    }

    public void setFeedbackList(int i, org.poscomp.eqclinic.ws.Feedback _value) {
        this.feedbackList[i] = _value;
    }


    /**
     * Gets the informationList value for this AssessmentTemplate.
     * 
     * @return informationList
     */
    public org.poscomp.eqclinic.ws.Information[] getInformationList() {
        return informationList;
    }


    /**
     * Sets the informationList value for this AssessmentTemplate.
     * 
     * @param informationList
     */
    public void setInformationList(org.poscomp.eqclinic.ws.Information[] informationList) {
        this.informationList = informationList;
    }

    public org.poscomp.eqclinic.ws.Information getInformationList(int i) {
        return this.informationList[i];
    }

    public void setInformationList(int i, org.poscomp.eqclinic.ws.Information _value) {
        this.informationList[i] = _value;
    }


    /**
     * Gets the showTemplate value for this AssessmentTemplate.
     * 
     * @return showTemplate
     */
    public boolean isShowTemplate() {
        return showTemplate;
    }


    /**
     * Sets the showTemplate value for this AssessmentTemplate.
     * 
     * @param showTemplate
     */
    public void setShowTemplate(boolean showTemplate) {
        this.showTemplate = showTemplate;
    }


    /**
     * Gets the allowWithdraw value for this AssessmentTemplate.
     * 
     * @return allowWithdraw
     */
    public boolean isAllowWithdraw() {
        return allowWithdraw;
    }


    /**
     * Sets the allowWithdraw value for this AssessmentTemplate.
     * 
     * @param allowWithdraw
     */
    public void setAllowWithdraw(boolean allowWithdraw) {
        this.allowWithdraw = allowWithdraw;
    }


    /**
     * Gets the reviewWorkflow value for this AssessmentTemplate.
     * 
     * @return reviewWorkflow
     */
    public boolean isReviewWorkflow() {
        return reviewWorkflow;
    }


    /**
     * Sets the reviewWorkflow value for this AssessmentTemplate.
     * 
     * @param reviewWorkflow
     */
    public void setReviewWorkflow(boolean reviewWorkflow) {
        this.reviewWorkflow = reviewWorkflow;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AssessmentTemplate)) return false;
        AssessmentTemplate other = (AssessmentTemplate) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.uniqueKey==null && other.getUniqueKey()==null) || 
             (this.uniqueKey!=null &&
              this.uniqueKey.equals(other.getUniqueKey()))) &&
            ((this.assessmentType==null && other.getAssessmentType()==null) || 
             (this.assessmentType!=null &&
              this.assessmentType.equals(other.getAssessmentType()))) &&
            ((this.assessmentTitle==null && other.getAssessmentTitle()==null) || 
             (this.assessmentTitle!=null &&
              this.assessmentTitle.equals(other.getAssessmentTitle()))) &&
            ((this.assessmentName==null && other.getAssessmentName()==null) || 
             (this.assessmentName!=null &&
              this.assessmentName.equals(other.getAssessmentName()))) &&
            ((this.assessmentPhase==null && other.getAssessmentPhase()==null) || 
             (this.assessmentPhase!=null &&
              this.assessmentPhase.equals(other.getAssessmentPhase()))) &&
            ((this.assessmentCourse==null && other.getAssessmentCourse()==null) || 
             (this.assessmentCourse!=null &&
              this.assessmentCourse.equals(other.getAssessmentCourse()))) &&
            this.userCanCreate == other.isUserCanCreate() &&
            ((this.assessmentDetail==null && other.getAssessmentDetail()==null) || 
             (this.assessmentDetail!=null &&
              this.assessmentDetail.equals(other.getAssessmentDetail()))) &&
            ((this.studentDetails==null && other.getStudentDetails()==null) || 
             (this.studentDetails!=null &&
              this.studentDetails.equals(other.getStudentDetails()))) &&
            ((this.assessorDetails==null && other.getAssessorDetails()==null) || 
             (this.assessorDetails!=null &&
              this.assessorDetails.equals(other.getAssessorDetails()))) &&
            ((this.sectionList==null && other.getSectionList()==null) || 
             (this.sectionList!=null &&
              java.util.Arrays.equals(this.sectionList, other.getSectionList()))) &&
            ((this.caseDescription==null && other.getCaseDescription()==null) || 
             (this.caseDescription!=null &&
              this.caseDescription.equals(other.getCaseDescription()))) &&
            ((this.grading==null && other.getGrading()==null) || 
             (this.grading!=null &&
              this.grading.equals(other.getGrading()))) &&
            ((this.feedbackList==null && other.getFeedbackList()==null) || 
             (this.feedbackList!=null &&
              java.util.Arrays.equals(this.feedbackList, other.getFeedbackList()))) &&
            ((this.informationList==null && other.getInformationList()==null) || 
             (this.informationList!=null &&
              java.util.Arrays.equals(this.informationList, other.getInformationList()))) &&
            this.showTemplate == other.isShowTemplate() &&
            this.allowWithdraw == other.isAllowWithdraw() &&
            this.reviewWorkflow == other.isReviewWorkflow();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUniqueKey() != null) {
            _hashCode += getUniqueKey().hashCode();
        }
        if (getAssessmentType() != null) {
            _hashCode += getAssessmentType().hashCode();
        }
        if (getAssessmentTitle() != null) {
            _hashCode += getAssessmentTitle().hashCode();
        }
        if (getAssessmentName() != null) {
            _hashCode += getAssessmentName().hashCode();
        }
        if (getAssessmentPhase() != null) {
            _hashCode += getAssessmentPhase().hashCode();
        }
        if (getAssessmentCourse() != null) {
            _hashCode += getAssessmentCourse().hashCode();
        }
        _hashCode += (isUserCanCreate() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getAssessmentDetail() != null) {
            _hashCode += getAssessmentDetail().hashCode();
        }
        if (getStudentDetails() != null) {
            _hashCode += getStudentDetails().hashCode();
        }
        if (getAssessorDetails() != null) {
            _hashCode += getAssessorDetails().hashCode();
        }
        if (getSectionList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSectionList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSectionList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCaseDescription() != null) {
            _hashCode += getCaseDescription().hashCode();
        }
        if (getGrading() != null) {
            _hashCode += getGrading().hashCode();
        }
        if (getFeedbackList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFeedbackList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFeedbackList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getInformationList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getInformationList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getInformationList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += (isShowTemplate() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isAllowWithdraw() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isReviewWorkflow() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AssessmentTemplate.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "AssessmentTemplate"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uniqueKey");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UniqueKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assessmentType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AssessmentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assessmentTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AssessmentTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assessmentName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AssessmentName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assessmentPhase");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AssessmentPhase"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assessmentCourse");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AssessmentCourse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userCanCreate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UserCanCreate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assessmentDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AssessmentDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("studentDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StudentDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "StudentDetails"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assessorDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AssessorDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "AssessorDetails"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sectionList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SectionList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "Section"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("caseDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CaseDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "CaseDescription"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("grading");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Grading"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "Grading"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feedbackList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FeedbackList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "Feedback"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("informationList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "InformationList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "Information"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("showTemplate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ShowTemplate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowWithdraw");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AllowWithdraw"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reviewWorkflow");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ReviewWorkflow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
