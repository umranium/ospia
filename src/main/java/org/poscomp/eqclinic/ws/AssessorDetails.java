/**
 * AssessorDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public class AssessorDetails  implements java.io.Serializable {
    private java.lang.String assessorId;

    private org.poscomp.eqclinic.ws.TemplateField nameOfAssessor;

    private org.poscomp.eqclinic.ws.TemplateField school;

    private org.poscomp.eqclinic.ws.TemplateField department;

    private org.poscomp.eqclinic.ws.TemplateField phone;

    private org.poscomp.eqclinic.ws.TemplateField email;

    public AssessorDetails() {
    }

    public AssessorDetails(
           java.lang.String assessorId,
           org.poscomp.eqclinic.ws.TemplateField nameOfAssessor,
           org.poscomp.eqclinic.ws.TemplateField school,
           org.poscomp.eqclinic.ws.TemplateField department,
           org.poscomp.eqclinic.ws.TemplateField phone,
           org.poscomp.eqclinic.ws.TemplateField email) {
           this.assessorId = assessorId;
           this.nameOfAssessor = nameOfAssessor;
           this.school = school;
           this.department = department;
           this.phone = phone;
           this.email = email;
    }


    /**
     * Gets the assessorId value for this AssessorDetails.
     * 
     * @return assessorId
     */
    public java.lang.String getAssessorId() {
        return assessorId;
    }


    /**
     * Sets the assessorId value for this AssessorDetails.
     * 
     * @param assessorId
     */
    public void setAssessorId(java.lang.String assessorId) {
        this.assessorId = assessorId;
    }


    /**
     * Gets the nameOfAssessor value for this AssessorDetails.
     * 
     * @return nameOfAssessor
     */
    public org.poscomp.eqclinic.ws.TemplateField getNameOfAssessor() {
        return nameOfAssessor;
    }


    /**
     * Sets the nameOfAssessor value for this AssessorDetails.
     * 
     * @param nameOfAssessor
     */
    public void setNameOfAssessor(org.poscomp.eqclinic.ws.TemplateField nameOfAssessor) {
        this.nameOfAssessor = nameOfAssessor;
    }


    /**
     * Gets the school value for this AssessorDetails.
     * 
     * @return school
     */
    public org.poscomp.eqclinic.ws.TemplateField getSchool() {
        return school;
    }


    /**
     * Sets the school value for this AssessorDetails.
     * 
     * @param school
     */
    public void setSchool(org.poscomp.eqclinic.ws.TemplateField school) {
        this.school = school;
    }


    /**
     * Gets the department value for this AssessorDetails.
     * 
     * @return department
     */
    public org.poscomp.eqclinic.ws.TemplateField getDepartment() {
        return department;
    }


    /**
     * Sets the department value for this AssessorDetails.
     * 
     * @param department
     */
    public void setDepartment(org.poscomp.eqclinic.ws.TemplateField department) {
        this.department = department;
    }


    /**
     * Gets the phone value for this AssessorDetails.
     * 
     * @return phone
     */
    public org.poscomp.eqclinic.ws.TemplateField getPhone() {
        return phone;
    }


    /**
     * Sets the phone value for this AssessorDetails.
     * 
     * @param phone
     */
    public void setPhone(org.poscomp.eqclinic.ws.TemplateField phone) {
        this.phone = phone;
    }


    /**
     * Gets the email value for this AssessorDetails.
     * 
     * @return email
     */
    public org.poscomp.eqclinic.ws.TemplateField getEmail() {
        return email;
    }


    /**
     * Sets the email value for this AssessorDetails.
     * 
     * @param email
     */
    public void setEmail(org.poscomp.eqclinic.ws.TemplateField email) {
        this.email = email;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AssessorDetails)) return false;
        AssessorDetails other = (AssessorDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.assessorId==null && other.getAssessorId()==null) || 
             (this.assessorId!=null &&
              this.assessorId.equals(other.getAssessorId()))) &&
            ((this.nameOfAssessor==null && other.getNameOfAssessor()==null) || 
             (this.nameOfAssessor!=null &&
              this.nameOfAssessor.equals(other.getNameOfAssessor()))) &&
            ((this.school==null && other.getSchool()==null) || 
             (this.school!=null &&
              this.school.equals(other.getSchool()))) &&
            ((this.department==null && other.getDepartment()==null) || 
             (this.department!=null &&
              this.department.equals(other.getDepartment()))) &&
            ((this.phone==null && other.getPhone()==null) || 
             (this.phone!=null &&
              this.phone.equals(other.getPhone()))) &&
            ((this.email==null && other.getEmail()==null) || 
             (this.email!=null &&
              this.email.equals(other.getEmail())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAssessorId() != null) {
            _hashCode += getAssessorId().hashCode();
        }
        if (getNameOfAssessor() != null) {
            _hashCode += getNameOfAssessor().hashCode();
        }
        if (getSchool() != null) {
            _hashCode += getSchool().hashCode();
        }
        if (getDepartment() != null) {
            _hashCode += getDepartment().hashCode();
        }
        if (getPhone() != null) {
            _hashCode += getPhone().hashCode();
        }
        if (getEmail() != null) {
            _hashCode += getEmail().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AssessorDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "AssessorDetails"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assessorId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AssessorId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameOfAssessor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NameOfAssessor"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("school");
        elemField.setXmlName(new javax.xml.namespace.QName("", "School"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("department");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Department"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phone");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Phone"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Email"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
