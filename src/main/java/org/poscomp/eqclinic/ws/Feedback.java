/**
 * Feedback.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public class Feedback  implements java.io.Serializable {
    private java.lang.String sectionId;

    private int feedbackId;

    private org.poscomp.eqclinic.ws.TemplateField title;

    private org.poscomp.eqclinic.ws.TemplateField description;

    private org.poscomp.eqclinic.ws.TemplateField response;

    private java.lang.String[] responseList;

    private java.lang.String componentType;

    private java.lang.String feedbackType;

    private java.lang.String responseCreated;

    private java.lang.String responseModified;

    public Feedback() {
    }

    public Feedback(
           java.lang.String sectionId,
           int feedbackId,
           org.poscomp.eqclinic.ws.TemplateField title,
           org.poscomp.eqclinic.ws.TemplateField description,
           org.poscomp.eqclinic.ws.TemplateField response,
           java.lang.String[] responseList,
           java.lang.String componentType,
           java.lang.String feedbackType,
           java.lang.String responseCreated,
           java.lang.String responseModified) {
           this.sectionId = sectionId;
           this.feedbackId = feedbackId;
           this.title = title;
           this.description = description;
           this.response = response;
           this.responseList = responseList;
           this.componentType = componentType;
           this.feedbackType = feedbackType;
           this.responseCreated = responseCreated;
           this.responseModified = responseModified;
    }


    /**
     * Gets the sectionId value for this Feedback.
     * 
     * @return sectionId
     */
    public java.lang.String getSectionId() {
        return sectionId;
    }


    /**
     * Sets the sectionId value for this Feedback.
     * 
     * @param sectionId
     */
    public void setSectionId(java.lang.String sectionId) {
        this.sectionId = sectionId;
    }


    /**
     * Gets the feedbackId value for this Feedback.
     * 
     * @return feedbackId
     */
    public int getFeedbackId() {
        return feedbackId;
    }


    /**
     * Sets the feedbackId value for this Feedback.
     * 
     * @param feedbackId
     */
    public void setFeedbackId(int feedbackId) {
        this.feedbackId = feedbackId;
    }


    /**
     * Gets the title value for this Feedback.
     * 
     * @return title
     */
    public org.poscomp.eqclinic.ws.TemplateField getTitle() {
        return title;
    }


    /**
     * Sets the title value for this Feedback.
     * 
     * @param title
     */
    public void setTitle(org.poscomp.eqclinic.ws.TemplateField title) {
        this.title = title;
    }


    /**
     * Gets the description value for this Feedback.
     * 
     * @return description
     */
    public org.poscomp.eqclinic.ws.TemplateField getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Feedback.
     * 
     * @param description
     */
    public void setDescription(org.poscomp.eqclinic.ws.TemplateField description) {
        this.description = description;
    }


    /**
     * Gets the response value for this Feedback.
     * 
     * @return response
     */
    public org.poscomp.eqclinic.ws.TemplateField getResponse() {
        return response;
    }


    /**
     * Sets the response value for this Feedback.
     * 
     * @param response
     */
    public void setResponse(org.poscomp.eqclinic.ws.TemplateField response) {
        this.response = response;
    }


    /**
     * Gets the responseList value for this Feedback.
     * 
     * @return responseList
     */
    public java.lang.String[] getResponseList() {
        return responseList;
    }


    /**
     * Sets the responseList value for this Feedback.
     * 
     * @param responseList
     */
    public void setResponseList(java.lang.String[] responseList) {
        this.responseList = responseList;
    }

    public java.lang.String getResponseList(int i) {
        return this.responseList[i];
    }

    public void setResponseList(int i, java.lang.String _value) {
        this.responseList[i] = _value;
    }


    /**
     * Gets the componentType value for this Feedback.
     * 
     * @return componentType
     */
    public java.lang.String getComponentType() {
        return componentType;
    }


    /**
     * Sets the componentType value for this Feedback.
     * 
     * @param componentType
     */
    public void setComponentType(java.lang.String componentType) {
        this.componentType = componentType;
    }


    /**
     * Gets the feedbackType value for this Feedback.
     * 
     * @return feedbackType
     */
    public java.lang.String getFeedbackType() {
        return feedbackType;
    }


    /**
     * Sets the feedbackType value for this Feedback.
     * 
     * @param feedbackType
     */
    public void setFeedbackType(java.lang.String feedbackType) {
        this.feedbackType = feedbackType;
    }


    /**
     * Gets the responseCreated value for this Feedback.
     * 
     * @return responseCreated
     */
    public java.lang.String getResponseCreated() {
        return responseCreated;
    }


    /**
     * Sets the responseCreated value for this Feedback.
     * 
     * @param responseCreated
     */
    public void setResponseCreated(java.lang.String responseCreated) {
        this.responseCreated = responseCreated;
    }


    /**
     * Gets the responseModified value for this Feedback.
     * 
     * @return responseModified
     */
    public java.lang.String getResponseModified() {
        return responseModified;
    }


    /**
     * Sets the responseModified value for this Feedback.
     * 
     * @param responseModified
     */
    public void setResponseModified(java.lang.String responseModified) {
        this.responseModified = responseModified;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Feedback)) return false;
        Feedback other = (Feedback) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sectionId==null && other.getSectionId()==null) || 
             (this.sectionId!=null &&
              this.sectionId.equals(other.getSectionId()))) &&
            this.feedbackId == other.getFeedbackId() &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.response==null && other.getResponse()==null) || 
             (this.response!=null &&
              this.response.equals(other.getResponse()))) &&
            ((this.responseList==null && other.getResponseList()==null) || 
             (this.responseList!=null &&
              java.util.Arrays.equals(this.responseList, other.getResponseList()))) &&
            ((this.componentType==null && other.getComponentType()==null) || 
             (this.componentType!=null &&
              this.componentType.equals(other.getComponentType()))) &&
            ((this.feedbackType==null && other.getFeedbackType()==null) || 
             (this.feedbackType!=null &&
              this.feedbackType.equals(other.getFeedbackType()))) &&
            ((this.responseCreated==null && other.getResponseCreated()==null) || 
             (this.responseCreated!=null &&
              this.responseCreated.equals(other.getResponseCreated()))) &&
            ((this.responseModified==null && other.getResponseModified()==null) || 
             (this.responseModified!=null &&
              this.responseModified.equals(other.getResponseModified())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSectionId() != null) {
            _hashCode += getSectionId().hashCode();
        }
        _hashCode += getFeedbackId();
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getResponse() != null) {
            _hashCode += getResponse().hashCode();
        }
        if (getResponseList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResponseList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResponseList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getComponentType() != null) {
            _hashCode += getComponentType().hashCode();
        }
        if (getFeedbackType() != null) {
            _hashCode += getFeedbackType().hashCode();
        }
        if (getResponseCreated() != null) {
            _hashCode += getResponseCreated().hashCode();
        }
        if (getResponseModified() != null) {
            _hashCode += getResponseModified().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Feedback.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "Feedback"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sectionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SectionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feedbackId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FeedbackId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Title"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("response");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Response"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ResponseList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("componentType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ComponentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feedbackType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FeedbackType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseCreated");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ResponseCreated"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseModified");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ResponseModified"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
