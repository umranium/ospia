/**
 * Grading.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public class Grading  implements java.io.Serializable {
    private org.poscomp.eqclinic.ws.TemplateField criteriaDescription;

    private org.poscomp.eqclinic.ws.Criterion[] criterionList;

    public Grading() {
    }

    public Grading(
           org.poscomp.eqclinic.ws.TemplateField criteriaDescription,
           org.poscomp.eqclinic.ws.Criterion[] criterionList) {
           this.criteriaDescription = criteriaDescription;
           this.criterionList = criterionList;
    }


    /**
     * Gets the criteriaDescription value for this Grading.
     * 
     * @return criteriaDescription
     */
    public org.poscomp.eqclinic.ws.TemplateField getCriteriaDescription() {
        return criteriaDescription;
    }


    /**
     * Sets the criteriaDescription value for this Grading.
     * 
     * @param criteriaDescription
     */
    public void setCriteriaDescription(org.poscomp.eqclinic.ws.TemplateField criteriaDescription) {
        this.criteriaDescription = criteriaDescription;
    }


    /**
     * Gets the criterionList value for this Grading.
     * 
     * @return criterionList
     */
    public org.poscomp.eqclinic.ws.Criterion[] getCriterionList() {
        return criterionList;
    }


    /**
     * Sets the criterionList value for this Grading.
     * 
     * @param criterionList
     */
    public void setCriterionList(org.poscomp.eqclinic.ws.Criterion[] criterionList) {
        this.criterionList = criterionList;
    }

    public org.poscomp.eqclinic.ws.Criterion getCriterionList(int i) {
        return this.criterionList[i];
    }

    public void setCriterionList(int i, org.poscomp.eqclinic.ws.Criterion _value) {
        this.criterionList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Grading)) return false;
        Grading other = (Grading) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.criteriaDescription==null && other.getCriteriaDescription()==null) || 
             (this.criteriaDescription!=null &&
              this.criteriaDescription.equals(other.getCriteriaDescription()))) &&
            ((this.criterionList==null && other.getCriterionList()==null) || 
             (this.criterionList!=null &&
              java.util.Arrays.equals(this.criterionList, other.getCriterionList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCriteriaDescription() != null) {
            _hashCode += getCriteriaDescription().hashCode();
        }
        if (getCriterionList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCriterionList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCriterionList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Grading.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "Grading"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criteriaDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CriteriaDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criterionList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CriterionList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "Criterion"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
