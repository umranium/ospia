/**
 * Information.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public class Information  implements java.io.Serializable {
    private org.poscomp.eqclinic.ws.TemplateField title;

    private org.poscomp.eqclinic.ws.TemplateField description;

    private org.poscomp.eqclinic.ws.TemplateField content;

    public Information() {
    }

    public Information(
           org.poscomp.eqclinic.ws.TemplateField title,
           org.poscomp.eqclinic.ws.TemplateField description,
           org.poscomp.eqclinic.ws.TemplateField content) {
           this.title = title;
           this.description = description;
           this.content = content;
    }


    /**
     * Gets the title value for this Information.
     * 
     * @return title
     */
    public org.poscomp.eqclinic.ws.TemplateField getTitle() {
        return title;
    }


    /**
     * Sets the title value for this Information.
     * 
     * @param title
     */
    public void setTitle(org.poscomp.eqclinic.ws.TemplateField title) {
        this.title = title;
    }


    /**
     * Gets the description value for this Information.
     * 
     * @return description
     */
    public org.poscomp.eqclinic.ws.TemplateField getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Information.
     * 
     * @param description
     */
    public void setDescription(org.poscomp.eqclinic.ws.TemplateField description) {
        this.description = description;
    }


    /**
     * Gets the content value for this Information.
     * 
     * @return content
     */
    public org.poscomp.eqclinic.ws.TemplateField getContent() {
        return content;
    }


    /**
     * Sets the content value for this Information.
     * 
     * @param content
     */
    public void setContent(org.poscomp.eqclinic.ws.TemplateField content) {
        this.content = content;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Information)) return false;
        Information other = (Information) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.content==null && other.getContent()==null) || 
             (this.content!=null &&
              this.content.equals(other.getContent())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getContent() != null) {
            _hashCode += getContent().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Information.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "Information"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Title"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("content");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Content"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
