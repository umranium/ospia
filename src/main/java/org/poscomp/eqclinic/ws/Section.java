/**
 * Section.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public class Section  implements java.io.Serializable {
    private java.lang.String sectionId;

    private org.poscomp.eqclinic.ws.TemplateField sectionType;

    private org.poscomp.eqclinic.ws.TemplateField sectionTitle;

    private org.poscomp.eqclinic.ws.TemplateField sectionDescription;

    private java.lang.String sectionColour;

    public Section() {
    }

    public Section(
           java.lang.String sectionId,
           org.poscomp.eqclinic.ws.TemplateField sectionType,
           org.poscomp.eqclinic.ws.TemplateField sectionTitle,
           org.poscomp.eqclinic.ws.TemplateField sectionDescription,
           java.lang.String sectionColour) {
           this.sectionId = sectionId;
           this.sectionType = sectionType;
           this.sectionTitle = sectionTitle;
           this.sectionDescription = sectionDescription;
           this.sectionColour = sectionColour;
    }


    /**
     * Gets the sectionId value for this Section.
     * 
     * @return sectionId
     */
    public java.lang.String getSectionId() {
        return sectionId;
    }


    /**
     * Sets the sectionId value for this Section.
     * 
     * @param sectionId
     */
    public void setSectionId(java.lang.String sectionId) {
        this.sectionId = sectionId;
    }


    /**
     * Gets the sectionType value for this Section.
     * 
     * @return sectionType
     */
    public org.poscomp.eqclinic.ws.TemplateField getSectionType() {
        return sectionType;
    }


    /**
     * Sets the sectionType value for this Section.
     * 
     * @param sectionType
     */
    public void setSectionType(org.poscomp.eqclinic.ws.TemplateField sectionType) {
        this.sectionType = sectionType;
    }


    /**
     * Gets the sectionTitle value for this Section.
     * 
     * @return sectionTitle
     */
    public org.poscomp.eqclinic.ws.TemplateField getSectionTitle() {
        return sectionTitle;
    }


    /**
     * Sets the sectionTitle value for this Section.
     * 
     * @param sectionTitle
     */
    public void setSectionTitle(org.poscomp.eqclinic.ws.TemplateField sectionTitle) {
        this.sectionTitle = sectionTitle;
    }


    /**
     * Gets the sectionDescription value for this Section.
     * 
     * @return sectionDescription
     */
    public org.poscomp.eqclinic.ws.TemplateField getSectionDescription() {
        return sectionDescription;
    }


    /**
     * Sets the sectionDescription value for this Section.
     * 
     * @param sectionDescription
     */
    public void setSectionDescription(org.poscomp.eqclinic.ws.TemplateField sectionDescription) {
        this.sectionDescription = sectionDescription;
    }


    /**
     * Gets the sectionColour value for this Section.
     * 
     * @return sectionColour
     */
    public java.lang.String getSectionColour() {
        return sectionColour;
    }


    /**
     * Sets the sectionColour value for this Section.
     * 
     * @param sectionColour
     */
    public void setSectionColour(java.lang.String sectionColour) {
        this.sectionColour = sectionColour;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Section)) return false;
        Section other = (Section) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sectionId==null && other.getSectionId()==null) || 
             (this.sectionId!=null &&
              this.sectionId.equals(other.getSectionId()))) &&
            ((this.sectionType==null && other.getSectionType()==null) || 
             (this.sectionType!=null &&
              this.sectionType.equals(other.getSectionType()))) &&
            ((this.sectionTitle==null && other.getSectionTitle()==null) || 
             (this.sectionTitle!=null &&
              this.sectionTitle.equals(other.getSectionTitle()))) &&
            ((this.sectionDescription==null && other.getSectionDescription()==null) || 
             (this.sectionDescription!=null &&
              this.sectionDescription.equals(other.getSectionDescription()))) &&
            ((this.sectionColour==null && other.getSectionColour()==null) || 
             (this.sectionColour!=null &&
              this.sectionColour.equals(other.getSectionColour())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSectionId() != null) {
            _hashCode += getSectionId().hashCode();
        }
        if (getSectionType() != null) {
            _hashCode += getSectionType().hashCode();
        }
        if (getSectionTitle() != null) {
            _hashCode += getSectionTitle().hashCode();
        }
        if (getSectionDescription() != null) {
            _hashCode += getSectionDescription().hashCode();
        }
        if (getSectionColour() != null) {
            _hashCode += getSectionColour().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Section.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "Section"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sectionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SectionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sectionType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SectionType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sectionTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SectionTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sectionDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SectionDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sectionColour");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SectionColour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
