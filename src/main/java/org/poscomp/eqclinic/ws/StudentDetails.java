/**
 * StudentDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public class StudentDetails  implements java.io.Serializable {
    private org.poscomp.eqclinic.ws.TemplateField title;

    private org.poscomp.eqclinic.ws.TemplateField description;

    private org.poscomp.eqclinic.ws.TemplateField studentId;

    private java.lang.String[] whoCanEdit;

    public StudentDetails() {
    }

    public StudentDetails(
           org.poscomp.eqclinic.ws.TemplateField title,
           org.poscomp.eqclinic.ws.TemplateField description,
           org.poscomp.eqclinic.ws.TemplateField studentId,
           java.lang.String[] whoCanEdit) {
           this.title = title;
           this.description = description;
           this.studentId = studentId;
           this.whoCanEdit = whoCanEdit;
    }


    /**
     * Gets the title value for this StudentDetails.
     * 
     * @return title
     */
    public org.poscomp.eqclinic.ws.TemplateField getTitle() {
        return title;
    }


    /**
     * Sets the title value for this StudentDetails.
     * 
     * @param title
     */
    public void setTitle(org.poscomp.eqclinic.ws.TemplateField title) {
        this.title = title;
    }


    /**
     * Gets the description value for this StudentDetails.
     * 
     * @return description
     */
    public org.poscomp.eqclinic.ws.TemplateField getDescription() {
        return description;
    }


    /**
     * Sets the description value for this StudentDetails.
     * 
     * @param description
     */
    public void setDescription(org.poscomp.eqclinic.ws.TemplateField description) {
        this.description = description;
    }


    /**
     * Gets the studentId value for this StudentDetails.
     * 
     * @return studentId
     */
    public org.poscomp.eqclinic.ws.TemplateField getStudentId() {
        return studentId;
    }


    /**
     * Sets the studentId value for this StudentDetails.
     * 
     * @param studentId
     */
    public void setStudentId(org.poscomp.eqclinic.ws.TemplateField studentId) {
        this.studentId = studentId;
    }


    /**
     * Gets the whoCanEdit value for this StudentDetails.
     * 
     * @return whoCanEdit
     */
    public java.lang.String[] getWhoCanEdit() {
        return whoCanEdit;
    }


    /**
     * Sets the whoCanEdit value for this StudentDetails.
     * 
     * @param whoCanEdit
     */
    public void setWhoCanEdit(java.lang.String[] whoCanEdit) {
        this.whoCanEdit = whoCanEdit;
    }

    public java.lang.String getWhoCanEdit(int i) {
        return this.whoCanEdit[i];
    }

    public void setWhoCanEdit(int i, java.lang.String _value) {
        this.whoCanEdit[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StudentDetails)) return false;
        StudentDetails other = (StudentDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.studentId==null && other.getStudentId()==null) || 
             (this.studentId!=null &&
              this.studentId.equals(other.getStudentId()))) &&
            ((this.whoCanEdit==null && other.getWhoCanEdit()==null) || 
             (this.whoCanEdit!=null &&
              java.util.Arrays.equals(this.whoCanEdit, other.getWhoCanEdit())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getStudentId() != null) {
            _hashCode += getStudentId().hashCode();
        }
        if (getWhoCanEdit() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getWhoCanEdit());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getWhoCanEdit(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StudentDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "StudentDetails"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Title"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("studentId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StudentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("whoCanEdit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "WhoCanEdit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
