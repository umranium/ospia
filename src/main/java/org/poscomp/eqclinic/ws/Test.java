package org.poscomp.eqclinic.ws;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Call;
import org.apache.axis.client.Stub;
//import org.json.JSONArray;
//import org.json.JSONException;
import org.json.JSONArray;
import org.json.JSONException;

public class Test {

	public static void main(String[] args) {

		String socaAnswer = "[{'qid':11,'qvalue':''},{'qid':1,'qvalue':'P'},{'qid':8,'qvalue':''},{'qid':41,'qvalue':'2'},{'qid':2,'qvalue':''},{'qid':7,'qvalue':'P'},{'qid':10,'qvalue':'P'},{'qid':5,'qvalue':'C,E'},{'qid':4,'qvalue':'P-'},{'qid':13,'qvalue':'Warm and friendly manner. I felt student focused almost entirely on chest pain ... a few times other symptoms were given but glossed over and didnt go back to request further information. Mentioned dead was deceased: no reaction. Covered most points (a bit sketchy) but I think student needed to delve deeper for a greater understanding. Good job though.'}]";
        try {
			JSONArray answerArray = new JSONArray(socaAnswer);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
//		Test test = new Test();
//		// test.saveAssessment();
//
////		String[] answers = {
////				"How you felt the interview went for you at the time?",
////				"managing the interview was smooth, felt it really went well",
////				"How this compares with the grade and comments entered by the assessor?",
////				"fairly reflects on the interview, made a good point about being more personal with the simulated patient (and it is something to work on)",
////				"What this means to you for how you will continue to develop your communication skills?",
////				"Being more personal and breaking the ice at the beginning. May have been too formal." };
////		test.update("z3457754", "1", "2HH77DL", answers); // course group
//
//		 String[] answers = {
//		 "How you felt the interview went for you at the time?",
//		 "I felt it went well. There were particular areas i felt i forgot to ask but managed to address the required areas. I made i mistake however, and accidentally ticked the areas i felt were achieved, so will try to ask and fix this",
//		 "How this compares with the grade and comments entered by the assessor?",
//		 "The assessor's grading was incredibly harsh and comments section was not adequate enough to enable me to actually improve as not many comments were given. I feel less confident after this interview despite receiving atleast Ps in all my assessments previously and receiving a P+ in my last hospital assessment, as i feel the assessor was unreasonably harsh and placed unachievable expectations on my level as only a second year student. I do agree certain areas need to be improved, however, a fail grade or P- grade i do not agree with, especially since the marked identified areas that 'i did not achieve' despite me ensuring i did those things eg. signposting, summarising where i was told to summarise (after presenting complaint rather than at the end) and saying things like 'that would be tough', 'that must have been uncomfortable' and ensuring i showed empaty.",
//		 "What this means to you for how you will continue to develop your communication skills?",
//		 "I will try to discuss my feedback with my clinical skills teacher and also clarify what we are actually expected to do in a history as despite taking on previous feedback and catering my approach to ensure i addressed the feedback, i received such harsh criticism."
//		 };
//		 test.update("z1111112", "1", "4HCQCH3", answers);

	}

	public void update(String studentId, String phase, String receiptNum,
			String[] answers) {

		AllDataServiceLocator serviceLocator = new AllDataServiceLocator();

		try {
			AllData service = serviceLocator.getDomino();
			((Stub) service)._setProperty(Call.USERNAME_PROPERTY,
					"WebServiceOSPIA");
			((Stub) service)._setProperty(Call.PASSWORD_PROPERTY,
					"WebServiceOSPIA");

			AssessmentList assessmentList = service.getAssessments(studentId,
					phase);
			Assessment[] assessments = assessmentList.getAssessmentList();

			for (int i = 0; i < assessments.length; i++) {
				String receiptNumber = assessments[i].getReceiptNumber();
				if (receiptNum.equals(receiptNumber)) {
					String newReceiptNum = addReflectionToSOCA(assessments[i],
							answers);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String addReflectionToSOCA2(Assessment assessment, String[] answers) {

		AllDataServiceLocator serviceLocator = new AllDataServiceLocator();

		AllData service;
		try {
			service = serviceLocator.getDomino();

			((Stub) service)._setProperty(Call.USERNAME_PROPERTY,
					"WebServiceOSPIA");
			((Stub) service)._setProperty(Call.PASSWORD_PROPERTY,
					"WebServiceOSPIA");

			CriterionGrade[] criterionGradeList = assessment
					.getGradedCriterionList();
			CriterionGrade reflectGrade = criterionGradeList[5];
			reflectGrade.setGrade(" ");
			reflectGrade.setGradeReason(answers);
			criterionGradeList[5] = reflectGrade;

			// CriterionGrade[] criterionGradeList =
			// assessment.getGradedCriterionList();
			// CriterionGrade g1 = criterionGradeList[0];
			// CriterionGrade g2 = criterionGradeList[1];
			// CriterionGrade g3 = criterionGradeList[2];
			// CriterionGrade g4 = criterionGradeList[3];
			//
			// g1.setGradeReason(null);
			// g2.setGradeReason(null);
			// g3.setGradeReason(null);
			// g4.setGradeReason(null);
			//
			//
			// criterionGradeList[0] = g1;
			// criterionGradeList[1] = g2;
			// criterionGradeList[2] = g3;
			// criterionGradeList[3] = g4;

			// CriterionGrade[] criterionGradeList =
			// assessment.getGradedCriterionList();
			// CriterionGrade[] criterionGradeListNew = new CriterionGrade[6];
			// criterionGradeListNew[0] = criterionGradeList[0];
			// criterionGradeListNew[1] = criterionGradeList[1];
			// criterionGradeListNew[2] = criterionGradeList[2];
			// criterionGradeListNew[3] = criterionGradeList[3];
			// criterionGradeListNew[4] = criterionGradeList[4];
			//
			// CriterionGrade reflectGrade = new CriterionGrade();
			// reflectGrade.setGrade(" ");
			// reflectGrade.setGradeReason(answers);
			// criterionGradeListNew[5] = reflectGrade;

			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
			String currentDate = sdf.format(new Date());
			assessment.setDateStatusModified(currentDate);

			assessment.setCriterionList(createCritionLong());
			// assessment.setGradedCriterionList(criterionGradeListNew);
			assessment.setGradedCriterionList(criterionGradeList);
			assessment.setAssessmentType("OSPIA");

			SubmitAssessmentResponse response = service
					.submitAssessment(assessment);
			System.out.println(response.getMessage());
			System.out.println(response.getMissingCount());
			System.out.println(response.getReceiptNumber());

			return response.getReceiptNumber();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public String addReflectionToSOCA(Assessment assessment, String[] answers) {

		AllDataServiceLocator serviceLocator = new AllDataServiceLocator();

		AllData service;
		try {
			service = serviceLocator.getDomino();

			((Stub) service)._setProperty(Call.USERNAME_PROPERTY,
					"WebServiceOSPIA");
			((Stub) service)._setProperty(Call.PASSWORD_PROPERTY,
					"WebServiceOSPIA");

			CriterionGrade[] criterionGradeList = assessment
					.getGradedCriterionList();
			CriterionGrade reflectGrade = criterionGradeList[5];
			reflectGrade.setGrade(" ");
			reflectGrade.setGradeReason(answers);
			criterionGradeList[5] = reflectGrade;

			// CriterionGrade[] criterionGradeList =
			// assessment.getGradedCriterionList();
			// CriterionGrade g1 = criterionGradeList[0];
			// CriterionGrade g2 = criterionGradeList[1];
			// CriterionGrade g3 = criterionGradeList[2];
			// CriterionGrade g4 = criterionGradeList[3];
			//
			// g1.setGradeReason(null);
			// g2.setGradeReason(null);
			// g3.setGradeReason(null);
			// g4.setGradeReason(null);
			//
			//
			// criterionGradeList[0] = g1;
			// criterionGradeList[1] = g2;
			// criterionGradeList[2] = g3;
			// criterionGradeList[3] = g4;

			// CriterionGrade[] criterionGradeList =
			// assessment.getGradedCriterionList();
			// CriterionGrade[] criterionGradeListNew = new CriterionGrade[6];
			// criterionGradeListNew[0] = criterionGradeList[0];
			// criterionGradeListNew[1] = criterionGradeList[1];
			// criterionGradeListNew[2] = criterionGradeList[2];
			// criterionGradeListNew[3] = criterionGradeList[3];
			// criterionGradeListNew[4] = criterionGradeList[4];
			//
			// CriterionGrade reflectGrade = new CriterionGrade();
			// reflectGrade.setGrade(" ");
			// reflectGrade.setGradeReason(answers);
			// criterionGradeListNew[5] = reflectGrade;

			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
			String currentDate = sdf.format(new Date());
			assessment.setDateStatusModified(currentDate);

			assessment.setCriterionList(createCritionLong());
			// assessment.setGradedCriterionList(criterionGradeListNew);
			assessment.setGradedCriterionList(criterionGradeList);
			assessment.setAssessmentType("OSPIA");

			SubmitAssessmentResponse response = service
					.submitAssessment(assessment);
			System.out.println(response.getMessage());
			System.out.println(response.getMissingCount());
			System.out.println(response.getReceiptNumber());

			return response.getReceiptNumber();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public void getAssessments() {
		AllDataServiceLocator serviceLocator = new AllDataServiceLocator();

		try {
			AllData service = serviceLocator.getDomino();
			((Stub) service)._setProperty(Call.USERNAME_PROPERTY,
					"WebServiceOSPIA");
			((Stub) service)._setProperty(Call.PASSWORD_PROPERTY,
					"WebServiceOSPIA");

			AssessmentList assessmentList = service.getAssessments("z1111112",
					"1");
			Assessment[] assessments = assessmentList.getAssessmentList();
			System.out.println(assessments.length);

			for (int i = 0; i < assessments.length; i++) {
				String receiptNumber = assessments[i].getReceiptNumber();
				if ("XHCQ9F4".equals(receiptNumber)) {
					System.out.println("assessment Id: "
							+ assessments[i].getCriterionList().length);
					saveAssessment(assessments[i]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveAssessment(Assessment assessment) {
		AllDataServiceLocator serviceLocator = new AllDataServiceLocator();

		try {
			AllData service = serviceLocator.getDomino();
			// to use Basic HTTP Authentication:
			((Stub) service)._setProperty(Call.USERNAME_PROPERTY,
					"WebServiceOSPIA");
			((Stub) service)._setProperty(Call.PASSWORD_PROPERTY,
					"WebServiceOSPIA");

			assessment.setAssessmentTitle("changed title with new interface");
			SubmitAssessmentResponse response = service
					.submitAssessment(assessment);
			System.out.println(response.getMessage());
			System.out.println(response.getMissingCount());
			System.out.println(response.getReceiptNumber());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void saveAssessment() {

		AllDataServiceLocator serviceLocator = new AllDataServiceLocator();

		try {
			AllData service = serviceLocator.getDomino();
			// to use Basic HTTP Authentication:
			((Stub) service)._setProperty(Call.USERNAME_PROPERTY,
					"WebServiceOSPIA");
			((Stub) service)._setProperty(Call.PASSWORD_PROPERTY,
					"WebServiceOSPIA");

			Assessment assessment = new Assessment();
			// set the student Id
			assessment.setStudentId("z1111112");

			// set assessor Id (SP name, or tutor name)
			assessment.setAssessorId("");
			AssessorDetails assessorDetails = new AssessorDetails();

			TemplateField assessorName = new TemplateField();
			assessorName.setLabel("Ian");
			assessorName.setValue("Liu");
			assessorDetails.setNameOfAssessor(assessorName);
			assessment.setAssessorDetails(assessorDetails);
			// set the status
			assessment.setStatus("Completed");

			// set the receipt number
			assessment.setReceiptNumber(null);

			assessment.setAssessmentType("OSPIA");

			// set template key
			// assessment.setTemplateUniqueKey("EMEDA395ZQ");

			// set submission date
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

			String currentDate = sdf.format(new Date());
			System.out.println(currentDate);
			assessment.setSubmissionDate(currentDate);
			// // set date modified
			// assessment.setDateModified(currentDate);

			// set date modified
			assessment.setDateStatusModified(currentDate);

			assessment.setCriterionList(createCritionLong());
			assessment.setGradedCriterionList(getGrad("F"));

			assessment
					.setAssessmentTitle("Patient-student communication assessment");

			SubmitAssessmentResponse response = service
					.submitAssessment(assessment);

			System.out.println(response.getMessage());
			System.out.println(response.getMissingCount());
			System.out.println(response.getReceiptNumber());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Criterion[] createCrition() {

		CriterionGrade grade_f = new CriterionGrade();
		grade_f.setGrade("F");
		CriterionGrade grade_pm = new CriterionGrade();
		grade_f.setGrade("P-");
		CriterionGrade grade_p = new CriterionGrade();
		grade_f.setGrade("P");
		CriterionGrade grade_pp = new CriterionGrade();
		grade_f.setGrade("P+");
		CriterionGrade[] totalGradeList = { grade_f, grade_pm, grade_p,
				grade_pp };

		CriterionGrade focus_1 = new CriterionGrade();
		focus_1.setGrade("Provide structure");
		CriterionGrade focus_2 = new CriterionGrade();
		focus_2.setGrade("Gather information");
		CriterionGrade focus_3 = new CriterionGrade();
		focus_3.setGrade("Developing rapport");
		CriterionGrade focus_4 = new CriterionGrade();
		focus_4.setGrade("Understanding needs");
		CriterionGrade[] overallGrades = { focus_1, focus_2, focus_3, focus_4 };

		Criterion cris_1 = new Criterion();
		cris_1.setCriterionNum("1");
		TemplateField title_1 = new TemplateField();
		title_1.setLabel("Provide structure");
		title_1.setValue("Provide structure");
		cris_1.setTitle(title_1);
		cris_1.setDescription(title_1);
		cris_1.setCriterionGradeList(totalGradeList);

		Criterion cris_2 = new Criterion();
		cris_2.setCriterionNum("2");
		TemplateField title_2 = new TemplateField();
		title_2.setLabel("Gather information");
		title_2.setValue("Gather information");
		cris_2.setTitle(title_2);
		cris_2.setDescription(title_2);
		cris_2.setCriterionGradeList(totalGradeList);

		Criterion cris_3 = new Criterion();
		cris_3.setCriterionNum("3");
		TemplateField title_3 = new TemplateField();
		title_3.setLabel("Developing rapport");
		title_3.setValue("Developing rapport");
		cris_3.setTitle(title_3);
		cris_3.setDescription(title_3);
		cris_3.setCriterionGradeList(totalGradeList);

		Criterion cris_4 = new Criterion();
		cris_4.setCriterionNum("4");
		TemplateField title_4 = new TemplateField();
		title_4.setLabel("Understanding needs");
		title_4.setValue("Understanding needs");
		cris_4.setTitle(title_4);
		cris_4.setDescription(title_4);
		cris_4.setCriterionGradeList(totalGradeList);

		Criterion cris_5 = new Criterion();
		cris_5.setCriterionNum("5");
		TemplateField title_5 = new TemplateField();
		title_5.setLabel("Overall feedback");
		title_5.setValue("Overall feedback");
		cris_5.setTitle(title_5);
		cris_5.setDescription(title_5);
		cris_5.setCriterionGradeList(overallGrades);

		Criterion[] cris = { cris_1, cris_2, cris_3, cris_4, cris_5 };
		return cris;
	}

	public Criterion[] createCritionLong() {

		CriterionGrade grade_f = new CriterionGrade();
		grade_f.setGrade("F");
		CriterionGrade grade_pm = new CriterionGrade();
		grade_f.setGrade("P-");
		CriterionGrade grade_p = new CriterionGrade();
		grade_f.setGrade("P");
		CriterionGrade grade_pp = new CriterionGrade();
		grade_f.setGrade("P+");
		CriterionGrade[] totalGradeList = { grade_f, grade_pm, grade_p,
				grade_pp };

		CriterionGrade focus_1 = new CriterionGrade();
		focus_1.setGrade("Provide structure");
		CriterionGrade focus_2 = new CriterionGrade();
		focus_2.setGrade("Gather information");
		CriterionGrade focus_3 = new CriterionGrade();
		focus_3.setGrade("Developing rapport");
		CriterionGrade focus_4 = new CriterionGrade();
		focus_4.setGrade("Understanding needs");
		CriterionGrade[] overallGrades = { focus_1, focus_2, focus_3, focus_4 };

		String question1 = "How you felt the interview went for you at the time?";
		String question2 = "How this compares with the grade and comments entered by the assessor?";
		String question3 = "What this means to you for how you will continue to develop your communication skills?";

		CriterionGrade ref_1 = new CriterionGrade();
		ref_1.setGrade(question1);
		CriterionGrade ref_2 = new CriterionGrade();
		ref_2.setGrade(question2);
		CriterionGrade ref_3 = new CriterionGrade();
		ref_3.setGrade(question3);
		CriterionGrade[] reflectionGrades = { ref_1, ref_2, ref_3 };

		Criterion cris_1 = new Criterion();
		cris_1.setCriterionNum("1");
		TemplateField title_1 = new TemplateField();
		title_1.setLabel("Provide structure");
		title_1.setValue("Provide structure");
		cris_1.setTitle(title_1);
		cris_1.setDescription(title_1);
		cris_1.setCriterionGradeList(totalGradeList);

		Criterion cris_2 = new Criterion();
		cris_2.setCriterionNum("2");
		TemplateField title_2 = new TemplateField();
		title_2.setLabel("Gather information");
		title_2.setValue("Gather information");
		cris_2.setTitle(title_2);
		cris_2.setDescription(title_2);
		cris_2.setCriterionGradeList(totalGradeList);

		Criterion cris_3 = new Criterion();
		cris_3.setCriterionNum("3");
		TemplateField title_3 = new TemplateField();
		title_3.setLabel("Developing rapport");
		title_3.setValue("Developing rapport");
		cris_3.setTitle(title_3);
		cris_3.setDescription(title_3);
		cris_3.setCriterionGradeList(totalGradeList);

		Criterion cris_4 = new Criterion();
		cris_4.setCriterionNum("4");
		TemplateField title_4 = new TemplateField();
		title_4.setLabel("Understanding needs");
		title_4.setValue("Understanding needs");
		cris_4.setTitle(title_4);
		cris_4.setDescription(title_4);
		cris_4.setCriterionGradeList(totalGradeList);

		Criterion cris_5 = new Criterion();
		cris_5.setCriterionNum("5");
		TemplateField title_5 = new TemplateField();
		title_5.setLabel("Overall feedback");
		title_5.setValue("Overall feedback");
		cris_5.setTitle(title_5);
		cris_5.setDescription(title_5);
		cris_5.setCriterionGradeList(overallGrades);

		Criterion cris_6 = new Criterion();
		cris_6.setCriterionNum("6");
		TemplateField title_6 = new TemplateField();
		title_6.setLabel("Reflection answer");
		title_6.setValue("Reflection answer");
		cris_6.setTitle(title_6);
		cris_6.setDescription(title_6);
		cris_6.setCriterionGradeList(reflectionGrades);

		// Criterion[] critionList = assessment.getCriterionList();
		//
		// Criterion[] newCritionList = new Criterion[critionList.length+1];
		//
		// for (int i = 0; i < critionList.length; i++) {
		// newCritionList[i] = critionList[i];
		// }
		//
		// String question1=
		// "How you felt the interview went for you at the time?";
		// String question2=
		// "How this compares with the grade and comments entered by the assessor?";
		// String question3=
		// "What this means to you for how you will continue to develop your communication skills?";
		//
		// CriterionGrade focus_1 = new CriterionGrade();
		// focus_1.setGrade(question1);
		// CriterionGrade focus_2 = new CriterionGrade();
		// focus_2.setGrade(question2);
		// CriterionGrade focus_3 = new CriterionGrade();
		// focus_3.setGrade(question3);
		// CriterionGrade[] reflectionGrades = { focus_1, focus_2, focus_3};
		//
		Criterion[] cris = { cris_1, cris_2, cris_3, cris_4, cris_5, cris_6 };
		return cris;
	}

	public CriterionGrade[] getGrad(String grade) {

		String reasons[] = { "reason 1", "reason 4" };

		String reasons_ff[] = { "You need to focus on following skills: " };

		// String reflection[] = {"reflection 1", "reflection 4"};
		String reflection[] = {};

		CriterionGrade grade_f = new CriterionGrade();
		grade_f.setGrade("F");
		grade_f.setGradeReason(reasons);

		CriterionGrade grade_f_copy = new CriterionGrade();
		grade_f_copy.setGrade("F");
		grade_f_copy.setGradeReason(reasons);

		CriterionGrade grade_pm = new CriterionGrade();
		grade_pm.setGrade("P-");
		grade_pm.setGradeReason(reasons);

		CriterionGrade grade_p = new CriterionGrade();
		grade_p.setGrade("P");
		grade_p.setGradeReason(reasons);

		CriterionGrade grade_pp = new CriterionGrade();
		grade_pp.setGrade("P+");
		grade_pp.setGradeReason(reasons);

		CriterionGrade grade_ff = new CriterionGrade();
		grade_ff.setGrade("Provide Structure;Gather information");
		grade_ff.setGradeReason(reasons_ff);

		CriterionGrade grade_reflection = new CriterionGrade();
		grade_reflection.setGrade("No Reflection");
		grade_reflection.setGradeReason(reflection);

		CriterionGrade[] grades = new CriterionGrade[6];

		if ("F".equals(grade)) {
			grades[0] = grade_f;
			grades[0].setCriterionNum("1");
			TemplateField tf1 = new TemplateField();
			tf1.setValue("Provide structure");
			grades[0].setTitle(tf1);

			grades[1] = grade_f_copy;
			grades[1].setCriterionNum("2");
			TemplateField tf2 = new TemplateField();
			tf2.setValue("Gather information");
			grades[1].setTitle(tf2);

			grades[2] = grade_p;
			grades[2].setCriterionNum("3");
			TemplateField tf3 = new TemplateField();
			tf3.setValue("Build relationships & developing rapport");
			grades[2].setTitle(tf3);

			grades[3] = grade_pp;
			grades[3].setCriterionNum("4");
			TemplateField tf4 = new TemplateField();
			tf4.setValue("Ensure a shared understanding of patient's needs and perspective/impact of problem");
			grades[3].setTitle(tf4);

			grades[4] = grade_ff;
			grades[4].setCriterionNum("5");
			TemplateField tf5 = new TemplateField();
			tf5.setValue("Overall feedback");
			grades[4].setTitle(tf5);

			grades[5] = grade_reflection;
			grades[5].setCriterionNum("6");
			TemplateField tf6 = new TemplateField();
			tf6.setValue("Reflection answer");
			grades[5].setTitle(tf6);

		} else if ("P-".equals(grade)) {
			grades[0] = grade_pm;
		} else if ("P".equals(grade)) {
			grades[0] = grade_p;
		} else if ("P+".equals(grade)) {
			grades[0] = grade_pp;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

		String currentDate = sdf.format(new Date());
		grades[0].setDateCreated(currentDate);
		grades[0].setDateModified(currentDate);
		grades[1].setDateCreated(currentDate);
		grades[1].setDateModified(currentDate);
		grades[2].setDateCreated(currentDate);
		grades[2].setDateModified(currentDate);
		grades[3].setDateCreated(currentDate);
		grades[3].setDateModified(currentDate);
		grades[4].setDateCreated(currentDate);
		grades[4].setDateModified(currentDate);
		grades[5].setDateCreated(currentDate);
		grades[5].setDateModified(currentDate);

		return grades;
	}

}
