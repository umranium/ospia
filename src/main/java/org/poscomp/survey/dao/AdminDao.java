package org.poscomp.survey.dao;

import org.poscomp.survey.domain.Admin;

public interface AdminDao {

    public void save(Admin admin);

    public Admin findByName(String name);

    public Admin findByEmail(String email);

    public Admin findByNameAndPassword(String name, String password);

    public Admin findById(int id);
}
