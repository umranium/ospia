package org.poscomp.survey.dao;

import java.util.List;

import org.poscomp.survey.domain.QuestionType;

public interface QuestionTypeDao {

    public QuestionType findByName(String name);
    
    public List<QuestionType> findAll();
    
}
