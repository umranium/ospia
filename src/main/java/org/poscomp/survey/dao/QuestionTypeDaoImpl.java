package org.poscomp.survey.dao;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.poscomp.survey.domain.QuestionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("questionTypeDao")
public class QuestionTypeDaoImpl implements QuestionTypeDao {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public QuestionType findByName(String name) {
        String hql = "from QuestionType as qt where qt.type=:name";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setString("name", name);
        QuestionType result = (QuestionType) query.uniqueResult();
        return result;
    }
    
    public List<QuestionType> findAll(){
        String hql = "from QuestionType";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        List<QuestionType> result = (List<QuestionType>) query.list();
        return result;
    }
    
}
