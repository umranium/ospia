package org.poscomp.survey.service;


import java.util.List;

import org.poscomp.survey.dao.AnswerDao;
import org.poscomp.survey.domain.Answer;
import org.poscomp.survey.domain.Survey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("answerService")
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    private AnswerDao answerDao;

    @Override
    public void save(Answer answer) {
        answerDao.save(answer);
    }

    @Override
    public Answer loadAnswerById(int id) {
        return answerDao.loadAnswerById(id);
    }

    @Override
    public Answer loadAnswerBySessionIdUserId(String sessionId, String userType, int surveyId) {
        List<Answer> answers = answerDao.loadAnswerBySessionId(sessionId);
        // for (Answer answer : answers) {
        if(answers!=null){
        for (int i = answers.size()-1; i >=0 ; i--) {
            Answer answer = answers.get(i);
            Survey survey = answer.getSurvey();
            if (survey.getId() == surveyId 
               && ((answer.getDoctor()!=null&&"doctor".equals(userType))
               || (answer.getPatient()!=null&&"patient".equals(userType))
               || (answer.getTeacher()!=null&&"teacher".equals(userType)))) {
                return answer;
            }
        }
        }
            
        // }

        return null;
    }

    @Override
    public Answer loadAnswerBySessionIdUserIdUsertype(String sessionId, String userType, int userId, int surveyId) {
        List<Answer> answers = answerDao.loadAnswerBySessionId(sessionId);
        Answer result = null;
        for (Answer answer : answers) {
            Survey survey = answer.getSurvey();
            if (survey.getId() == surveyId 
               && ((answer.getDoctor()!=null&&"doctor".equals(userType)&&userId==answer.getDoctor().getDoctorId())
               || (answer.getPatient()!=null&&"patient".equals(userType)&&userId==answer.getPatient().getPatientid())
               || (answer.getTeacher()!=null&&"teacher".equals(userType)&&userId==answer.getTeacher().getTeacherId()))) {
                result = answer;
            }
        }
        return result;
    }

    @Override
    public List<Answer> loadAnswer() {
        return answerDao.loadAnswer();
    }
}
