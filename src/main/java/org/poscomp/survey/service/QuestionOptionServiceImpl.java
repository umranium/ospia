package org.poscomp.survey.service;

import java.util.ArrayList;
import java.util.List;

import org.poscomp.survey.dao.QuestionOptionDao;
import org.poscomp.survey.domain.QuestionOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("questionOptionService")
public class QuestionOptionServiceImpl implements QuestionOptionService {

    @Autowired
    private QuestionOptionDao questionOptionDao;

    @Override
    public int saveQustionOption(QuestionOption option) {
        return questionOptionDao.saveQuestionOption(option);
    }

    @Override
    public List<Integer> saveQustionOptions(List<QuestionOption> options) {
        return questionOptionDao.saveQustionOptions(options);
    }

}
