package org.poscomp.survey.service;


import java.util.List;

import javax.inject.Named;

import org.poscomp.survey.domain.Survey;

public interface SurveyService {
    public void save(Survey survey, int type);

    public Survey findById(int id);

    public List<Survey> listByUser(int u_id);

    public Survey update(Survey survey);
    
    public void addQuestion(int surveyId, int questionId);
    
    public void removeQuestion(int surveyId, int questionId);
    
    public List<Survey> findAll(int pageNo, int pageSize);
    
    public void delete(Survey survey);
}
