package org.poscomp.survey.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.poscomp.survey.dao.QuestionDao;
import org.poscomp.survey.dao.SurveyDao;
import org.poscomp.survey.domain.Question;
import org.poscomp.survey.domain.Survey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("surveyService")
public class SurveyServiceImpl implements SurveyService {
    @Autowired
    private SurveyDao surveyDao;

    @Autowired
    private QuestionDao questionDao;

    public void save(Survey survey, int type) {

        if (type != 0) {
            if (survey.getId() != 0) {
                surveyDao.update(survey);
            } else {
                Set<Question> questions = survey.getQuestions();
                Set<Question> tempQuestions = new HashSet<Question>();
                String orderOfQuestions = "";
                for (Question question : questions) {
                    Question tempQuestion = questionDao.findById(question.getId());
                    tempQuestions.add(tempQuestion);

                    int id = question.getId();
                    if ("".equals(orderOfQuestions)) {
                        orderOfQuestions += id;
                    } else {
                        orderOfQuestions += "," + id;
                    }
                }
                survey.setOrderOfQuestions(orderOfQuestions);
                survey.setQuestions(tempQuestions);
                surveyDao.save(survey);
            }
        } else {
            surveyDao.save(survey);
        }

    }

    public Survey findById(int id) {
        return surveyDao.findById(id);
    }

    public List<Survey> listByUser(int u_id) {
        return surveyDao.listByUser(u_id);
    }

    public Survey update(Survey survey) {
        Survey entity = surveyDao.findById(survey.getId());
        entity.setTitle(survey.getTitle());
        entity.setDescription(survey.getDescription());
        return entity;
    }

    public void addQuestion(int surveyId, int questionId) {

        Survey survey = findById(surveyId);
        Question question = questionDao.findById(questionId);

        Set<Question> questions = survey.getQuestions();
        questions.add(question);

        String orderOfQuestions = survey.getOrderOfQuestions();

        if ("".equals(orderOfQuestions)) {
            orderOfQuestions += question.getId();
        } else {
            orderOfQuestions += "," + question.getId();
        }

        survey.setOrderOfQuestions(orderOfQuestions);
        survey.setQuestions(questions);
        save(survey, 0);

    }


    @Override
    public void removeQuestion(int surveyId, int questionId) {
        Survey survey = findById(surveyId);
        Set<Question> questions = survey.getQuestions();

        Question question = null;
        for (Question q : questions) {
            if (q.getId() == questionId) {
                question = q;
            }
        }

        questions.remove(question);
        String orderOfQuestions = survey.getOrderOfQuestions();

        String[] quesitonIds = orderOfQuestions.split(",");
        String newOrderOfQuestions = "";

        for (String idStr : quesitonIds) {
            int id = Integer.parseInt(idStr);
            if (questionId != id) {
                if ("".equals(newOrderOfQuestions)) {
                    newOrderOfQuestions += id;
                } else {
                    newOrderOfQuestions += "," + id;
                }
            }
        }

        survey.setOrderOfQuestions(newOrderOfQuestions);
        survey.setQuestions(questions);
        save(survey, 0);
    }

    @Override
    public List<Survey> findAll(int pageNo, int pageSize) {
        return surveyDao.findAll(pageNo, pageSize);
    }

    @Override
    public void delete(Survey survey) {
        surveyDao.delete(survey);
    }
}
