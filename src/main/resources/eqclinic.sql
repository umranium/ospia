/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50539
Source Host           : localhost:3306
Source Database       : eqclinic

Target Server Type    : MYSQL
Target Server Version : 50539
File Encoding         : 65001

Date: 2015-09-27 13:39:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_c0r9atamxvbhjjvy5j8da1kam` (`email`),
  UNIQUE KEY `UK_gfn44sntic2k93auag97juyij` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'ianliu0420@gmail.com', '111', 'ian@gmail.com');

-- ----------------------------
-- Table structure for `application`
-- ----------------------------
DROP TABLE IF EXISTS `application`;
CREATE TABLE `application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applyAt` bigint(20) DEFAULT NULL,
  `aptId` int(11) DEFAULT NULL,
  `doctorId` int(11) DEFAULT NULL,
  `result` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of application
-- ----------------------------
INSERT INTO `application` VALUES ('32', '1443324546389', '29', '11', '1');
INSERT INTO `application` VALUES ('33', '1443324551096', '30', '11', '1');
INSERT INTO `application` VALUES ('34', '1443324718832', '31', '11', '1');

-- ----------------------------
-- Table structure for `appointment`
-- ----------------------------
DROP TABLE IF EXISTS `appointment`;
CREATE TABLE `appointment` (
  `aptid` int(11) NOT NULL AUTO_INCREMENT,
  `apptime` bigint(20) DEFAULT NULL,
  `aptdate` date DEFAULT NULL,
  `archId` varchar(255) DEFAULT NULL,
  `archIdDocBaseline` varchar(255) DEFAULT NULL,
  `archIdPatBaseline` varchar(255) DEFAULT NULL,
  `cancelreason` varchar(10000) DEFAULT NULL,
  `canceltime` bigint(20) DEFAULT NULL,
  `confirmtime` bigint(20) DEFAULT NULL,
  `createtime` bigint(20) DEFAULT NULL,
  `doctorId` int(11) DEFAULT NULL,
  `doctorLoginAt` bigint(20) DEFAULT NULL,
  `doctorLogoutAt` bigint(20) DEFAULT NULL,
  `doctorRecTime` bigint(20) DEFAULT NULL,
  `doctorSessionIdBaseline` varchar(255) DEFAULT NULL,
  `doctorname` varchar(255) DEFAULT NULL,
  `endtime` bigint(20) DEFAULT NULL,
  `hasDonwload` int(11) DEFAULT NULL,
  `patientid` int(11) DEFAULT NULL,
  `patientLoginAt` bigint(20) DEFAULT NULL,
  `patientLogoutAt` bigint(20) DEFAULT NULL,
  `patientRecTime` bigint(20) DEFAULT NULL,
  `patientSessionIdBaseline` varchar(255) DEFAULT NULL,
  `patientname` varchar(255) DEFAULT NULL,
  `scenario` int(11) DEFAULT NULL,
  `scenarioCode` varchar(255) DEFAULT NULL,
  `sessionId` varchar(255) DEFAULT NULL,
  `sessionType` int(11) DEFAULT NULL,
  `startRecTime` bigint(20) DEFAULT NULL,
  `starttime` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `stopRecTime` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`aptid`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of appointment
-- ----------------------------
INSERT INTO `appointment` VALUES ('29', '1443324546389', '2015-10-01', null, null, null, null, '0', '1443324673518', '1443324504555', '11', null, null, null, '1_MX40NDc5NTM2Mn5-MTQ0MzMyNDUwODE5OH4raytDck02WVNyeDA5d1dQYVFObm9DQm5-fg', 'Tom Cross', '1443655800000', '0', '5', null, null, null, '2_MX40NDc5NTM2Mn5-MTQ0MzMyNDUwOTI1MH4yaTdIamcrZlR2bHVLMmpDRERiZUJzNlN-fg', 'Ian Liu', '1', '#101', '2_MX40NDc5NTM2Mn5-MTQ0MzMyNDUwNzA2NH53eGZjTlNNVHRCakhpU1lOQURnYlhpU2p-fg', '0', null, '1443654000000', '3', null);
INSERT INTO `appointment` VALUES ('30', '1443324551096', '2015-10-01', null, null, null, null, '0', '1443324673541', '1443324524009', '11', null, null, null, '1_MX40NDc5NTM2Mn5-MTQ0MzMyNDUyNjI2Nn5VT3pDc3RrZDZ5S08xUDA2aFNDSG1BUjl-fg', 'Tom Cross', '1443666600000', '0', '5', null, null, null, '1_MX40NDc5NTM2Mn5-MTQ0MzMyNDUyNzQ5N35vcVNtS083MjI4am9TUHNFbjQ3TDJ5c25-fg', 'Ian Liu', '1', '#101', '2_MX40NDc5NTM2Mn5-MTQ0MzMyNDUyNTIxMn5tZjRZWmgvRVAwTmtzZ3VTSmhsdDd1RE1-fg', '0', null, '1443664800000', '3', null);
INSERT INTO `appointment` VALUES ('31', '1443324718832', '2015-10-02', null, null, null, null, '0', '1443324886605', '1443324704165', '11', null, null, null, '2_MX40NDc5NTM2Mn5-MTQ0MzMyNDcwNjQ4NX55Nzh3Y2FCbnFFUmtKS3o4U3RjRWdNSC9-fg', 'Tom Cross', '1443753000000', '0', '5', null, null, null, '1_MX40NDc5NTM2Mn5-MTQ0MzMyNDcwNzU3NH5Ga0tCdnpxV0JuSkx6R2VyMTM1ODNMUnh-fg', 'Ian Liu', '1', '#101', '1_MX40NDc5NTM2Mn5-MTQ0MzMyNDcwNTQwN35rU2VBbCthd2t2TzB4a29HRWdMWnRnL3p-fg', '0', null, '1443751200000', '3', null);

-- ----------------------------
-- Table structure for `appointmentprogress`
-- ----------------------------
DROP TABLE IF EXISTS `appointmentprogress`;
CREATE TABLE `appointmentprogress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aptid` int(11) DEFAULT NULL,
  `hasFinish` int(11) DEFAULT NULL,
  `lastOperationAt` bigint(20) DEFAULT NULL,
  `sessionId` varchar(255) DEFAULT NULL,
  `stage` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of appointmentprogress
-- ----------------------------

-- ----------------------------
-- Table structure for `doctor`
-- ----------------------------
DROP TABLE IF EXISTS `doctor`;
CREATE TABLE `doctor` (
  `doctorId` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30) DEFAULT NULL,
  `lastname` varchar(30) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `lastLoginAt` bigint(20) DEFAULT NULL,
  `userId` varchar(20) DEFAULT NULL,
  `agreement` int(11) DEFAULT '0',
  PRIMARY KEY (`doctorId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doctor
-- ----------------------------
INSERT INTO `doctor` VALUES ('1', 'doctor1', 'test', '1', 'doctor1', 'doctor1', '1438824587497', '123123123', '0');
INSERT INTO `doctor` VALUES ('2', 'doctor2', 'test', '1', 'doctor2', 'doctor2', '1439784864367', '123123123', '0');
INSERT INTO `doctor` VALUES ('3', 'doctor3', 'test', '1', 'doctor3', 'doctor3', null, '123123123', '0');
INSERT INTO `doctor` VALUES ('4', 'doctor4', 'test', '1', 'doctor4', 'doctor4', null, '123123123', '0');
INSERT INTO `doctor` VALUES ('5', 'doctor5', 'test', '1', 'doctor5', 'doctor5', null, '123123123', '0');
INSERT INTO `doctor` VALUES ('6', 'Georgina', 'Gorman', '1', 'ggor4975@uni.sydney.edu.au', 'ggor4975', null, '123123123', '0');
INSERT INTO `doctor` VALUES ('7', 'Peter', 'Wang', '1', 'pwan7880@uni.sydney.edu.au', 'pwan7880', null, '123123123', '0');
INSERT INTO `doctor` VALUES ('8', 'Robert', 'Dewar', '1', 'rdew4072@uni.sydney.edu.au', 'rdew4072', null, '123123123', '0');
INSERT INTO `doctor` VALUES ('9', 'Nikolaos', 'Papadopoulos', '1', 'npap2586@uni.sydney.edu.au', 'npap2586', null, '123123123', '0');
INSERT INTO `doctor` VALUES ('10', 'Amanda', 'Farrell', '1', 'afar5889@uni.sydney.edu.au', 'afar5889', null, '123123123', '0');
INSERT INTO `doctor` VALUES ('11', 'Tom', 'Cross', '1', 'doctor@gmail.com', '827daab2a8d2163d67efc16856b42a66', '1442276934183', '123123123', '1');
INSERT INTO `doctor` VALUES ('12', 'Doctor1', 'Doctor1', '1', 'doctor1@gmail.com', 'd1009e4b2edd56e9bfe411074e855215', '1441589781007', '123123123', '0');
INSERT INTO `doctor` VALUES ('13', 'Doctor2', 'Doctor2', '1', 'doctor2@gmail.com', '594bdf2513bf730ba1135ca4b5ffd4fc', '1441432780956', '123123123', '0');

-- ----------------------------
-- Table structure for `note`
-- ----------------------------
DROP TABLE IF EXISTS `note`;
CREATE TABLE `note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noteContent` varchar(255) DEFAULT NULL,
  `noterId` int(11) DEFAULT NULL,
  `noterType` int(11) DEFAULT NULL,
  `sessionId` varchar(255) DEFAULT NULL,
  `time` double DEFAULT NULL,
  `realTime` bigint(20) DEFAULT NULL,
  `startTime` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of note
-- ----------------------------
INSERT INTO `note` VALUES ('213', '1', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0MjI3NDYxNTQ0Mn4wNk1qR1JLcEJZTitlQjFJUERIZ01RUDJ-fg', '1.19', '1442274694438', '1442274693248');
INSERT INTO `note` VALUES ('214', '2', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0MjI3NDYxNTQ0Mn4wNk1qR1JLcEJZTitlQjFJUERIZ01RUDJ-fg', '4.286', '1442274697534', '1442274693248');
INSERT INTO `note` VALUES ('215', 'good', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0MjI3NDYxNTQ0Mn4wNk1qR1JLcEJZTitlQjFJUERIZ01RUDJ-fg', '9.464', '1442274702712', '1442274693248');
INSERT INTO `note` VALUES ('217', 'bad', '1', '1', '1_MX40NDc5NTM2Mn5-MTQ0MjI3NDYxNTQ0Mn4wNk1qR1JLcEJZTitlQjFJUERIZ01RUDJ-fg', '5.573', null, null);
INSERT INTO `note` VALUES ('223', '1', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0MjI3Njk2MDQxNH5BKzdJemZYa0txME5zVHFROE1uTnJZc05-fg', '-0.572', '1442277163625', '1442277164197');
INSERT INTO `note` VALUES ('224', '2', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0MjI3Njk2MDQxNH5BKzdJemZYa0txME5zVHFROE1uTnJZc05-fg', '2.251', '1442277166448', '1442277164197');
INSERT INTO `note` VALUES ('225', '1', '18', '2', '1_MX40NDc5NTM2Mn5-MTQ0MjI5NjMxMzY2M343bEpENm12N3Nndm9WSmNxSTVRVHFCb01-fg', '10.312', '1442296391537', '1442296381225');
INSERT INTO `note` VALUES ('226', '1', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0MjUzNjM2Mjc3MH5tVVNlSWxKQzdOTFQ0VjZVdlpIY0RBTjh-fg', '-0.696', '1442537745232', '1442537745928');
INSERT INTO `note` VALUES ('227', '2', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0MjUzNjM2Mjc3MH5tVVNlSWxKQzdOTFQ0VjZVdlpIY0RBTjh-fg', '1.864', '1442537747792', '1442537745928');
INSERT INTO `note` VALUES ('228', '1', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0MjUzNjM2Mjc3MH5tVVNlSWxKQzdOTFQ0VjZVdlpIY0RBTjh-fg', '5.648', '1442537751576', '1442537745928');
INSERT INTO `note` VALUES ('229', '111', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0MjUzNjM2Mjc3MH5tVVNlSWxKQzdOTFQ0VjZVdlpIY0RBTjh-fg', '12.266', '1442537758194', '1442537745928');
INSERT INTO `note` VALUES ('230', '1', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0MjU0MzU1MDQ3M35hTlVGdCsyR3hFQnRnRlJwR0ZtaU84Qll-fg', '0.963', '1442543668502', '1442543667539');
INSERT INTO `note` VALUES ('231', '2', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0MjU0MzU1MDQ3M35hTlVGdCsyR3hFQnRnRlJwR0ZtaU84Qll-fg', '4.266', '1442543671805', '1442543667539');
INSERT INTO `note` VALUES ('232', 'okokpopk', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0MjU0MzU1MDQ3M35hTlVGdCsyR3hFQnRnRlJwR0ZtaU84Qll-fg', '8.387', '1442543675926', '1442543667539');
INSERT INTO `note` VALUES ('233', 'good', '1', '1', '1_MX40NDc5NTM2Mn5-MTQ0MjU0MzU1MDQ3M35hTlVGdCsyR3hFQnRnRlJwR0ZtaU84Qll-fg', '3.754933', null, null);
INSERT INTO `note` VALUES ('234', 'bad', '1', '1', '1_MX40NDc5NTM2Mn5-MTQ0MjU0MzU1MDQ3M35hTlVGdCsyR3hFQnRnRlJwR0ZtaU84Qll-fg', '10.41663', null, null);
INSERT INTO `note` VALUES ('235', 'good', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0Mjg3ODkyMjI4OX41L2VSM2licERCVVhBdTM0ZHNFZ3ZXYi9-fg', '4.36', '1442878999859', '1442878995499');
INSERT INTO `note` VALUES ('236', '1', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0Mjg3ODkyMjI4OX41L2VSM2licERCVVhBdTM0ZHNFZ3ZXYi9-fg', '13.038', '1442879008537', '1442878995499');
INSERT INTO `note` VALUES ('237', '2', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0Mjg3ODkyMjI4OX41L2VSM2licERCVVhBdTM0ZHNFZ3ZXYi9-fg', '18.762', '1442879014261', '1442878995499');
INSERT INTO `note` VALUES ('238', '1', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0Mjg5NzgzODI4N35BQmNDU0N5a2NneWJwRmd0c2xGZjRDZGN-fg', '11.879', '1442897926829', '1442897914950');
INSERT INTO `note` VALUES ('239', '2', '16', '2', '1_MX40NDc5NTM2Mn5-MTQ0Mjg5NzgzODI4N35BQmNDU0N5a2NneWJwRmd0c2xGZjRDZGN-fg', '19.436', '1442897934386', '1442897914950');
INSERT INTO `note` VALUES ('240', '1', '3', '2', '1_MX40NDc5NTM2Mn5-MTQ0MzE1MTI0MDIyOX55MTJEcHhvaEJpUkMwMTIwczc5akQ2ekV-fg', '6.244', '1443151933855', '1443151927611');
INSERT INTO `note` VALUES ('241', '2', '3', '2', '1_MX40NDc5NTM2Mn5-MTQ0MzE1MTI0MDIyOX55MTJEcHhvaEJpUkMwMTIwczc5akQ2ekV-fg', '9.833', '1443151937444', '1443151927611');
INSERT INTO `note` VALUES ('242', 'good', '3', '2', '1_MX40NDc5NTM2Mn5-MTQ0MzE1MTI0MDIyOX55MTJEcHhvaEJpUkMwMTIwczc5akQ2ekV-fg', '13.816', '1443151941427', '1443151927611');
INSERT INTO `note` VALUES ('243', '1', '3', '2', '1_MX40NDc5NTM2Mn5-MTQ0MzE1Mjk1NTQ0NH5nNlRlTW91RlVWYlJueE5rR28wdytjRDB-fg', '0.903', '1443153036599', '1443153035696');
INSERT INTO `note` VALUES ('244', '2', '3', '2', '1_MX40NDc5NTM2Mn5-MTQ0MzE1Mjk1NTQ0NH5nNlRlTW91RlVWYlJueE5rR28wdytjRDB-fg', '6.145', '1443153041841', '1443153035696');
INSERT INTO `note` VALUES ('245', 'good', '3', '2', '1_MX40NDc5NTM2Mn5-MTQ0MzE1Mjk1NTQ0NH5nNlRlTW91RlVWYlJueE5rR28wdytjRDB-fg', '10.016', '1443153045712', '1443153035696');
INSERT INTO `note` VALUES ('246', 'good', '1', '1', '1_MX40NDc5NTM2Mn5-MTQ0MzE1Mjk1NTQ0NH5nNlRlTW91RlVWYlJueE5rR28wdytjRDB-fg', '1.552', null, null);
INSERT INTO `note` VALUES ('247', 'bad', '1', '1', '1_MX40NDc5NTM2Mn5-MTQ0MzE1Mjk1NTQ0NH5nNlRlTW91RlVWYlJueE5rR28wdytjRDB-fg', '7.809076', null, null);
INSERT INTO `note` VALUES ('248', 'not bad', '1', '1', '1_MX40NDc5NTM2Mn5-MTQ0MzE1Mjk1NTQ0NH5nNlRlTW91RlVWYlJueE5rR28wdytjRDB-fg', '12.666395', null, null);

-- ----------------------------
-- Table structure for `patient`
-- ----------------------------
DROP TABLE IF EXISTS `patient`;
CREATE TABLE `patient` (
  `patientid` int(11) NOT NULL AUTO_INCREMENT,
  `age` int(11) DEFAULT NULL,
  `birthday` bigint(20) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `ethnicity` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `health` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `language` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `lastLoginAt` bigint(20) DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `postcode` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `reason` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `scenarioids` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `street` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `suburb` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `title` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `trainedsceids` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `appliedApt` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`patientid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of patient
-- ----------------------------
INSERT INTO `patient` VALUES ('3', '20', null, 'patient@gmail.com', 'Asian', 'Chunfeng', '2', 'I ma very good', 'Chinese', null, 'Liu', '9767a0d6748bafdd6b4d6ebe78b6be85', '+61416408825', '2220', 'I hear it from friends.', '1', '1', '323 Forest Rd', 'Hurstville', 'Mr.', '1', 'patient@gmail.com', '27');
INSERT INTO `patient` VALUES ('5', '20', null, 'chunfeng.liu@sydney.edu.au', 'Asian', 'Ian', '2', 'I am very healthy.', 'Chinese', null, 'Liu', 'ff6ccbc4b0ce707ff75a556dce32c7fe', '+61416408826', '2220', 'I hear it from my friends.', '1', '1', '323 Forest Rd', 'Hurstville', 'Mr.', '1', 'chunfeng.liu@sydney.edu.au', '31');

-- ----------------------------
-- Table structure for `scenario`
-- ----------------------------
DROP TABLE IF EXISTS `scenario`;
CREATE TABLE `scenario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `detail` varchar(10000) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `pysicality` varchar(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `videoLoc` varchar(500) DEFAULT NULL,
  `trained` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of scenario
-- ----------------------------
INSERT INTO `scenario` VALUES ('1', '#101', '20', 'Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa.', 'male', 'type1', '1', 'video_1436843299347.mp4', '1');
INSERT INTO `scenario` VALUES ('2', '#102', '50', 'Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa.', 'male', 'type2', '1', 'video_1436843299347.mp4', '0');
INSERT INTO `scenario` VALUES ('3', '#103', '20', 'Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa.', 'female', 'type3', '1', 'video_1436843299347.mp4', '0');
INSERT INTO `scenario` VALUES ('4', '#104', '50', 'Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa.', 'female', 'type4', '1', 'video_1436843299347.mp4', '0');

-- ----------------------------
-- Table structure for `scenario_patient`
-- ----------------------------
DROP TABLE IF EXISTS `scenario_patient`;
CREATE TABLE `scenario_patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applyat` bigint(20) DEFAULT NULL,
  `confirmat` bigint(20) DEFAULT NULL,
  `patientId` int(11) DEFAULT NULL,
  `scenarioId` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of scenario_patient
-- ----------------------------

-- ----------------------------
-- Table structure for `s_admin`
-- ----------------------------
DROP TABLE IF EXISTS `s_admin`;
CREATE TABLE `s_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(1000) COLLATE utf8_bin NOT NULL,
  `username` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_edm2m2ks2n54r39o4ipxk22e9` (`email`),
  UNIQUE KEY `UK_hby25rpnay6vu2dgjpdxkn1lx` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of s_admin
-- ----------------------------
INSERT INTO `s_admin` VALUES ('1', 'admin@gmail.com', '7d95bf3f5a14d7adeeda2f5931edf72b', 'admin@gmail.com');

-- ----------------------------
-- Table structure for `s_answer`
-- ----------------------------
DROP TABLE IF EXISTS `s_answer`;
CREATE TABLE `s_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(3000) DEFAULT NULL,
  `createAt` bigint(20) DEFAULT NULL,
  `sessionId` varchar(255) DEFAULT NULL,
  `doctorid` int(11) DEFAULT NULL,
  `patientid` int(11) DEFAULT NULL,
  `surveyid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t4i9otxjj57vcu1w7gkfhx1fw` (`doctorid`),
  KEY `FK_830r3ixr91sq4kxy4xsc7jqkh` (`patientid`),
  KEY `FK_iqgh61c7ckyeldb775nswsa01` (`surveyid`),
  CONSTRAINT `FK_830r3ixr91sq4kxy4xsc7jqkh` FOREIGN KEY (`patientid`) REFERENCES `patient` (`patientid`),
  CONSTRAINT `FK_iqgh61c7ckyeldb775nswsa01` FOREIGN KEY (`surveyid`) REFERENCES `s_survey` (`id`),
  CONSTRAINT `FK_t4i9otxjj57vcu1w7gkfhx1fw` FOREIGN KEY (`doctorid`) REFERENCES `doctor` (`doctorId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of s_answer
-- ----------------------------
INSERT INTO `s_answer` VALUES ('1', '[{\"qid\":28,\"qvalue\":\"111\"},{\"qid\":29,\"qvalue\":\"111\"},{\"qid\":27,\"qvalue\":\"111\"},{\"qid\":25,\"qvalue\":\"2\"}]', '1443153014241', '1_MX40NDc5NTM2Mn5-MTQ0MzE1Mjk1NTQ0NH5nNlRlTW91RlVWYlJueE5rR28wdytjRDB-fg', null, null, '3');
INSERT INTO `s_answer` VALUES ('2', '[{\"qid\":1,\"qvalue\":\"F\"},{\"qid\":12,\"qvalue\":\"iiii\"},{\"qid\":2,\"qvalue\":\"dddd\"},{\"qid\":13,\"qvalue\":\"bbbb\"},{\"qid\":3,\"qvalue\":\"ddd\"},{\"qid\":10,\"qvalue\":\"P+\"},{\"qid\":9,\"qvalue\":\"yyy\"},{\"qid\":11,\"qvalue\":\"uuuu\"},{\"qid\":5,\"qvalue\":\"fff\"},{\"qid\":8,\"qvalue\":\"eeee\"},{\"qid\":4,\"qvalue\":\"P-\"},{\"qid\":6,\"qvalue\":\"gggg\"},{\"qid\":7,\"qvalue\":\"P\"}]', '1443153130645', '1_MX40NDc5NTM2Mn5-MTQ0MzE1Mjk1NTQ0NH5nNlRlTW91RlVWYlJueE5rR28wdytjRDB-fg', null, '3', '1');
INSERT INTO `s_answer` VALUES ('3', '[{\"qid\":36,\"qvalue\":\"3333\"},{\"qid\":34,\"qvalue\":\"333\"},{\"qid\":33,\"qvalue\":\"333\"},{\"qid\":35,\"qvalue\":\"3\"},{\"qid\":32,\"qvalue\":\"333\"},{\"qid\":31,\"qvalue\":\"333\"}]', '1443153145203', '1_MX40NDc5NTM2Mn5-MTQ0MzE1Mjk1NTQ0NH5nNlRlTW91RlVWYlJueE5rR28wdytjRDB-fg', '11', null, '4');
INSERT INTO `s_answer` VALUES ('4', '[{\"qid\":38,\"qvalue\":\"2\"},{\"qid\":37,\"qvalue\":\"3\"}]', '1443153153456', '1_MX40NDc5NTM2Mn5-MTQ0MzE1Mjk1NTQ0NH5nNlRlTW91RlVWYlJueE5rR28wdytjRDB-fg', '11', null, '5');
INSERT INTO `s_answer` VALUES ('5', '[{\"qid\":30,\"qvalue\":\"3333\"}]', '1443153156454', '1_MX40NDc5NTM2Mn5-MTQ0MzE1Mjk1NTQ0NH5nNlRlTW91RlVWYlJueE5rR28wdytjRDB-fg', '11', null, '7');
INSERT INTO `s_answer` VALUES ('6', '[{\"qid\":39,\"qvalue\":\"3333\"},{\"qid\":40,\"qvalue\":\"3333\"}]', '1443153163215', '1_MX40NDc5NTM2Mn5-MTQ0MzE1Mjk1NTQ0NH5nNlRlTW91RlVWYlJueE5rR28wdytjRDB-fg', '11', null, '6');
INSERT INTO `s_answer` VALUES ('7', '[{\"qid\":30,\"qvalue\":\"vcxvc\"}]', '1443153167632', '1_MX40NDc5NTM2Mn5-MTQ0MzE1Mjk1NTQ0NH5nNlRlTW91RlVWYlJueE5rR28wdytjRDB-fg', null, '3', '2');

-- ----------------------------
-- Table structure for `s_question`
-- ----------------------------
DROP TABLE IF EXISTS `s_question`;
CREATE TABLE `s_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(10000) COLLATE utf8_bin NOT NULL,
  `orderOfOptions` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `typeS` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fcr39hn99hv06v1w65tvut69l` (`type`),
  CONSTRAINT `FK_fcr39hn99hv06v1w65tvut69l` FOREIGN KEY (`type`) REFERENCES `s_questiontype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of s_question
-- ----------------------------
INSERT INTO `s_question` VALUES ('1', 'Student introduces self, explains the nature of the interaction, outlines agenda, seeks permission to proceed; patient thanked and the interaction terminated cordially.', '1,2,3,4', '1. Initiate and end the interaction', 'LIKERT', '5');
INSERT INTO `s_question` VALUES ('2', 'For part 1', null, 'What was done well:', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('3', 'For part 1', null, 'What one thing is most important to change:', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('4', 'a. Respecfully developing and maintaining rapport by - allowing patient to use his or her own words without premature interruption, actively listening, using open and closed questions as appropriate, replecting important feelings, picking up verbal and non-verbal cues, eliciting the patient\'s perspective & summarising back to patient to check understanding <br/>\nb. By taking into account patient\'s concerns and understanding of their sitution, and so fromes questions in light of the patient\'s perspective', '33,34,35,36', '2. Use principles of good communication', 'LIKERT', '5');
INSERT INTO `s_question` VALUES ('5', 'For part 2', null, 'What was done well:', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('6', 'For part 2', null, 'What one thing is most important to change:', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('7', 'Student establishes reason for presentation, course and nature of recent symptoms, elicit relevant family & social support, cultural & lifestyle factors or employment issues. NB A formal medical history is not required.', '25,26,27,28', '3. Elicit information', 'LIKERT', '5');
INSERT INTO `s_question` VALUES ('8', 'For part 3', null, 'What was done well:', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('9', 'For part 3', null, 'What one thing is most important to change:', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('10', 'Student identifies & briefly summarises the patient\'s key concerns about the presentation/admission and the effects of it on the patient.', '29,30,31,32', '4. Summarise presentation & impact on the patient', 'LIKERT', '5');
INSERT INTO `s_question` VALUES ('11', 'For part 4', null, 'What was done well:', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('12', 'For part 4', null, 'What one thing is most important to change:', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('13', 'Overall Comments:', null, 'Overall Comments:', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('15', '', null, 'Issues with the system:', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('25', '1: very bad, 7: very good', '82,83,84,85,86,87,88', 'Do you think you are a good communicator?', 'LIKERT', '5');
INSERT INTO `s_question` VALUES ('27', 'Nonverbal behaviours likes sound volume, body gestures, and etc.', null, 'What are the 5 most important nonverbal behaviours?', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('28', '', null, 'What are your strengths/weaknesses in communication?', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('29', '', null, 'Have you had a particularly good or bad communication interaction, and why do you think this happened?', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('30', 'EQ clinic feedback of students', null, 'Please inform us of any concerns you have regarding the EQ clinic.', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('31', '', null, 'What do you remember about the interview? ', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('32', '', null, 'Did you feel that you communication was particularly good at any points? If so, describe them and why?', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('33', '', null, 'Were there any times when you felt you were not communicating well? If so, describe them and why?', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('34', '', null, 'Did you remember any of your nonverbal behaviours during this session?', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('35', '1: very weak 7: very strong', '89,90,91,92,93,94,95', 'How confident do you feel now about your communication skills?', 'LIKERT', '5');
INSERT INTO `s_question` VALUES ('36', '', null, 'You said these were the 5 most important behaviours. Do you think that is still true?', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('37', '', '96,97,98,99,100', 'This scenario was appropriate and useful for my learning.', 'LIKERT', '5');
INSERT INTO `s_question` VALUES ('38', '', '101,102,103,104,105', 'The simulated patient was an appropriate representation of a patient.', 'LIKERT', '5');
INSERT INTO `s_question` VALUES ('39', '', null, 'Did you agree with the SOCA evaluation? Explain your response.', 'TEXTAREA', '2');
INSERT INTO `s_question` VALUES ('40', '', null, 'What three things will you focu on to develop in your communication skills?', 'TEXTAREA', '2');

-- ----------------------------
-- Table structure for `s_questionoption`
-- ----------------------------
DROP TABLE IF EXISTS `s_questionoption`;
CREATE TABLE `s_questionoption` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `optkey` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `optvalue` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `questionid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bl39vvrbvcah4bakh6ilnwba8` (`questionid`),
  CONSTRAINT `FK_bl39vvrbvcah4bakh6ilnwba8` FOREIGN KEY (`questionid`) REFERENCES `s_question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of s_questionoption
-- ----------------------------
INSERT INTO `s_questionoption` VALUES ('1', 'F', 'F', null);
INSERT INTO `s_questionoption` VALUES ('2', 'P-', 'P-', null);
INSERT INTO `s_questionoption` VALUES ('3', 'P', 'P', null);
INSERT INTO `s_questionoption` VALUES ('4', 'P+', 'P+', null);
INSERT INTO `s_questionoption` VALUES ('5', 'F', 'F', null);
INSERT INTO `s_questionoption` VALUES ('6', 'P-', 'P-', null);
INSERT INTO `s_questionoption` VALUES ('7', 'P', 'P', null);
INSERT INTO `s_questionoption` VALUES ('8', 'P+', 'P+', null);
INSERT INTO `s_questionoption` VALUES ('9', 'F', 'F', null);
INSERT INTO `s_questionoption` VALUES ('10', 'P-', 'P-', null);
INSERT INTO `s_questionoption` VALUES ('11', 'P', 'P', null);
INSERT INTO `s_questionoption` VALUES ('12', 'P+', 'P+', null);
INSERT INTO `s_questionoption` VALUES ('13', 'F', 'F', null);
INSERT INTO `s_questionoption` VALUES ('14', 'P-', 'P-', null);
INSERT INTO `s_questionoption` VALUES ('15', 'P', 'P', null);
INSERT INTO `s_questionoption` VALUES ('16', 'P+', 'P+', null);
INSERT INTO `s_questionoption` VALUES ('17', 'F', 'F', null);
INSERT INTO `s_questionoption` VALUES ('18', 'P-', 'P-', null);
INSERT INTO `s_questionoption` VALUES ('19', 'P', 'P', null);
INSERT INTO `s_questionoption` VALUES ('20', 'P+', 'P+', null);
INSERT INTO `s_questionoption` VALUES ('21', 'F', 'F', null);
INSERT INTO `s_questionoption` VALUES ('22', 'P-', 'P-', null);
INSERT INTO `s_questionoption` VALUES ('23', 'P', 'P', null);
INSERT INTO `s_questionoption` VALUES ('24', 'P+', 'P+', null);
INSERT INTO `s_questionoption` VALUES ('25', 'F', 'F', null);
INSERT INTO `s_questionoption` VALUES ('26', 'P-', 'P-', null);
INSERT INTO `s_questionoption` VALUES ('27', 'P', 'P', null);
INSERT INTO `s_questionoption` VALUES ('28', 'P+', 'P+', null);
INSERT INTO `s_questionoption` VALUES ('29', 'F', 'F', null);
INSERT INTO `s_questionoption` VALUES ('30', 'P-', 'P-', null);
INSERT INTO `s_questionoption` VALUES ('31', 'P', 'P', null);
INSERT INTO `s_questionoption` VALUES ('32', 'P+', 'P+', null);
INSERT INTO `s_questionoption` VALUES ('33', 'F', 'F', null);
INSERT INTO `s_questionoption` VALUES ('34', 'P-', 'P-', null);
INSERT INTO `s_questionoption` VALUES ('35', 'P', 'P', null);
INSERT INTO `s_questionoption` VALUES ('36', 'P+', 'P+', null);
INSERT INTO `s_questionoption` VALUES ('62', '', '', null);
INSERT INTO `s_questionoption` VALUES ('68', '111', '1', null);
INSERT INTO `s_questionoption` VALUES ('69', '222', '2', null);
INSERT INTO `s_questionoption` VALUES ('70', '333', '3', null);
INSERT INTO `s_questionoption` VALUES ('74', '111', '1', null);
INSERT INTO `s_questionoption` VALUES ('75', '222', '2', null);
INSERT INTO `s_questionoption` VALUES ('76', '333', '3', null);
INSERT INTO `s_questionoption` VALUES ('77', '444', '4', null);
INSERT INTO `s_questionoption` VALUES ('82', '1', '1', null);
INSERT INTO `s_questionoption` VALUES ('83', '2', '2', null);
INSERT INTO `s_questionoption` VALUES ('84', '3', '3', null);
INSERT INTO `s_questionoption` VALUES ('85', '4', '4', null);
INSERT INTO `s_questionoption` VALUES ('86', '5', '5', null);
INSERT INTO `s_questionoption` VALUES ('87', '6', '6', null);
INSERT INTO `s_questionoption` VALUES ('88', '7', '7', null);
INSERT INTO `s_questionoption` VALUES ('89', '1', '1', null);
INSERT INTO `s_questionoption` VALUES ('90', '2', '2', null);
INSERT INTO `s_questionoption` VALUES ('91', '3', '3', null);
INSERT INTO `s_questionoption` VALUES ('92', '4', '4', null);
INSERT INTO `s_questionoption` VALUES ('93', '5', '5', null);
INSERT INTO `s_questionoption` VALUES ('94', '6', '6', null);
INSERT INTO `s_questionoption` VALUES ('95', '7', '7', null);
INSERT INTO `s_questionoption` VALUES ('96', 'Strong disagree', '1', null);
INSERT INTO `s_questionoption` VALUES ('97', 'Disagree', '2', null);
INSERT INTO `s_questionoption` VALUES ('98', 'Neutral', '3', null);
INSERT INTO `s_questionoption` VALUES ('99', 'Agree', '4', null);
INSERT INTO `s_questionoption` VALUES ('100', 'Strong agree', '5', null);
INSERT INTO `s_questionoption` VALUES ('101', 'Strong disagree', '1', null);
INSERT INTO `s_questionoption` VALUES ('102', 'Disagree', '2', null);
INSERT INTO `s_questionoption` VALUES ('103', 'Neutral', '3', null);
INSERT INTO `s_questionoption` VALUES ('104', 'Agree', '4', null);
INSERT INTO `s_questionoption` VALUES ('105', 'Strong Agree', '5', null);

-- ----------------------------
-- Table structure for `s_questiontype`
-- ----------------------------
DROP TABLE IF EXISTS `s_questiontype`;
CREATE TABLE `s_questiontype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of s_questiontype
-- ----------------------------
INSERT INTO `s_questiontype` VALUES ('1', 'TEXTFIELD');
INSERT INTO `s_questiontype` VALUES ('2', 'TEXTAREA');
INSERT INTO `s_questiontype` VALUES ('3', 'RADIO');
INSERT INTO `s_questiontype` VALUES ('4', 'CHECKBOX');
INSERT INTO `s_questiontype` VALUES ('5', 'LIKERT');

-- ----------------------------
-- Table structure for `s_question_questionoption`
-- ----------------------------
DROP TABLE IF EXISTS `s_question_questionoption`;
CREATE TABLE `s_question_questionoption` (
  `s_question_id` int(11) NOT NULL,
  `options_id` int(11) NOT NULL,
  UNIQUE KEY `UK_b4k9mx6haxga1j20mjx0u2gvf` (`options_id`),
  KEY `FK_oc9qy0nbyppqkusouiurcpajl` (`s_question_id`),
  CONSTRAINT `FK_b4k9mx6haxga1j20mjx0u2gvf` FOREIGN KEY (`options_id`) REFERENCES `s_questionoption` (`id`),
  CONSTRAINT `FK_oc9qy0nbyppqkusouiurcpajl` FOREIGN KEY (`s_question_id`) REFERENCES `s_question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of s_question_questionoption
-- ----------------------------
INSERT INTO `s_question_questionoption` VALUES ('1', '1');
INSERT INTO `s_question_questionoption` VALUES ('1', '2');
INSERT INTO `s_question_questionoption` VALUES ('1', '3');
INSERT INTO `s_question_questionoption` VALUES ('1', '4');
INSERT INTO `s_question_questionoption` VALUES ('4', '33');
INSERT INTO `s_question_questionoption` VALUES ('4', '34');
INSERT INTO `s_question_questionoption` VALUES ('4', '35');
INSERT INTO `s_question_questionoption` VALUES ('4', '36');
INSERT INTO `s_question_questionoption` VALUES ('7', '25');
INSERT INTO `s_question_questionoption` VALUES ('7', '26');
INSERT INTO `s_question_questionoption` VALUES ('7', '27');
INSERT INTO `s_question_questionoption` VALUES ('7', '28');
INSERT INTO `s_question_questionoption` VALUES ('10', '29');
INSERT INTO `s_question_questionoption` VALUES ('10', '30');
INSERT INTO `s_question_questionoption` VALUES ('10', '31');
INSERT INTO `s_question_questionoption` VALUES ('10', '32');
INSERT INTO `s_question_questionoption` VALUES ('25', '82');
INSERT INTO `s_question_questionoption` VALUES ('25', '83');
INSERT INTO `s_question_questionoption` VALUES ('25', '84');
INSERT INTO `s_question_questionoption` VALUES ('25', '85');
INSERT INTO `s_question_questionoption` VALUES ('25', '86');
INSERT INTO `s_question_questionoption` VALUES ('25', '87');
INSERT INTO `s_question_questionoption` VALUES ('25', '88');
INSERT INTO `s_question_questionoption` VALUES ('35', '89');
INSERT INTO `s_question_questionoption` VALUES ('35', '90');
INSERT INTO `s_question_questionoption` VALUES ('35', '91');
INSERT INTO `s_question_questionoption` VALUES ('35', '92');
INSERT INTO `s_question_questionoption` VALUES ('35', '93');
INSERT INTO `s_question_questionoption` VALUES ('35', '94');
INSERT INTO `s_question_questionoption` VALUES ('35', '95');
INSERT INTO `s_question_questionoption` VALUES ('37', '96');
INSERT INTO `s_question_questionoption` VALUES ('37', '97');
INSERT INTO `s_question_questionoption` VALUES ('37', '98');
INSERT INTO `s_question_questionoption` VALUES ('37', '99');
INSERT INTO `s_question_questionoption` VALUES ('37', '100');
INSERT INTO `s_question_questionoption` VALUES ('38', '101');
INSERT INTO `s_question_questionoption` VALUES ('38', '102');
INSERT INTO `s_question_questionoption` VALUES ('38', '103');
INSERT INTO `s_question_questionoption` VALUES ('38', '104');
INSERT INTO `s_question_questionoption` VALUES ('38', '105');

-- ----------------------------
-- Table structure for `s_survey`
-- ----------------------------
DROP TABLE IF EXISTS `s_survey`;
CREATE TABLE `s_survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_bin NOT NULL,
  `orderOfQuestions` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `adminid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_flfw1qi2rl1iagi68sjqepr8k` (`adminid`),
  CONSTRAINT `FK_flfw1qi2rl1iagi68sjqepr8k` FOREIGN KEY (`adminid`) REFERENCES `s_admin` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of s_survey
-- ----------------------------
INSERT INTO `s_survey` VALUES ('1', 'Observe student interviewing patient (real or simulated) and comment on the student\'s ability to:', '1,2,3,4,5,6,7,8,9,10,11,12,13', 'Student-Patient Observed Communication Assessment (SOCA) form', null);
INSERT INTO `s_survey` VALUES ('2', 'Please evaluate the EQClinic process (For Patient)', '30', 'EQ Clinic Feedback', null);
INSERT INTO `s_survey` VALUES ('3', '', '25,27,28,29', 'Pre-appoinment Form', null);
INSERT INTO `s_survey` VALUES ('4', '', '31,32,33,34,35,36', 'Personal Reflection (1)', null);
INSERT INTO `s_survey` VALUES ('5', 'Please evaluate the performance of the simulated patient and difficulty of the scenario', '37,38', 'Simulated Patient & Scenario Rating Form', null);
INSERT INTO `s_survey` VALUES ('6', '', '39,40', 'Personal Reflection (2)', null);
INSERT INTO `s_survey` VALUES ('7', 'Please evaluate the EQClinic process (For Doctor)', '30', 'EQ Clinic Feedback', null);

-- ----------------------------
-- Table structure for `s_survey_question`
-- ----------------------------
DROP TABLE IF EXISTS `s_survey_question`;
CREATE TABLE `s_survey_question` (
  `survey_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`survey_id`,`question_id`),
  KEY `FK_lckh8bncgc6ux0saw1su2arlq` (`question_id`),
  CONSTRAINT `FK_hl27lsstc7kpoli9vye1gf6ld` FOREIGN KEY (`survey_id`) REFERENCES `s_survey` (`id`),
  CONSTRAINT `FK_lckh8bncgc6ux0saw1su2arlq` FOREIGN KEY (`question_id`) REFERENCES `s_question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of s_survey_question
-- ----------------------------
INSERT INTO `s_survey_question` VALUES ('1', '1');
INSERT INTO `s_survey_question` VALUES ('1', '2');
INSERT INTO `s_survey_question` VALUES ('1', '3');
INSERT INTO `s_survey_question` VALUES ('1', '4');
INSERT INTO `s_survey_question` VALUES ('1', '5');
INSERT INTO `s_survey_question` VALUES ('1', '6');
INSERT INTO `s_survey_question` VALUES ('1', '7');
INSERT INTO `s_survey_question` VALUES ('1', '8');
INSERT INTO `s_survey_question` VALUES ('1', '9');
INSERT INTO `s_survey_question` VALUES ('1', '10');
INSERT INTO `s_survey_question` VALUES ('1', '11');
INSERT INTO `s_survey_question` VALUES ('1', '12');
INSERT INTO `s_survey_question` VALUES ('1', '13');
INSERT INTO `s_survey_question` VALUES ('3', '25');
INSERT INTO `s_survey_question` VALUES ('3', '27');
INSERT INTO `s_survey_question` VALUES ('3', '28');
INSERT INTO `s_survey_question` VALUES ('3', '29');
INSERT INTO `s_survey_question` VALUES ('2', '30');
INSERT INTO `s_survey_question` VALUES ('7', '30');
INSERT INTO `s_survey_question` VALUES ('4', '31');
INSERT INTO `s_survey_question` VALUES ('4', '32');
INSERT INTO `s_survey_question` VALUES ('4', '33');
INSERT INTO `s_survey_question` VALUES ('4', '34');
INSERT INTO `s_survey_question` VALUES ('4', '35');
INSERT INTO `s_survey_question` VALUES ('4', '36');
INSERT INTO `s_survey_question` VALUES ('5', '37');
INSERT INTO `s_survey_question` VALUES ('5', '38');
INSERT INTO `s_survey_question` VALUES ('6', '39');
INSERT INTO `s_survey_question` VALUES ('6', '40');

-- ----------------------------
-- Table structure for `teacher`
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `teacherId` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30) DEFAULT NULL,
  `lastname` varchar(30) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `lastLoginAt` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`teacherId`),
  UNIQUE KEY `teacherId` (`teacherId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES ('1', 'Ming', 'Ma', 'tutor@gmail.com', '6e290cf849cad9ccf5d0d5479d0f62ba', '1', '1442275328046');
INSERT INTO `teacher` VALUES ('2', 'Ming', 'Ma', 'tutor1@gmail.com', '82260f1fe545ab87254bbbb7ba952c9e', '1', '1442032266131');

-- ----------------------------
-- Table structure for `video`
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aptDate` varchar(255) DEFAULT NULL,
  `aptTime` varchar(255) DEFAULT NULL,
  `archiveId` varchar(255) DEFAULT NULL,
  `doctorId` int(11) DEFAULT NULL,
  `doctorName` varchar(255) DEFAULT NULL,
  `generateTime` bigint(20) DEFAULT NULL,
  `generateTimeStr` varchar(255) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `patientId` int(11) DEFAULT NULL,
  `patientName` varchar(255) DEFAULT NULL,
  `sceCode` varchar(255) DEFAULT NULL,
  `sessionId` varchar(255) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `videoType` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of video
-- ----------------------------
