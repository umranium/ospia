<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<body>
	<h1>HTTP Status 403 - Access is denied</h1>
	<sec:authorize access="isAnonymous()">
	   <h2>You do not have the permission to access this page!</h2>
	</sec:authorize>
	
	<sec:authorize access="isAuthenticated()">
       <h2>To access this page, you have to log off the system.</h2>
    </sec:authorize>
</body>
</html>