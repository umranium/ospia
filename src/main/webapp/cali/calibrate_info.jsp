<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SOCA Training</title>
<c:import url="/common.jsp"></c:import>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/cali/calibrate_info.js"></script>
<style type="text/css">
#signupForm label.error {
	color: red;
	width: auto;
	margin-top: 2px;
	/* 
	display: inline; 
	margin-left: 10px;
	*/
}
</style>
</head>

<body>
	<c:import url="/spheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<div class="usyd-inline-wrap">
							<h1>Assessment Training and Calibration</h1>
						</div>
					</div>
					<br />
					<hr class="autohr">
					<br />

					<form class="form-horizontal" id="signupForm" method="post" action="<%=request.getContextPath()%>/cali/savecalibrationuser">
						<input type="hidden" id="agreementValue" value="">
						<div class="usyd-form-component">
							<div class="control-group" id="registerErroDiv" style="display:none">
								<label class="control-label" for="textinput"></label>
								<div class="controls">
									<p style="color:red" id="registerError"></p>
								</div>
							</div>
						</div>


						<div class="usyd-form-component">
							<div class="control-group">
								<label class="control-label" for="radios" style="width:300px; margin-right:30px;">Are you a member of UNSW Faculty of Medicine?</label>
								<div class="controls">
									<label class="radio-inline" for="radios-0"> <input type="radio" name="faculty" id="faculty1" value="1"
										checked="checked"> Yes
									</label> <label class="radio-inline" for="radios-1"> <input type="radio" name="faculty" id="faculty2" value="2"> No
									</label>
								</div>
							</div>
						</div>

						<div class="usyd-form-component" id="facultyBtnDiv">
							<div class="control-group">
								<label class="control-label" for="singlebutton" style="width:300px; margin-right:30px;"></label>
								<div class="controls">
									<input type="button" id="facultyBtn" name="facultyBtn" class="usyd-ui-button-primary" value="submit">
								</div>
							</div>
						</div>

						<div id="facultyDiv" style="display:none">
							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput" style="width:300px; margin-right:30px;">Please enter your zID only</label>
									<div class="controls">
										<input id="zpass" name="zpass" type="text" placeholder=""> (Password not required)
									</div>
								</div>
							</div>

							<div class="usyd-form-component" id="facultyBtnDiv">
								<div class="control-group">
									<label class="control-label" for="singlebutton" style="width:300px; margin-right:30px;"></label>
									<div class="controls">
										<input type="button" id="notFacultySubmitBtn" name="notFacultySubmitBtn" class="usyd-ui-button-primary" value="submit">
									</div>
								</div>
							</div>
						</div>


						<div id="notFacultyDiv" style="display:none">
							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="selectbasic" style="width:300px; margin-right:30px;">Clinical Site</label>
									<div class="controls">
										<select id="clinicalSite" name="clinicalSite" class="form-control" style="width:300px;">
											<option value="0" selected="selected">Select</option>
											<option value="1">Prince of Wales Hospital</option>
											<option value="2">St Vincent's Hospital</option>
											<option value="3">St George and Sutherland Hospital</option>
											<option value="4">Liverpool and associated hospitals</option>
											<option value="5">other</option>
										</select>
									</div>
								</div>
							</div>

							<div id="otherDiv" style="display:none">
								<div class="usyd-form-component">
									<div class="control-group">
										<label class="control-label" for="textinput" style="width:300px; margin-right:30px;">other</label>
										<div class="controls">
											<input id="other" name="other" type="text" placeholder="">
										</div>
									</div>
								</div>
							</div>


							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput" style="width:300px; margin-right:30px;">Position</label>
									<div class="controls">
										<input id="position" name="position" type="text" placeholder="">
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput" style="width:300px; margin-right:30px;">Name</label>
									<div class="controls">
										<input id="name" name="name" type="text" placeholder="">
									</div>
								</div>
							</div>


							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput" style="width:300px; margin-right:30px;">Email</label>
									<div class="controls">
										<input id="email" name="email" type="text" placeholder="">
									</div>
								</div>
							</div>

							<div class="usyd-form-component" id="facultyBtnDiv">
								<div class="control-group">
									<label class="control-label" for="singlebutton" style="width:300px; margin-right:30px;"></label>
									<div class="controls">
										<input type="button" id="facultySubmitBtn" name="facultySubmitBtn" class="usyd-ui-button-primary" value="submit">
									</div>
								</div>
							</div>
					</form>
					
					
					<div id="agreementPopUp" style="display:none;height: 560px; overflow-y: scroll;">
                            <table class="table borderless " style="border:0;">
                                <tr>
                                    <td id="top" colspan="2" style="font-size:18px;"><b>PARTICIPANT INFORMATION STATEMENT AND CONSENT FORM</b>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;"><b>The study is being carried out by the following researchers</b> <br>    
                                    <b>Chief Investigator:</b> <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Dr Silas Taylor (UNSW) <br/>
                                    <b>Co-Investigator/s:</b> <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Prof Rafael Calvo (University of Sydney)<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Dr Renee Lim (University of Sydney)<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp; A/Prof Boaz Shulruf (UNSW)<br/>
                                    <b>Student Investigator/s:</b> <br>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Chunfeng Liu (PhD student, University of Sydney)
                                    <br/>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;"><b>What is the research study about?</b> <br> 
                                    You are invited to take part in this research study. You have been invited because you are a volunteer Simulated Patient (SP) for UNSW Medicine students in the Clinical Skills program. <br><br>
                                    To participate in this project you need to meet the following inclusion criteria:<br>
                                      &nbsp;&nbsp;&nbsp;&bull; You are completing an OSPIA, that is an online interaction with a medical student.<br>
                                      &nbsp;&nbsp;&nbsp;&bull; You have completed the online training modules for interacting in the OSPIA session.<br><br>
                                    The purpose of this research is to provide evidence that medical students will improve their communication skills (specifically nonverbal) by using the OSPIA system.
                                    <br>The first aim of this research project is to help medical students to improve their overall CSL by providing focus and specific feedback on nonverbal skills in online sessions.
                                    <br>The second aim is more technical: to improve the algorithms that generate the automated feedback. This study will provide further data that will continue to improve the accuracy of computing algorithms that analyse the video and detect nonverbal features of the interactions.
                                    <br>Importance of the study - Clinical interviews play an important role in the therapeutic process. Medical education thus includes the teaching of communication skills. However, much communication between individuals occurs nonverbally, and this can be difficult to teach students about during live interactions. Providing a sustainable system that aims to improve medical students' nonverbal communication skills, including provision of good quality automated feedback, is a significant intervention. 
                                    <br>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>Do I have to take part in this research study?</b> <br/>
                                    <u>Participation in this research study is voluntary.</u> If you don't wish to take part, you don't have to. Your decision will not affect your relationship with The University of New South Wales or the University of Sydney. However, <u>if you do not agree you will not be able to take part in the trial of the online system.</u>
                                    <br><br>
                                    This Participant Information Statement and Consent Form tells you about the research study. It explains the research tasks involved. Knowing what is involved will help you decide if you want to take part in the research.
                                    <br><br>
                                    Please read this information carefully. Ask questions about anything that you don't understand or want to know more about.  Before deciding whether or not to take part, you might want to talk about it with a relative or friend.
                                    <br><br>
                                    If you decide you want to take part in the research study, you will be asked to:
                                    <br>
                                    &nbsp;&nbsp;&nbsp;&bull; Tick 'Agree' on the online consent form;
                                    <br>
                                    &nbsp;&nbsp;&nbsp;&bull; Keep a copy of this Participant Information Statement (available on the OSPIA website)
                                    <br>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What does participation in this research require, and are there any risks involved?</b> <br/>
                                    If you decide to take part in the research study, you will be asked to participate in an OSPIA session, part of the normal requirements for the UNSW Medicine Clinical Skills curriculum.
                                    <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>Study tasks & risks:</b> <br/>
                                    You will perform an OSPIA interaction, as part of the UNSW Medicine Clinical Skills curriculum. As part of the study, with your permission, we would like to gain access to the automated computer-generated analysis of non-verbal communication in the recordings of the OSPIA interactions. This is to collect information that tells us about non-verbal communication skills as a part of communication skills in student-SP interactions.
                                     <br><br>
                                    An OSPIA interaction will take approximately 30-45 minutes.
                                     <br><br>
                                    Aside from giving up your time, we do not expect that there will be any risks or costs associated with taking part in this study.
                                    <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>Will I be paid to participate in this project?</b> <br/>
                                    There are no costs associated with participating in this research study, nor will you be paid.
                                     <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What are the possible benefits to participation?</b> <br/>
                                    We hope to use information we get from this research study to benefit others who will participate in communication skills teaching in future. 
                                     <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What will happen to information about me?</b> <br/>
                                    By signing the consent form you consent to the research team collecting and using information about you for the research study. We will keep your data for up to 7 years. We will store information from the study on the Microsoft Azure Cloud Platform and Sydney University password protected computers and on UNSW password protected computers thereafter...
                                    <br><br>
                                    It is anticipated that the results of this research study will be published and/or presented in a variety of forums. In any publication and/or presentation, information will be published, in a way such that you will not be individually identifiable.
                                    <br><br>
                                    Please note that the digital video recordings are not in themselves for the purposes of the research study. After the OSPIA, we will store these digital recordings for up to three months. We store these files on the Microsoft Azure Cloud Platform. Your confidentiality will be ensured by this being a password protected, enterprise quality system. The server can only be accessed by the researchers in the team.
                                     <br>
                                    </td>
                                </tr>
                                
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>How and when will I find out what the results of the research study are?</b> <br/>
                                    You have a right to receive feedback about the overall results of this study. You can tell us that you wish to receive feedback by emailing <a>csadmin@unsw.edu.au</a>. This feedback will be in the form of a link to a webpage that will have a summary of results, or a published conference or journal article. You can receive this feedback after the study is finished.
                                    <br>
                                    </td>
                                </tr>
                                
                                 <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What if I want to withdraw from the research study?</b> <br/>
                                    If you do consent to participate, you may withdraw at any time. If you do withdraw, you will be asked to complete and sign the 'Withdrawal of Consent Form' which is provided at the end of this document, and/or as another tick box option on the OSPIA website. Alternatively you can ring the research team and tell them you no longer want to participate.
                                    <br><br>
                                    You are free to stop the OSPIA interaction at any time if it does not comply with the code of conduct - please use the on-screen STOP button. For teaching purposes, these recordings will be reviewed by Dr Silas Taylor. If deemed to not be an appropriate student-SP interview, the research study will delete all analysis of that recording for research purposes. 
                                    <br>Submitting your completed questionnaires is an indication of your consent to participate in the study. You can withdraw your responses if you change your mind about having them included in the study, up to the point that we have analysed the results.
                                    <br>If you decide to withdraw from the study, we will not collect any more information from you. Any non-identifiable information that we have already collected, however, will be kept in our study records and may be included in the study results.
                                    <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What should I do if I have further questions about my involvement in the research study?</b> <br/>
                                    The person you may need to contact will depend on the nature of your query. If you want any further information concerning this project or if you have any problems which may be related to your involvement in the project, you can contact the following member/s of the research team:
                                    <br><br>
                                    <b>Research Team Contact</b><br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Name: </b> Dr Silas Taylor <br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Position: </b> Chief Investigator<br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Telephone: </b> 9385 2607<br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Email: </b> <a>silas.taylor@unsw.edu.au</a><br>
                                    
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What if I have a complaint or any concerns about the research study?</b> <br/>
                                    If you have any complaints about any aspect of the project, the way it is being conducted, then you may contact:
                                    <br><br>
                                    <b>Complaints Contact</b><br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Position: </b> Human Research Ethics Coordinator<br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Telephone: </b> + 61 2 9385 6222<br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Email: </b> <a>humanethics@unsw.edu.au</a><br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>HC Reference Number: </b> HC16048 <br>
                                    </td>
                                </tr>
                                
                                 <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b style="color:red; font-size:18px;">I would like to participate and I have</b> <br/><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Read the Participant Information Sheet or someone has read it to me in a language that I understand;<br><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Understood the purposes, study tasks and risks of the research described in the project;<br><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Had an opportunity to email questions to the Chief Investigator and I am satisfied with the answers I have received;<br><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Freely agree to participate in this research study as described and understand that I am free to withdraw at any time during the project and withdrawal will not affect my relationship with any of the named organisations and/or research team members;<br><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Understand that I am able to down load a copy of this document to keep from the OSPIA website (student login)<br><br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2">Do you agree above items?</td>
                                </tr>
                                <tr>
                                    <td style="width:50%"><input type="radio" value="1" name="agreement" checked="checked">&nbsp;I agree</td>
                                    <td style="width:50%"><input type="radio" value="0" name="agreement">&nbsp;I don't agree</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="button" id="submitAgreement" class="usyd-ui-button-primary" value="submit"></td>
                                </tr>
                            </table>
                        </div>
					
				</div>
			</div>
		</div>
	</div>
	</div>

	<c:import url="/footer.jsp"></c:import>
</body>
</html>
