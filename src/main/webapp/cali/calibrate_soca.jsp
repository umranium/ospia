<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Tutor Review</title>
<c:import url="/common.jsp"></c:import>
<link href="<%=request.getContextPath()%>/css/wufoo/form.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/wufoo/theme.css"
	rel="stylesheet">
<script src="<%=request.getContextPath()%>/js/cali/calibrate_soca.js"></script>
</head>

<body>
	<c:import url="/tutheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<form id="Survey_SOCA" name="Survey_SOCA" action="spSOCA_submit">

							<input type="hidden" id="nextpage" value="${nextpage}"> <input
								type="hidden" id="teacherId"
								value="<sec:authentication property='principal.teacher.teacherId'/>">
							<input type="hidden" value="${surveyId}" id="surveyId"> <input
								type="hidden" value="${ispreview}" id="ispreview"> <input
								type="hidden" value="${sessionId}" id="sessionId"> <input
								type="hidden" value="${answerId}" id="answerId">

							<header class="info">
								<h1 id="surveyTitle"></h1>
								<div>
									<p id="surveyDescription"></p>
								</div>
							</header>
							<ul id="questions">
							</ul>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
