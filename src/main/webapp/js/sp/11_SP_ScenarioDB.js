$(document).ready(function() {

	searchSce();

	$("#searchSce").click(function() {
		searchSce();
	});

});

function searchSce() {

	var age = parseInt($("#age").val());
	var gender = $("#gender").val() + "";
	var physocality = $("#physocality").val() + "";
	var formdata = {
		"age" : age,
		"gender" : gender,
		"pysicality" : physocality
	};

	$.ajax({
				type : 'POST',
				headers: getTokenHeader(),
				url : '/'+PATIENTPATH+'/getscenarios',
				contentType : 'application/json',
				dataType : "json",
				data : JSON.stringify(formdata),
				success : function(data) {

					var list = $("#scenarioList");
					list.empty();

					for (var i = 0; i < data.length; i++) {

						var scenario = data[i];
						trEle = "<tr><td>" +
								 scenario.code+
								 "</td><td>"+
								 scenario.age+
								 "</td><td>"+
								 scenario.gender+
								 "</td><td>"+
								 scenario.pysicality+
								 "</td>"+
								 "</td>"+
								 "<td><a href='10_SP_TrainingDetail.jsp'><button class='usyd-ui-button-primary'>Training</button></a></td>"+
								 "</tr>";
						list.append(trEle);
					}
				}
			});

}
