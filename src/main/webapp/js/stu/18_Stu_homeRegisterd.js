$(document).ready(function(){
	// browserDetection();
	getStuRecentRequest();
	loadNextAptsStu();
	// loadCancelledAptsStu();
	
	var agreement = $("#agreement").val();
	var homeloaded = $("#homeloaded").val();
	
	// due to ethics, hide the agreement
	if(agreement == 0 && homeloaded==0){
		$.blockUI({
			message : $('#agreementPopUp'),
			css : {
				top : ($(window).height() - 560) / 2 + 'px',
				left : ($(window).width() - 500) / 2 + 'px',
				width : '500px',
				height : '560px',
			}
		});
		setTimeout(function(){
			$('#agreementPopUp').scrollTop(-200);
		},100);
	}
	
	
	$("#codeCondect").change(function(){
		if($("#codeCondect").is(':checked')){
			$("#submitCodeConduct").removeAttr("disabled");
		}else{
			$("#submitCodeConduct").attr("disabled","disabled");
		}	
	});
	
	$("#submitCodeConduct").click(function(){
		$.blockUI({
			message : $('#agreementPopUp'),
			css : {
				top : ($(window).height() - 560) / 2 + 'px',
				left : ($(window).width() - 400) / 2 + 'px',
				width : '400px',
				height : '560px',
			}
		});
		$('#agreementPopUp').scrollTop(-100);
	});
	
	$("#submitAgreement").click(function(){
		var doctorId = $("#doctorId").val();
		var agreement = $("input[name='agreement']:checked").val();	
		if(agreement == 1){
			var formdata = {"doctorId" : doctorId,	"agreement" : agreement };
			$.ajax({
				type : 'POST',
				headers: getTokenHeader(),
				url : '/'+DOCTORPATH+'/updateagreement',
				contentType : 'application/json',
				dataType : "json",
				data : JSON.stringify(formdata),
				success : function(data) {
					if(data!=null){
						if (data.code == 1) {
							$.unblockUI();
						} else {
							alert("Submit failed, please try again");
						}	
					}
				}
			});
		}else{
			$.unblockUI();
		}
	});
});


function getStuRecentRequest(){
	
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/getdoctorrecentrequest',
		dataType : "json",
		beforeSend : function() {
			$("#requestDivLoading").show();
		},
		success : function(data) {
			
			$("#requestDivLoading").hide();
			
			$("#reqList").empty();
			
			if(data.length>0){
				var totalShowItem = 0;
				
				for(var i = 0; i<data.length ; i++){
					var apt = data[i];
					var reqInfo = convertToDate(apt.starttime).split("T");
					var trEle="";
					
					if(apt.status==2){
						trEle = "<tr><td>"+convertNumnberDateToString(reqInfo[0])+"</td><td>"+convertFullDateToAPTime(convertToDate(apt.starttime))+"</td><td>"+apt.patientname+"</td>" +
				           "<td><span>In progress</span></td>"+
				           "</tr>";	
						totalShowItem= totalShowItem+1;
					}else if(apt.status==3){
							trEle = "<tr><td>"+convertNumnberDateToString(reqInfo[0])+"</td><td>"+convertFullDateToAPTime(convertToDate(apt.starttime))+"</td><td>"+apt.patientname+"</td>" +
					           "<td><span style='color:green'>Accepted</span></td>"+
					           "</tr>";	
							totalShowItem= totalShowItem+1;
					}else if(apt.status==4){
						trEle = "<tr><td>"+convertNumnberDateToString(reqInfo[0])+"</td><td>"+convertFullDateToAPTime(convertToDate(apt.starttime))+"</td><td>"+apt.patientname+"</td>" +
				           "<td><span style='color:red'>Declined</span></td>"+
				           "</tr>";
						totalShowItem= totalShowItem+1;
					}else if(apt.status==5){
						trEle = "<tr><td>"+convertNumnberDateToString(reqInfo[0])+"</td><td>"+convertFullDateToAPTime(convertToDate(apt.starttime))+"</td><td>"+apt.patientname+"</td>" +
				           "<td><span style='color:red'>Cancelled</span></td>"+
				           "</tr>";	
						totalShowItem= totalShowItem+1;
					}else if(apt.status==8){
//							trEle = "<tr><td>"+reqInfo[0]+"</td><td>"+reqInfo[1]+"</td><td>"+apt.patientname+"</td>" +
//					           "<td><span style='color:green'>Finished</span></td>"+
//					           "</tr>";
					}
					
					$("#reqList").append(trEle);
				}
				if(totalShowItem!=0){
					$("#reqTable").show();
				}else{
					var obj = $("#reqTable").parent();
					obj.hide();
					$("#reqTable").hide();
					$("#requestNote").html("<br/>You have no requests.");
				}
			}else{
				var obj = $("#reqTable").parent();
				obj.hide();
				$("#reqTable").hide();
				$("#requestNote").html("<br/>You have no requests.");
			}
		}
	});
	
	
}


function loadNextAptsStu(){
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/getdoctornextapts',
		dataType : "json",
		beforeSend : function() {
			$("#nextaptDivLoading").show();
		},
		success : function(data) {
			$("#nextaptDivLoading").hide();
			
			$("#aptList").empty();
			
			if(data.length>0){
				for(var i = 0; i<data.length ; i++){
					var apt = data[i];
					var reqInfo = convertToDate(apt.starttime).split("T");
					var trEle="";
					
					trEle = "<tr><td>"+convertNumnberDateToString(reqInfo[0])
					+"</td><td>"+convertFullDateToAPTime(convertToDate(apt.starttime))
					+"</td><td>"+apt.patientname+"</td>" +
			           "<td><input type='button' value='Start Appointment' onclick='startApt("+apt.aptid+")' class='usyd-ui-button-primary'>"+
			           "</tr>";
					$("#aptList").append(trEle);
				}
				$("#aptTable").show();
			}else{
				var obj = $("#aptTable").parent();
				obj.hide();
				$("#aptTable").hide();
				$("#aptNote").html("<br>You have no appointments in the next two weeks.");
			}
		}
	});
}



function loadCancelledAptsStu(){
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/getdoctorcancelledrequest',
		dataType : "json",
		success : function(data) {
			$("#cancelledAptList").empty();
			
			if(data.length>0){
				for(var i = 0; i<data.length ; i++){
					var apt = data[i];
					var reqInfo = convertToDate(apt.starttime).split("T");
					var trEle="";
					
					trEle = "<tr><td>"+convertNumnberDateToString(reqInfo[0])
					+"</td><td>"+convertFullDateToAPTime(convertToDate(apt.starttime))
					+"</td><td>"+apt.patientname+"</td>" +
			           "<td><a href='/"+"/evaluatesp_stu'>SP Rating Form</a></td>"+
			           "</tr>";
					$("#cancelledAptList").append(trEle);
				}
				$("#cancelledAptTable").show();
			}else{
				var obj = $("#cancelledAptTable").parent();
				obj.hide();
				$("#cancelledAptTable").hide();
				$("#cancelledAptNote").text("You don't have any cancelled appointments.");
			}
		}
	});
}


function startApt(aptId){
	window.location.href="/"+DOCTORPATH+"/start_stu/"+aptId;
}

function browserDetection(){
	
	var isFirefox = typeof InstallTrigger !== 'undefined';
	var isIE = false || !!document.documentMode;
	
	if(isFirefox==false || isIE==true){
		showWaitingPopUp();
	}
	
}


function showWaitingPopUp() {
	$.blockUI({
		message : $('#waitingPopup'),
		css : {
			top : ($(window).height() - 80) / 3 + 'px',
			left : ($(window).width() - 500) / 2 + 'px',
			width : '500px',
			height : '100px',
		}
	});
}
