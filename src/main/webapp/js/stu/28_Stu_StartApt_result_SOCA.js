var questionsData;
var pageLoadTime;

$(document).ready(function() {

	var answerId = $("#answerId").val();
	pageLoadTime = new Date().getTime();
	$.ajax({
		type : 'GET',
		url : '/survey/loadanswer/' + answerId,
		dataType : "json",
		success : function(data) {

			if (data != null) {
				
				var survey = data.survey;
				var content = data.content;
				var answers = eval(content);
				
				questionsData = survey;
				
				var surveyTitle = survey.title;
				var surveyDescription = survey.description;
				var surveyOrderOfQuestions = survey.orderOfQuestions;
				var questions = survey.questions;
				var questionOrders = surveyOrderOfQuestions.split(",");

				$("#surveyTitle").text(surveyTitle);
				$("#surveyDescription").text(surveyDescription);
               
				var divId = "questions";
				for (var i = 0; i < questionOrders.length; i++) {
					var answerForQuestion = null;
					var question = findQuestionById(questionOrders[i],questions);
					
					for (var j = 0; j < answers.length; j++) {
						if(answers[j].qid==question.id){
							answerForQuestion = answers[j];
						}
					}
					displayQuestion(question, divId, answerForQuestion);
				}
				disableAllInput();
				
				accessSecondSurvey();
				
			}
		}
	});
	

});


function accessSecondSurvey(){
	
	var answerIdSecond = $("#answerIdSecond").val();
	$.ajax({
		type : 'GET',
		url : '/survey/loadanswer/' + answerIdSecond,
		dataType : "json",
		success : function(data) {

			if (data != null) {
				
				var divId = "questions";
				
				var newHead = "<h1 id='surveyTitleSecond'></h1><div><p id='surveyDescriptionSecond'></p></div>";
				$("#"+divId).append(newHead);
				
				var survey = data.survey;
				var content = data.content;
				var answers = eval(content);
				
				questionsData = survey;
				
				var surveyTitle = survey.title;
				var surveyDescription = survey.description;
				var surveyOrderOfQuestions = survey.orderOfQuestions;
				var questions = survey.questions;
				var questionOrders = surveyOrderOfQuestions.split(",");

				$("#surveyTitleSecond").text(surveyTitle);
				$("#surveyDescriptionSecond").text(surveyDescription);
				
				for (var i = 0; i < questionOrders.length; i++) {
					var answerForQuestion = null;
					var question = findQuestionById(questionOrders[i],questions);
					
					for (var j = 0; j < answers.length; j++) {
						if(answers[j].qid==question.id){
							answerForQuestion = answers[j];
						}
					}
					displayQuestion(question, divId, answerForQuestion);
				}
				disableAllInput();
			}
		}
	});
	
}


function findQuestionById(id, questions) {

	for (var i = 0; i < questions.length; i++) {
		if (questions[i].id == id) {
			return questions[i];
		}
	}

	return null;
}

function displayQuestion(question, divId,answerForQuestion) {

	var element = "";
	if (question.typeS == "TEXTAREA") {
		element = displayTextarea(question, answerForQuestion);
	} else if (question.typeS == "TEXTFIELD") {
		element = displayTextfield(question, answerForQuestion);
	} else if (question.typeS == "RADIO") {
		element = displayRadio(question, answerForQuestion);
	} else if (question.typeS == "CHECKBOX") {
		element = displayCheckbox(question, answerForQuestion);
	}else if (question.typeS == "LIKERT") {
		element = displayLikert(question, answerForQuestion);
	}
	
	$("#"+divId).append(element);
	
}

function displayTextfield(question, answerForQuestion) {
	
	var element = "<li id='aaa' class='notranslate'>"
		+"<label class='desc'>"+ question.title + "</label> "
		+"<label>"+ question.description +" </label> "
		+"<div><input  id='q"+question.id+"' name='q"+question.id+"' type='text' class='field text medium' value='"+answerForQuestion.qvalue+"' maxlength='255' tabindex='28' onkeyup='' /></div>"
		+"</li><hr>";
	
	return element;
}

function displayTextarea(question, answerForQuestion) {
	
	var element = "<li id='aaa' class='notranslate'>"
		+"<label class='desc'>"+ question.title + "</label> "
		+"<label>"+ question.description +" </label> "
		+"<div><textarea  id='q"+question.id+"' name='q"+question.id+"' class='field textarea medium' spellcheck='true' rows='10' cols='50' tabindex='5' onkeyup=''>"+answerForQuestion.qvalue+"</textarea></div>"
		+"</li><hr>";
	
	return element;
}

function displayRadio(question, answerForQuestion) {
	
	qvalue = answerForQuestion.qvalue;
	var element = "<li id='foli115' class='notranslate threeColumns'>"
		+"<fieldset>"
		+"<legend id='title115' class='desc'> "+question.title+" </legend>"
		+"<label>"+ question.description +" </label> "
		+"<div>";
	
	var options = question.options;
	
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		var optvalue = option.optvalue;
		if(qvalue==optvalue){
			element += "<span> <input  name='q"+question.id+"' type='radio' class='field radio' checked='checked' value="+optvalue+" tabindex='8' />"
	           +"<label class='choice'>"+optkey+"</label>"
	           +"</span>";
		}else{
			element += "<span> <input  name='q"+question.id+"' type='radio' class='field radio' value="+optvalue+" tabindex='8' />"
	           +"<label class='choice'>"+optkey+"</label>"
	           +"</span>";
		}
		
	}
	
	element += "</div></fieldset></li><hr>";
	
	return element;
}

function displayCheckbox(question, answerForQuestion) {
	
	var options = question.options;
	
	var largestLength=0;
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		if(optkey.length>largestLength){
			largestLength = optkey.length;
		}
	}
	
	var style = "";
	if(largestLength>30){
		style = "oneColumn";
	}else{
		style = "threeColumns";
	}
	
	qvalues = answerForQuestion.qvalue.split(",");
	
	var element = "<li id='foli115' class='notranslate "+style+"'>"
		+"<fieldset>"
		+"<legend id='title115' class='desc'> "+question.title+" </legend>"
		+"<label>"+ question.description +" </label> "
		+"<div>";
	
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		var optvalue = option.optvalue;
		var temp="";
		for (var j = 0; j < qvalues.length; j++) {
			qvalue = qvalues[j];
			if(qvalue==optvalue){
				temp += "<span> <input  name='q"+question.id+"' type='checkbox' class='field checkbox' checked='checked' value="+optvalue+" tabindex='12' />"
		           +"<label class='choice'>"+optkey+"</label>"
		           +"</span>";
			}
		}
		
		if(temp==""){
			temp += "<span> <input  name='q"+question.id+"' type='checkbox' class='field checkbox' value="+optvalue+" tabindex='12' />"
	           +"<label class='choice'>"+optkey+"</label>"
	           +"</span>";
		}
		element += temp;
		
	}
	
	element += "</div></fieldset></li><hr>";

	return element;
}


function displayLikert(question, answerForQuestion){
	
	qvalue = answerForQuestion.qvalue;
	
	var options = question.options;
	
	var optionNumbers = options.length;
	var element = "<li id='foli115' class='likert notranslate col"+optionNumbers+"'>"
		+"<label>"+ question.description +" </label> "
		+"<table cellspacing='0'>"
		+"<thead><tr><th>&nbsp;</th>";
	
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		element += "<td>"+optkey+"</td>";
	}
	element += "</tr></thead><tbody><tr class='statement108 alt'>"
		     + "<th><label for='Field108'>"+question.title+"</label></th>";
		
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optvalue = option.optvalue;
		if(qvalue==optvalue){
			element += "<td title='"+optvalue+"'><input  name='q"+question.id+"' type='radio' tabindex='1' value='"+optvalue+"' checked='checked'/> <label>"+optvalue+"</label></td>";
		}else{
			element += "<td title='"+optvalue+"'><input  name='q"+question.id+"' type='radio' tabindex='1' value='"+optvalue+"' /> <label>"+optvalue+"</label></td>";
		}
	}
	element += "</tr></tbody></table></li><hr>";
	return element;

}


function disableAllInput(){
	$(":input").attr("disabled","disabled");
}


//nonverbalType = 31
function addReflectionLog(){
	
	currentReflectionSection = 31;
	sessionId = $("#reflogSessionId").val();
	currentReflectionStartTime = new Date().getTime();
	
	var reflectionLog= new Object();
	
	reflectionLog.sessionId = sessionId;
	reflectionLog.nonverbalType = currentReflectionSection;
	reflectionLog.startTime = pageLoadTime;
	
	var currentTime = new Date().getTime();
	reflectionLog.endTime = currentTime;
	
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/addreflectionlog',
		contentType : 'application/json',
		dataType : "json",
		data : JSON.stringify(reflectionLog),
		success : function(data) {
			if (data.code == 1) {
			} else {
			}
		}
	});
}

$(window).bind("beforeunload",addReflectionLog);


