$(document).ready(function() {

	$("#submitAgreement").click(function() {
		var doctorId = $("#doctorId").val();
		var agreement = $("input[name='agreement']:checked").val();
		if (agreement == 1) {
			var formdata = {
				"doctorId" : doctorId,
				"agreement" : agreement
			};
			$.ajax({
				type : 'POST',
				headers : getTokenHeader(),
				url : '/' + DOCTORPATH + '/updateagreement',
				contentType : 'application/json',
				dataType : "json",
				data : JSON.stringify(formdata),
				success : function(data) {
					if (data != null) {
						if (data.code == 1) {
							$.unblockUI();
						} else {
							alert("Submit failed, please try again");
						}
					}
				}
			});
		} else {
			$.unblockUI();
		}
	});
});