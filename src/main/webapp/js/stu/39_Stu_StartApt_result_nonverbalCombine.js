
//---------------for the turn-taking timeline: start---------------------
var timeline = null;
var data = null;
var order = 1;
var truck = 1;

var timeline_turntaking = null;
var timeline_headmovment = null;
var timeline_selftouchmovment = null;
var lastUpdatedTimeForTurnTakingWindow = 0;

//for feedback notes
var titlingObv = 30;
var movementObv = 0.3;
var movementTop = 50;

google.load("visualization", "1");

// Set callback to run when API is loaded
google.setOnLoadCallback(drawVisualization);

// Called when the Visualization API is loaded.
function drawVisualization() {
	
	// get the length of the video
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/totallength/'+video_name,
		dataType : "json",
		success : function(data) {
			 // right now we set the length of time as a fixed number, we need to find out the real numbmer
			duration_length = Math.ceil(data);
			var initTimeStr ='2015-01-01 00:00:00';
		    initTimeStr = initTimeStr.replace(/-/g,"/");
		    var initTime = new Date(initTimeStr);
		    var initLength = new Date(initTime.getTime() + duration_length* 1000);
			
		    var visibleStartTimeStr ='2015-01-01 00:00:00';
		    var visibleStopTimeStr ='2015-01-01 00:01:00';
		    var visibleStartTime = new Date(2015,0,1,0,0,0,0);
		    var visibleStopTime = new Date(2015,0,1,0,1,0,0);
		    
		    // specify options
		    var options = {
				width:  "100%",
		        // height: heightInPixel,
		        height: "auto",
		        layout: "box",
		        axisOnTop: true,
		        eventMargin: 10,  // minimal margin between events
		        eventMarginAxis: 0, // minimal margin between events and the axis
		        // editable: true,
		        showNavigation: false,
		        max: initLength,
		        min: initTime,
		        showCustomTime: true
		        // cluster: true,
		    };

		    /* time line of turn taking */
		    // Instantiate our timeline object.
		    timeline_turntaking = new links.Timeline(document.getElementById('turntaking'), options);
		    
		    // Create and populate a data table.
		    data_tt = new google.visualization.DataTable();
		    data_tt.addColumn('datetime', 'start');
		    data_tt.addColumn('datetime', 'end');
		    data_tt.addColumn('string', 'content');
		    data_tt.addColumn('string', 'group');
		    data_tt.addColumn('string', 'className');
		    data_tt.addColumn('string', 'id');
		    
		    // add doctor speaking period data
		    var endPoints = endPoints_doctor.split(";");
		    for(var i = 0; i<endPoints.length;i++){
		    	var points = endPoints[i].split(",");
		    	var startTime = new Date(initTime.getTime() + parseFloat(points[0])* 1000);
				var stopTime = new Date(initTime.getTime() + parseFloat(points[1])* 1000);
				var duration = parseFloat(points[1])-parseFloat(points[0]);
				data_tt.addRow([startTime, stopTime,duration.toFixed(2)+"s", "You", "doctor", 166+""]);
		    }
		    
		    // add patient speaking period data
		    var endPoints = endPoints_patient.split(";");
		    for(var i = 0; i<endPoints.length;i++){
		    	var points = endPoints[i].split(",");
		    	var startTime = new Date(initTime.getTime() + parseFloat(points[0])* 1000);
				var stopTime = new Date(initTime.getTime() + parseFloat(points[1])* 1000);
				var duration = parseFloat(points[1])-parseFloat(points[0]);
				data_tt.addRow([startTime, stopTime,duration.toFixed(2)+"s", "SP", "patient", 166+""]);
		    }
		    
		    // Draw our timeline with the created data and options
			timeline_turntaking.draw(data_tt);
			timeline_turntaking.setVisibleChartRange(visibleStartTime, visibleStopTime,true);
		    
		    google.visualization.events.addListener(timeline_turntaking, 'select',
		            function (event) {
				    	var row = getSelectedRow(timeline_turntaking);
				    	var startTime = data_tt.getValue(row, 0);
				
				    	var hours = startTime.getHours();   
				    	var minutes = startTime.getMinutes();   
				    	var seconds = startTime.getSeconds(); 
				    	var ms = startTime.getMilliseconds();  
				    	var totalSec = 60*60*hours + 60*minutes+seconds+ms/1000;
				    	jumpTo(totalSec);
		            }
		    );
		    google.visualization.events.addListener(timeline_turntaking, 'edit',
		            function() {
		            }
		    );
		    google.visualization.events.addListener(timeline_turntaking, 'change',
		            function() {
		            }
		    );
		    google.visualization.events.addListener(timeline_turntaking, 'add',
		            function() {
		            }
		    );
		    
		    google.visualization.events.addListener(timeline_turntaking, 'rangechange', 
		    		function() {
//		    			var myvideo = document.getElementById("video");
//		    			myvideo.pause();
//				        var range = timeline_turntaking.getVisibleChartRange();
//				        var windowStartTime = range.start;
//				        
//				        var munites = windowStartTime.getMinutes();
//				        var seconds = windowStartTime.getSeconds();
//				        
//				    	myvideo.currentTime=munites*60+seconds;
				    }
		    );
		    
		    /* time line of head movement */
		    timeline_headmovment = new links.Timeline(document.getElementById('headmovment'), options);

		    // Create and populate a data table.
		    data_hm = new google.visualization.DataTable();
		    data_hm.addColumn('datetime', 'start');
		    data_hm.addColumn('datetime', 'end');
		    data_hm.addColumn('string', 'content');
		    data_hm.addColumn('string', 'group');
		    data_hm.addColumn('string', 'className');
		    data_hm.addColumn('string', 'id');

			var noddings = noddingline.split(",");
			var shakings = shakingline.split(",");
			
			if(!(noddings.length==1 && noddings[0]=="")){
				for(var i = 0; i<noddings.length;i++){
					var temps = noddings[i].split(";");
					
					var startTime = new Date(initTime.getTime() + parseFloat(temps[1])* 1000);
					var stopTime = new Date(initTime.getTime() + parseFloat(temps[0])* 1000);
					var duration = parseFloat(temps[0])- parseFloat(temps[1]);
					data_hm.addRow([startTime, stopTime, duration.toFixed(2)+"s", "nodding", "doctor", 166+""]);
				}
			}else{
				// data_hm.addRow([0, 0, "1s", "nodding", "doctor", 166+""]);
			}
			if(!(shakings.length==1 && shakings[0]=="")){
				for(var i = 0; i<shakings.length;i++){
					var temps = shakings[i].split(";");
					
					var startTime = new Date(initTime.getTime() + parseFloat(temps[1])* 1000);
					var stopTime = new Date(initTime.getTime() + parseFloat(temps[0])* 1000);
					var duration = parseFloat(temps[0])- parseFloat(temps[1]);
					data_hm.addRow([startTime, stopTime,duration.toFixed(2)+"s", "shaking", "patient", 166+""]);
				}
			}

			// Draw our timeline with the created data and options
			timeline_headmovment.draw(data_hm);

		    google.visualization.events.addListener(timeline_headmovment, 'select',
		            function (event) {
				    	var row = getSelectedRow(timeline_headmovment);
				    	var startTime = data_hm.getValue(row, 0);
				
				    	var hours = startTime.getHours();   
				    	var minutes = startTime.getMinutes();   
				    	var seconds = startTime.getSeconds(); 
				    	var ms = startTime.getMilliseconds();  
				    	var totalSec = 60*60*hours + 60*minutes+seconds+ms/1000;
				    	jumpTo(totalSec);				    	
		            }
		    );
		    google.visualization.events.addListener(timeline_headmovment, 'edit',
		            function() {
		            }
		    );
		    google.visualization.events.addListener(timeline_headmovment, 'change',
		            function() {
		            }
		    );
		    google.visualization.events.addListener(timeline_headmovment, 'add',
		            function() {
		            }
		    ); 
		    
		}
	});
	
}

function getSelectedRow(timeline) {
    var row = undefined;
    var sel = timeline.getSelection();
    if (sel.length) {
        if (sel[0].row != undefined) {
            row = sel[0].row;
        }
    }
    return row;
}
//---------------for the turn-taking timeline: end---------------------



var OldInterval = 10;
var NewInterval = 3;

var speakPercentageChart = null;
function DynamicChangeTickIntervalSpeakPercentageChart(interval) {
	speakPercentageChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsSpeakPercentageChart() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalSpeakPercentageChart(OldInterval);
        this.zoom();
    }
    });
}


var smileChart = null;
function DynamicChangeTickIntervalSmile(interval) {
	smileChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsSmile() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalSmile(OldInterval);
        this.zoom();
    }
    });
}


var leaningChart = null;
function DynamicChangeTickIntervalLeaning(interval) {
	leaningChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsLeaning() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalLeaning(OldInterval);
        this.zoom();
    }
    });
}

var tiltingChart = null;
function DynamicChangeTickIntervalTilting(interval) {
	tiltingChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsTilting() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalTilting(OldInterval);
        this.zoom();
    }
    });
}

var frownChart = null;
function DynamicChangeTickIntervalFrown(interval) {
	frownChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsFrown() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalFrown(OldInterval);
        this.zoom();
    }
    });
}


var nofaceChart = null;
function DynamicChangeTickIntervalNoface(interval) {
	nofaceChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsNoface() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalNoface(OldInterval);
        this.zoom();
    }
    });
}

var highchartsDivWidth = 0; 
var highchartsDivHeight = 0;

var loudnessChart = null;
function DynamicChangeTickIntervalLoudness(interval) {
	loudnessChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsLoudness() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalLoudness(OldInterval);
        this.zoom();
    }
    });
}

var pitchChart = null;
function DynamicChangeTickIntervalPitch(interval) {
	loudnessChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsPitch() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalPitch(OldInterval);
        this.zoom();
    }
    });
}


var touchesChart = null;
function DynamicChangeTickIntervalTouches(interval) {
	touchesChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsTouches() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalTouches(OldInterval);
        this.zoom();
    }
    });
}


var movesChart = null;
function DynamicChangeTickIntervalMoves(interval) {
	movesChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsMoves() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalMoves(OldInterval);
        this.zoom();
    }
    });
}

var attentionChart = null;
function DynamicChangeTickIntervalAttention(interval) {
	attentionChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsAttention() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalAttention(OldInterval);
        this.zoom();
    }
    });
}

var stretchChart = null;
function DynamicChangeTickIntervalStretch(interval) {
	stretchChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsStretch() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalStretch(OldInterval);
        this.zoom();
    }
    });
}

var speechPercentageData = null;
var loudnessData = null;
var pitchData = null;
var smileData = null;
var frownData = null;
var leaningData = null;
var tiltingData = null;
var touchesData = null;
var movesData = null;
var attentionData = null;
var stretchData = null;


function displaySpeechPercentage(plotLines){
	
	var splitLength = parseInt($("#speakpercentagesplit").val())*60;
	
	// get the speech percentage
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/speechpercentage/'+video_name+"/"+splitLength,
		dataType : "json",
		success : function(data) {
			
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        },
		        lang: {
                    resetZoom: "Return",
                    resetZoomTitle: "Return to initial"
                }
		    });  
			
			var chart;  
		    chart = new Highcharts.Chart({  
				chart: {  
					renderTo: 'speechpercentagecontainer', 
		            type: 'spline',  
		            marginRight: 10,
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            height: highchartsDivHeight,
		            events: {
                        selection: function () {
                        	DynamicChangeTickIntervalSpeakPercentageChart(NewInterval);
                        }
                    }
		        },  
		        xAxis: {  
		            type: 'int',  
		            tickInterval: OldInterval,
		            plotLines:plotLines,
		        },  
		        yAxis: {  
		            title: {  
		                text: 'Speak Ratio'  
		            },  
		        },  
		        title: {  
		            text: ''  
		        },
		        plotOptions: {
		        	 spline: {
                        turboThreshold: 100000
		        	 }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        tooltip: {  
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y*100, 2)+"%"
		                    ;  
		            }  
		        },  
		        credits: {  
		            enabled: false  
		        },  
		        legend: {
		            layout: 'vertical',
		            align: 'right',
		            verticalAlign: 'top',
		            borderWidth: 0
		        }, 
		        
		        exporting: {  
		            enabled: false  
		        },  
		        
		        series: [
		        {  
		            name: 'You',
		            lineWidth: 2,
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;  
		                for (i = 1; i < data.length; i++) {
		                	if(data[i].doctorPercentage+data[i].patientPercentage == 0){
		                		drawdata.push({  
			                        x: data[i].time,  
			                        y: 0.5  
			                    });  
		                	}else{
		                		drawdata.push({  
			                        x: data[i].time,  
			                        y: data[i].doctorPercentage/(data[i].doctorPercentage+data[i].patientPercentage)  
			                    });  
		                	}
		                }
		                speechPercentageData = drawdata;
		                return drawdata;  
		            })()  
		        },
		        {  
		            name: 'SP',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;  
		                for (i = 1; i < data.length; i++) {
		                	
		                	if(data[i].doctorPercentage+data[i].patientPercentage==0){
		                		drawdata.push({  
			                        x: data[i].time,  
			                        y: 0.5  
			                    });  
		                	}else{
		                		drawdata.push({  
			                        x: data[i].time,  
			                        y: data[i].patientPercentage/(data[i].doctorPercentage+data[i].patientPercentage) 
			                    });   
		                	}
		                }
		                return drawdata;  
		            })()  
		        }]
			},function (chartObj) {
				speakPercentageChart = chartObj;
            });
		    ExtendHighchartsSpeakPercentageChart();
		    drawAnnotations(speakPercentageChart,speechPercentageData);
		}
	});
}

//draw sound properties: loudness, pitch, speech rate
function drawsoundProperty(totalLength, totalFrame){
	
	// get the loudness
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/loudness/'+video_name,
		dataType : "json",
		success : function(data) {
			
		    totalLength = data.totalLength;
			var loudness= data.loudness;
			
			var tags = loudness.split(",");
			var tagsLength = tags.length;
			var timeslot = totalLength/tagsLength;
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        },
		        lang: {
                    resetZoom: "Return",
                    resetZoomTitle: "Return to initial"
                }
		    });  
		    var chart;  
		    
		    chart = new Highcharts.Chart({  
		        chart: {  
		            renderTo: 'loudnesscontainer',  
		            type: 'area',  
		            marginRight: 10, 
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            height: highchartsDivHeight,
		            events: {
                        selection: function () {
                        	DynamicChangeTickIntervalLoudness(NewInterval);
                        }
                    }
		        },  
		        plotOptions: {
		        	 spline: {
                         turboThreshold: 100000
		        	 },
		        	 area: {
		        		 turboThreshold: 100000,
		                    fillColor: {
		                        linearGradient: {
		                            x1: 0,
		                            y1: 0,
		                            x2: 0,
		                            y2: 1
		                        },
		                        stops: [
		                            [0, Highcharts.getOptions().colors[0]],
		                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
		                        ]
		                    },
		                    marker: {
		                        radius: 2
		                    },
		                    lineWidth: 1,
		                    states: {
		                        hover: {
		                            lineWidth: 1
		                        }
		                    },
		                    threshold: null
		                }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        title: {  
		            text: ''  
		        },  
		        xAxis: {  
		            type: 'int', 
		            tickInterval: OldInterval
		            // tickPixelInterval: 150  
		        },  
		        yAxis: {  
		            title: {  
		                text: 'Sound power'  
		            },  
		            plotBands: [{
		            	color: '#FCFFC5',
		            	from: loudnessBottom, 
		                to: loudnessTop,
//		                label: {
//		                    text: 'average',
//		                    align: 'left',
//		                    x: -45,
//		                    style: {
//		                        color: '#606060'
//		                    }
//		                }
		            }],  
		        },  
		        tooltip: { 
		        	enabled: false,
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y, 2)
		                    ;  
		            }  
		        },  
		        legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },  
		        credits: {  
		            enabled: false  
		        },  
		        series: [{  
		            name: 'Sound power',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;
		                var totalNum = 0;
		                var totalVolume = 0;
		                var interval = 10;
		                var validNum = 0;
		                var prevValue = 0;
		                
		                if(tagsLength>10000){
		                	interval = 100;
		                }
		                
		                for (i = 0; i < tagsLength; i=i+1) {
		                	
		                	totalNum++;
		                	
		                	var number = new Number(tags[i]);
		                	var numberValue = number.valueOf();
		                	
		                	if(totalNum==interval){
		                		if(validNum==0){
		                			drawdata.push({  
				                        x: timeslot*(i-interval),
				                        y: 0
				                    });	
		                		}else{
		                			if(totalVolume == 0){
		                				drawdata.push({  
					                        x: timeslot*(i-interval),
					                		y: 0
					                    });
		                			}else{
		                				drawdata.push({  
					                        x: timeslot*(i-interval),
					                		y: totalVolume/validNum
					                    });	
		                			}
		                				
		                			prevValue = totalVolume/validNum;
		                		}
		                		
		                		totalNum = 0;
		                		totalVolume = 0;
		                		validNum=0;
		                	}else{
		                		if(numberValue>5 && numberValue<100){
		                			totalVolume = totalVolume + numberValue;
		                			validNum++;
		                		}
		                	}
		                }
		                loudnessData = drawdata;
 		                return drawdata;  
		            })()  
		        }],
		       
		    }, function (chartObj) {
		    	loudnessChart = chartObj;
            });
            ExtendHighchartsLoudness();
			drawAnnotations(loudnessChart, loudnessData);
            
		}
	});
	
	
	// get the pitch
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/pitch/'+video_name,
		dataType : "json",
		success : function(data) {
			
			var pitch= data.pitch;
			
			var tags = pitch.split(",");
			var tagsLength = tags.length;
			
			var timeslot = totalLength/tagsLength;
			
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        },
		        lang: {
                    resetZoom: "Return",
                    resetZoomTitle: "Return to initial"
                }
		    });  
		    var chart;  
		    chart = new Highcharts.Chart({  
		        chart: {  
		            renderTo: 'pitchcontainer',  
		            type: 'area',  
		            marginRight: 10,
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            height: highchartsDivHeight,
		            events: {
                        selection: function () {
                        	DynamicChangeTickIntervalPitch(NewInterval);
                        }
                    }
		        },  
		        title: {  
		            text: ''  
		        },
		        plotOptions: {
		        	 spline: {
                        turboThreshold: 100000
		        	 },
		        	 area: {
		        		 turboThreshold: 100000,
		                    fillColor: {
		                        linearGradient: {
		                            x1: 0,
		                            y1: 0,
		                            x2: 0,
		                            y2: 1
		                        },
		                        stops: [
		                            [0, Highcharts.getOptions().colors[0]],
		                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
		                        ]
		                    },
		                    marker: {
		                        radius: 2
		                    },
		                    lineWidth: 1,
		                    states: {
		                        hover: {
		                            lineWidth: 1
		                        }
		                    },
		                    threshold: null
		                }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        xAxis: {  
		            type: 'int',
		            tickInterval: OldInterval
		            // tickPixelInterval: 150  
		        },  
		        yAxis: {  
		            title: {  
		                text: 'Sound pitch'  
		            },  
		            plotBands: [{
		            	color: '#FCFFC5',
		            	from: pitchBottom, 
		                to: pitchTop,
//		                label: {
//		                    text: 'average',
//		                    align: 'left',
//		                    x: -45,
//		                    style: {
//		                        color: '#606060'
//		                    }
//		                }
		            }],  
		        },  
		        tooltip: {  
		        	enabled: false,
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y, 2)
		                    ;  
		            }  
		        },  
		        legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },  
		        credits: {  
		            enabled: false  
		        },  
		        series: [{  
		            name: 'Sound pitch',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;  
		                var totalNum = 0;
		                var totalVolume = 0;
		                var interval = 10;
		                var validNum = 0;
		                var prevValue = 0;
		                
		                if(tagsLength>10000){
		                	interval = 50;
		                }
		                
		                if(tagsLength<200){
		                	interval = 1;
		                }
		                
		                for (i = 0; i < tagsLength; i++) {
		                	totalNum++;
		                	var number = new Number(tags[i]);
		                	var numberValue = number.valueOf();
		                	if(totalNum==interval){
		                		if(validNum==0){
		                			drawdata.push({  
				                        x: timeslot*(i-interval),
				                        y: 0
				                    });	
		                		}else{
		                			prevValue = totalVolume/validNum;
		                			if(totalVolume==0){
		                				drawdata.push({  
					                        x: timeslot*(i-interval),
					                		y: 0
					                    });
		                			}else{
		                				drawdata.push({  
					                        x: timeslot*(i-interval),
					                		y: totalVolume/validNum
					                    });
		                			}
		                				
		                		}
		                		
		                		totalNum = 0;
		                		totalVolume = 0;
		                		validNum=0;
		                	}else{
		                		if(numberValue<70){
		                			totalVolume = totalVolume + numberValue;
		                			validNum++;
		                		}
		                	}
		                	
		                }
		                drawdata[0] =drawdata[1];
		                pitchData = drawdata;
 		                return drawdata;  
		            })()  
		        }]  
		    }, function (chartObj) {
		    	pitchChart = chartObj;
            });
            ExtendHighchartsPitch();
			drawAnnotations(pitchChart, pitchData);
		}
	});
	
}

function displaySmile(){
	// get the smiles
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/smiles/'+video_name,
		dataType : "json",
		success : function(data) {
			
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        } ,
		        lang: {
                    resetZoom: "Return",
                    resetZoomTitle: "Return to initial"
                } 
		    });  
		    var chart;  
		    chart = new Highcharts.Chart({  
		        chart: {  
		            renderTo: 'smilecontainer',  
		            type: 'area',  
		            marginRight: 10, 
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            height: highchartsDivHeight,
		            events: {
                        selection: function () {
                        	DynamicChangeTickIntervalSmile(NewInterval);
                        }
                    }
		        },  
		        title: {  
		            text: ''  
		        },  
		        xAxis: {  
		            type: 'int',  
		            tickInterval: OldInterval 
		        }, 
		        plotOptions: {
		        	 spline: {
                        turboThreshold: 100000
		        	 },
		        	 area: {turboThreshold: 100000,
		                    fillColor: {
		                        linearGradient: {
		                            x1: 0,
		                            y1: 0,
		                            x2: 0,
		                            y2: 1
		                        },
		                        stops: [
		                            [0, Highcharts.getOptions().colors[0]],
		                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
		                        ]
		                    },
		                    marker: {
		                        radius: 2
		                    },
		                    lineWidth: 1,
		                    states: {
		                        hover: {
		                            lineWidth: 1
		                        }
		                    },
		                    threshold: null
		                }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        yAxis: {  
		            title: {  
		                text: 'Smile intensity'  
		            },  
		            plotLines:[{
		            	color: 'red', // Color value
		                dashStyle: 'longdash', // Style of the plot line. Default to solid
		                value: 50, // Value of where the line will appear
		                width: 2 // Width of the line
					}]
		        },  
		        tooltip: {  
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y, 2)
		                    ;  
		            }  
		        },  
		        legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        }, 
		        credits: {  
		            enabled: false  
		        },  
		        series: [{  
		            name: 'Smile intensity',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;  
		                for (i = 0; i < data.length; i++) {
		                	drawdata.push({  
		                        x: data[i].tagId,  
		                        y: data[i].value  
		                    });  
		                }
		                smileData = drawdata;
		                return drawdata;  
		            })()  
		        }, {
					name:'',
					tyle:'scatter',
					marker: {
						enabled:false
					},
					data:[50]
					
				}]  
		    }, function (chartObj) {
		    	smileChart = chartObj;
            });
            ExtendHighchartsSmile();
            drawAnnotations(smileChart,smileData);
              
		}
	});
}

function displayFrown(){
	// get the frowns
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/frown/'+video_name,
		dataType : "json",
		success : function(data) {
			
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        } ,
		        lang: {
	                resetZoom: "Return",
	                resetZoomTitle: "Return to initial"
	            } 
		    });  
		    var chart;  
		    chart = new Highcharts.Chart({  
		        chart: {  
		            renderTo: 'frowncontainer',  
		            type: 'area',  
		            marginRight: 10,  
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            height: highchartsDivHeight,
		            events: {
	                    selection: function () {
	                    	DynamicChangeTickIntervalFrown(NewInterval);
	                    }
	                }
		        },  
		        title: {  
		            text: ''  
		        },  
		        xAxis: {  
		            type: 'int',  
		            tickInterval: OldInterval   
		        }, 
		        plotOptions: {
		        	 spline: {
                        turboThreshold: 100000
		        	 },
		        	 area: {turboThreshold: 100000,
		                    fillColor: {
		                        linearGradient: {
		                            x1: 0,
		                            y1: 0,
		                            x2: 0,
		                            y2: 1
		                        },
		                        stops: [
		                            [0, Highcharts.getOptions().colors[0]],
		                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
		                        ]
		                    },
		                    marker: {
		                        radius: 2
		                    },
		                    lineWidth: 1,
		                    states: {
		                        hover: {
		                            lineWidth: 1
		                        }
		                    },
		                    threshold: null
		                }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        yAxis: {  
		            title: {  
		                text: 'Frown intensity'  
		            },  
		            plotLines:[{
		            	color: 'red', 
		                dashStyle: 'longdash',
		                value: 50, 
		                width: 2
					}]
		        },  
		        tooltip: {  
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y, 2)
		                    ;  
		            }  
		        },  
		        legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },  
		        credits: {  
		            enabled: false  
		        },  
		        series: [{  
		            name: 'Frown intensity',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i; 
		                
		                for (i = 0; i < data.length-25; i=i+25) {
		                	
		                	var maxForwn = 0;
		                	
		                	for(var j = 0; j < 25; j++){
//		                		if(data[i+j].value>maxForwn){
//		                			maxForwn += data[i+j].value;
//		                		}
		                		if(data[i+j].value>maxForwn){
		                			maxForwn = data[i+j].value;
		                		}
		                	}
		                	drawdata.push({  
		                        x: data[i].time,  
		                        y:maxForwn
		                    });  
		                }
		                frownData = drawdata;
		                return drawdata;  
		            })()  
		        },{
					name:'',
					tyle:'scatter',
					marker: {
						enabled:false
					},
					data:[50]
					
				}]  
		    }, function (chartObj) {
		    	frownChart = chartObj;
	        });
	        ExtendHighchartsFrown();  
	        drawAnnotations(frownChart,frownData);
		}
	});
}

function displayLeaning(){
	// get the leanings
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/leanings/'+video_name,
		dataType : "json",
		success : function(data) {
			var items = data;
	    	var timeslots = new Array();
	    	var timeValues = new Array();
	    	var prevValue = 0;
	    	var interval = 25;
	    	// size
	    	var tempSize = 0;
	    	// number
	    	var tempTotal = 0;
	    	var validNum=0;
	    	for(var i = 0; i < items.length; i++){
	    		
	    		tempTotal++;
	    		var values = items[i].split("\t");
	    		var faceSize = parseFloat(values[0]);
	    		
	    		if(tempTotal == interval){
	    			if(validNum==0){
	    				timeslots.push(i/25);
		    			timeValues.push(prevValue);	
	        		}else{
	        			timeslots.push(i/25);
		    			timeValues.push(tempSize/validNum);	
	        			prevValue = tempSize/validNum;
	        		}
	        		
	    			tempTotal = 0;
	    			tempSize = 0;
	        		validNum=0;
	    		}else{
	    			if(faceSize != 0){
	    				tempSize += faceSize;
	    				validNum++;
	    			}
	    		}
	    	}
	    	Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        } ,
		        lang: {
                    resetZoom: "Return",
                    resetZoomTitle: "Return to initial"
                } 
		    });  
	    	
	        $('#leaningcontainer').highcharts({
	        	
	            chart: {
	            	renderTo: 'leaningcontainer',  
		            type: 'spline',  
		            marginRight: 10, 
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            height: highchartsDivHeight,
		            events: {
                     selection: function () {
                     	DynamicChangeTickIntervalLeaning(NewInterval);
                     }
                 }
	            },
	            title: {
	                text: ''
	            },
	            xAxis: {
	            	// categories: timeslots
	            	type: 'float',
	            	tickInterval: OldInterval
	            },
	            yAxis: {
	                title: {
	                    text: 'Head leaning'
	                },
	                plotBands: [{
		            	color: '#FCFFC5',
		            	from: leaningBottom, 
		                to: leaningTop,
//		                label: {
//		                    text: 'average',
//		                    align: 'left',
//		                    x: -45,
//		                    style: {
//		                        color: '#606060'
//		                    }
//		                }
		            }]  
	            },
	            
	            legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },
		        credits: {  
		            enabled: false  
		        },  
	            plotOptions: {
	                line: {
	                    dataLabels: {
	                        enabled: false
	                    },
	                    enableMouseTracking: true
	                },
	                spline: {
                        turboThreshold: 100000
		        	 }
	            },
	            annotationsOptions: {
		            enabledButtons: false   
		        },
	            series: [{  
	            	name: 'Leaning',
	            	events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;  
		                for (i = 0; i < timeslots.length; i++) {
		                	drawdata.push({  
		                        x: timeslots[i],  
		                        y: Math.floor(timeValues[i])  
		                    });  
		                }
		                leaningData = drawdata;
		                return drawdata;  
		            })()  
		        }]  
	            
	        }, function (chartObj) {
		    	leaningChart = chartObj;
            });
            ExtendHighchartsLeaning();
            drawAnnotations(leaningChart,leaningData);
		}
	});
}

function displayTilting(){
	// get the tiltings
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/tiltings/'+video_name,
		dataType : "json",
		success : function(data) {
			
			var items = data;
			var timeslots = new Array();
			var timeValues = new Array();
			
			for(var i = 0; i < items.length; i=i+25){
				
				timeslots.push(i/25);
				
				var tiltingValue = Math.floor(parseFloat(items[i])*100)/100;
				if(Math.abs(tiltingValue)>70){
					tiltingValue = 0;
				}
				timeValues.push(tiltingValue);
			}
			
			
			Highcharts.setOptions({  
				global: {  
					useUTC: false  
				} ,
				lang: {
					resetZoom: "Return",
					resetZoomTitle: "Return to initial"
				} 
			}); 
			
			$('#tiltingcontainer').highcharts({
				chart: {
					renderTo: 'tiltingcontainer',  
					type: 'spline',  
					marginRight: 10, 
					zoomType: 'x',
					width: highchartsDivWidth,
					height: highchartsDivHeight,
					events: {
						selection: function () {
							DynamicChangeTickIntervalTilting(NewInterval);
						}
					}
				},
				title: {
					text: ''
				},
				xAxis: {
					type: 'float',
					tickInterval: OldInterval 
				},
				yAxis: {
					title: {
						text: 'Head tilting'
					},
					plotBands: [
					{
		            	color: '#FCFFC5',
		            	from: tiltingBottom, 
		                to: tiltingTop,
//		                label: {
//		                    text: 'left',
//		                    align: 'left',
//		                    x: -45,
//		                    style: {
//		                        color: '#606060'
//		                    }
//		                }
		            }
		            ] 
				},
				credits: {  
					enabled: false  
				},  
				legend: {  
					enabled: false  
				},  
				exporting: {  
					enabled: false  
				},  
//            tooltip: {
//                enabled: true,
//                formatter: function() {
//                    return '<b>'+ this.series.name +'</b><br>'+this.x +': '+ this.y +'°C';
//                }
//            },
				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: true
					},
					spline: {
						turboThreshold: 100000
					}
					
				},
				annotationsOptions: {
		            enabledButtons: false   
		        },
				series: [{  
					name: 'Tilting',
					events:{
						click:function(event){
							jumpTo(event.point.x);
						}
					},
					lineWidth:2,
					data: (function() {  
						var drawdata = [],i;  
						//timeslots.length
						for (i = 0; i < timeslots.length; i++) {
							drawdata.push({  
								x: timeslots[i],  
								y: timeValues[i]
							});  
						}
						tiltingData = drawdata;
						return drawdata;  
					})()  
				}]  
			}
			, function (chartObj) {
				tiltingChart = chartObj;
			});
			ExtendHighchartsTilting();
			drawAnnotations(tiltingChart,tiltingData);
		}
	});
}

function displayNoFace(){
	
	// get the leanings
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/leanings/'+video_name,
		dataType : "json",
		success : function(data) {
			var items = data;
	    	var timeslots = new Array();
	    	var timeValues = new Array();
	    	var interval = 25;
	    	// number
	    	var tempTotal = 0;
	    	var validNum=0;
	    	for(var i = 0; i < items.length; i++){
	    		
	    		tempTotal++;
	    		var values = items[i].split(",");
	    		var faceSize = parseFloat(values[4]);
	    		
	    		if(tempTotal == interval){
	    			if(validNum<23){
	    				timeslots.push(i/25);
		    			timeValues.push(1);	
	        		}else{
	        			timeslots.push(i/25);
		    			timeValues.push(0);	
	        		}
	        		
	    			tempTotal = 0;
	    			tempSize = 0;
	        		validNum=0;
	    		}else{
	    			if(faceSize != 0){
	    				validNum++;
	    			}
	    		}
	    	}
	    	Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        } ,
		        lang: {
                    resetZoom: "Return",
                    resetZoomTitle: "Return to initial"
                } 
		    });  
	    	
	        $('#nofacecontainer').highcharts({
	        	
	            chart: {
	            	renderTo: 'nofacecontainer',  
		            type: 'spline',  
		            marginRight: 10, 
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            height: highchartsDivHeight,
		            events: {
                     selection: function () {
                     	DynamicChangeTickIntervalNoface(NewInterval);
                     }
                 }
	            },
	            title: {
	                text: '<b>Lost Face</b>'
	            },
	            xAxis: {
	            	type: 'float',
	            	tickInterval: OldInterval
	            },
	            yAxis: {
	                title: {
	                    text: 'Distance'
	                },
	                plotLines: [{  
		                value: 0,  
		                width: 1,  
		                color: '#808080'  
		            }]  
	            },
	          
	            legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },  
		        credits: {  
		            enabled: false  
		        },  
	            plotOptions: {
	                line: {
	                    dataLabels: {
	                        enabled: false
	                    },
	                    enableMouseTracking: true
	                },
	                spline: {
                        turboThreshold: 100000
		        	 }
	            },
	            annotationsOptions: {
		            enabledButtons: false   
		        },
	            series: [{  
	            	name: 'No face detected',
	            	events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;  
		                for (i = 0; i < timeslots.length; i++) {
		                	drawdata.push({  
		                        x: timeslots[i],  
		                        y: timeValues[i]  
		                    });  
		                }
		                return drawdata;  
		            })()  
		        }]  
	            
	            
	            
	        }, function (chartObj) {
		    	nofaceChart = chartObj;
            });
            ExtendHighchartsNoface();
		}
	});
}

function displayTouches(){
	// get the tiltings
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/touches/'+video_name,
		dataType : "json",
		success : function(data) {
			
		var timeslots = new Array();
		var timeValues = new Array();
		
		for(var i = 1; i < data.length-1; i++){
			var preTouchObj = data[i-1];
			var curTouchObj = data[i];
			var nextObj = data[i+1];
			var diff = curTouchObj.persentage - preTouchObj.persentage;
			var diffNext = curTouchObj.persentage - nextObj.persentage;
			
			timeslots.push(i/25);
			
			if(preTouchObj.persentage==0){
				timeValues.push(0);
			}else{
				if(diff<0){
					timeValues.push(0);
				}else{
					if(diff<40){
						if(diff>2 && diffNext<-2){
							console.log(diffNext+","+i);
							timeValues.push(diff*10);
						}else{
							timeValues.push(0);
						}
					}
					
				}
			}
		}
			
			Highcharts.setOptions({  
				global: {  
					useUTC: false  
				} ,
				lang: {
					resetZoom: "Return",
					resetZoomTitle: "Return to initial"
				} 
			}); 
			
			$('#touchescontainer').highcharts({
				chart: {
					renderTo: 'touchescontainer',  
					type: 'area',  
					marginRight: 10, 
					zoomType: 'x',
					width: highchartsDivWidth,
					height: highchartsDivHeight,
					events: {
						selection: function () {
							DynamicChangeTickIntervalTouches(NewInterval);
						}
					}
				},
				title: {
					text: ''
				},
				xAxis: {
					type: 'float',
					tickInterval: OldInterval 
				},
				yAxis: {
					title: {
						text: 'Hand gesture'
					},
					plotLines:[{
		            	color: 'red', // Color value
		                dashStyle: 'longdash', // Style of the plot line. Default to solid
		                value: 2, // Value of where the line will appear
		                width: 2 // Width of the line
					}] 
				},
				credits: {  
					enabled: false  
				},  
				legend: {  
					enabled: false  
				},  
				exporting: {  
					enabled: false  
				},  
				plotOptions: {
					area: {
						turboThreshold: 100000,
	                    fillColor: {
	                        linearGradient: {
	                            x1: 0,
	                            y1: 0,
	                            x2: 0,
	                            y2: 1
	                        },
	                        stops: [
	                            [0, Highcharts.getOptions().colors[0]],
	                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
	                        ]
	                    },
	                    marker: {
	                        radius: 2
	                    },
	                    lineWidth: 1,
	                    states: {
	                        hover: {
	                            lineWidth: 1
	                        }
	                    },
	                    threshold: null,
	                    turboThreshold: 100000
	                }
				},
				annotationsOptions: {
		            enabledButtons: false   
		        },
				series: [{  
					name: 'Percentage',
					events:{
						click:function(event){
							jumpTo(event.point.x);
						}
					},
					lineWidth:1,
					data: (function() {  
						var drawdata = [],i,j; 
						var timeResult=[];
						var valueResult =[];
						for (i = 25; i < timeslots.length; i=i+25) {
							var tempTotal = 0;   
							for (j = 1; j <= 25 ; j=j+1){
								tempTotal += timeValues[i-j];
							}
							timeResult.push(timeslots[i-25]);
							valueResult.push(tempTotal);
						}
						//timeslots.length
						for (i = 0; i < timeResult.length; i++) {
							if(isNaN(valueResult[i])){
								drawdata.push({  
									x: timeResult[i],  
									y: 0
								});  
							}else{
								drawdata.push({  
									x: timeResult[i],  
									y: Math.floor(valueResult[i]*1000)/1000
								});  
							}
						}
						
						touchesData = drawdata;
						// drawdata = sparseArray(drawdata,15);
						return drawdata;  
					})()  
				}, {
					name:'',
					tyle:'scatter',
					marker: {
						enabled:false
					},
					data:[2]
					
				}]  
			}
			, function (chartObj) {
				touchesChart = chartObj;
			});
			ExtendHighchartsTouches();
			drawAnnotations(touchesChart,touchesData);
		}
	});
}

function displayMovements(){
	// get the tiltings
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/moves/'+video_name,
		dataType : "json",
		success : function(data) {
			
		var timeslots = new Array();
		var timeValues = new Array();
		
		var timeslots = new Array();
		var timeValues = new Array();
		
		for(var i = 1; i < data.length; i++){
			var preTouchObj = data[i-1];
			var curTouchObj = data[i];
			timeslots.push(curTouchObj.lineNo/25);
			
			if((curTouchObj.persentage-preTouchObj.persentage)>0.4){
				timeValues.push(0);
			}else{
				timeValues.push(curTouchObj.persentage);
			}
		}
			
		var specialMoment = [];
		var lastMoment = -1;
        for (var i = 0; i < timeslots.length; i++) {
        	if(timeValues[i]>movementObv && (timeslots[i]-lastMoment)>3){
        		specialMoment.push(timeslots[i]);
        		lastMoment = timeslots[i];
        	}
        }
			
        Highcharts.setOptions({  
			global: {  
				useUTC: false  
			} ,
			lang: {
				resetZoom: "Return",
				resetZoomTitle: "Return to initial"
			} 
		}); 
		
		$('#movementcontainer').highcharts({
			chart: {
				renderTo: 'movementcontainer',  
				type: 'area',  
				marginRight: 10, 
				zoomType: 'x',
				width: highchartsDivWidth,
				height: highchartsDivHeight,
				events: {
					selection: function () {
						DynamicChangeTickIntervalMoves(NewInterval);
					}
				}
			},
			title: {
				text: '<b></b>'
			},
			xAxis: {
				type: 'float',
				tickInterval: OldInterval 
			},
			yAxis: {
				title: {
					text: 'Face movement'
				},
				plotLines:[{
	            	color: 'red', // Color value
	                dashStyle: 'longdash', // Style of the plot line. Default to solid
	                value: movementTop, // Value of where the line will appear
	                width: 2 // Width of the line
				}]	
			},
			credits: {  
				enabled: false  
			},  
			legend: {  
				enabled: false  
			},  
			exporting: {  
				enabled: false  
			},  
			plotOptions: {
				line: {
					dataLabels: {
						enabled: true
					},
					enableMouseTracking: true
				},
				spline: {
					turboThreshold: 100000
				},
				area: {
					turboThreshold: 100000,
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
				
			},
			annotationsOptions: {
	            enabledButtons: false   
	        },
			series: [{  
				name: 'Percentage',
				events:{
					click:function(event){
						jumpTo(event.point.x);
					}
				},
				lineWidth:1,
				data: (function() {  
					var drawdata = [],i;  
					//timeslots.length
					for (i = 0; i < timeslots.length; i++) {
						drawdata.push({  
							x: timeslots[i],  
							y: Math.floor(timeValues[i]*1000)/1000
						});  
					}
					drawdata = sparseArray( drawdata, 5);
					movesData = drawdata;
					return drawdata;  
				})()  
			}, {
				name:'',
				tyle:'scatter',
				marker: {
					enabled:false
				},
				data:[movementTop]
				
			}]  
		}
		, function (chartObj) {
			movesChart = chartObj;
		});
		ExtendHighchartsMoves();
		drawAnnotations(movesChart,movementsData);
		}
	});
}

function displayAttention(){
	// get the frowns
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/attention/'+video_name,
		dataType : "json",
		success : function(data) {
			
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        } ,
		        lang: {
	                resetZoom: "Return",
	                resetZoomTitle: "Return to initial"
	            } 
		    });  
		    var chart;  
		    chart = new Highcharts.Chart({  
		        chart: {  
		            renderTo: 'attentioncontainer',  
		            type: 'area',  
		            marginRight: 10,  
		            zoomType: 'x',
		            width: highchartsDivWidth,
					height: highchartsDivHeight,
		            events: {
	                    selection: function () {
	                    	DynamicChangeTickIntervalAttention(NewInterval);
	                    }
	                }
		        },  
		        title: {  
		            text: '<b></b>'  
		        },  
		        xAxis: {  
		            type: 'int',  
		            tickInterval: OldInterval   
		        }, 
		        plotOptions: {
		        	 spline: {
                        turboThreshold: 100000
		        	 },
		        	 area: {
		        		    turboThreshold: 100000,
		                    fillColor: {
		                        linearGradient: {
		                            x1: 0,
		                            y1: 0,
		                            x2: 0,
		                            y2: 1
		                        },
		                        stops: [
		                            [0, Highcharts.getOptions().colors[0]],
		                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
		                        ]
		                    },
		                    marker: {
		                        radius: 2
		                    },
		                    lineWidth: 1,
		                    states: {
		                        hover: {
		                            lineWidth: 1
		                        }
		                    },
		                    threshold: null
		                }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        
		        yAxis: {  
		            title: {  
		                text: 'Insttention intensity'  
		            },  
		            plotLines:[{
		            	color: 'red', 
		                dashStyle: 'longdash',
		                value: 50, 
		                width: 2
					}]
		        },  
		        tooltip: {  
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y, 2)
		                    ;  
		            }  
		        },  
		        legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },  
		        credits: {  
		            enabled: false  
		        },  
		        series: [{  
		            name: 'Inattention intensity',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i; 
		                
		                for (i = 0; i < data.length-25; i=i+25) {
		                	
		                	var maxAttention = 0;
		                	
		                	for(var j = 0; j < 25; j++){
		                		if(data[i+j].value>maxAttention){
		                			maxAttention = data[i+j].value;
		                		}
		                	}
		                	drawdata.push({  
		                        x: data[i].time,  
		                        y:maxAttention
		                    });  
		                }
		                attentionData = drawdata;
		                return drawdata;  
		            })()  
		        },{
					name:'',
					tyle:'scatter',
					marker: {
						enabled:false
					},
					data:[50]
					
				}]  
		    }, function (chartObj) {
		    	attentionChart = chartObj;
	        });
	        ExtendHighchartsAttention();  
	        drawAnnotations(attentionChart,attentionData);
		}
	});
}


function displayStretch(){
	// get the frowns
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/stretch/'+video_name,
		dataType : "json",
		success : function(data) {
			
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        } ,
		        lang: {
	                resetZoom: "Return",
	                resetZoomTitle: "Return to initial"
	            } 
		    });  
		    var chart;  
		    chart = new Highcharts.Chart({  
		        chart: {  
		            renderTo: 'stretchcontainer',  
		            type: 'area',  
		            marginRight: 10,  
		            zoomType: 'x',
		            width: highchartsDivWidth,
					height: highchartsDivHeight,
		            events: {
	                    selection: function () {
	                    	DynamicChangeTickIntervalStretch(NewInterval);
	                    }
	                }
		        },  
		        title: {  
		            text: '<b></b>'  
		        },  
		        xAxis: {  
		            type: 'int',  
		            tickInterval: OldInterval   
		        }, 
		        plotOptions: {
		        	 spline: {
                        turboThreshold: 100000
		        	 },
		        	 area: {
		        		    turboThreshold: 100000,
		                    fillColor: {
		                        linearGradient: {
		                            x1: 0,
		                            y1: 0,
		                            x2: 0,
		                            y2: 1
		                        },
		                        stops: [
		                            [0, Highcharts.getOptions().colors[0]],
		                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
		                        ]
		                    },
		                    marker: {
		                        radius: 2
		                    },
		                    lineWidth: 1,
		                    states: {
		                        hover: {
		                            lineWidth: 1
		                        }
		                    },
		                    threshold: null
		                }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        
		        yAxis: {  
		            title: {  
		                text: 'Lip stretch'  
		            },  
		            plotLines:[{
		            	color: 'red', 
		                dashStyle: 'longdash',
		                value: 50, 
		                width: 2
					}]
		        },  
		        tooltip: {  
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y, 2)
		                    ;  
		            }  
		        },  
		        legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },  
		        credits: {  
		            enabled: false  
		        },  
		        series: [{  
		            name: 'Lip stretch',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i; 
		                
		                for (i = 0; i < data.length-25; i=i+25) {
		                	
		                	var maxStretch = 0;
		                	
		                	for(var j = 0; j < 25; j++){
		                		if(data[i+j].value>maxStretch){
		                			maxStretch = data[i+j].value;
		                		}
		                	}
		                	drawdata.push({  
		                        x: data[i].time,  
		                        y:maxStretch
		                    });  
		                }
		                stretchData = drawdata;
		                return drawdata;  
		            })()  
		        },{
					name:'',
					tyle:'scatter',
					marker: {
						enabled:false
					},
					data:[50]
					
				}]  
		    }, function (chartObj) {
		    	stretchChart = chartObj;
	        });
	        ExtendHighchartsAttention();  
	        drawAnnotations(stretchChart,stretchData);
		}
	});
}


$(document).ready(function(){

	highchartsDivWidth = $("#turntaking").css("width").slice(0,-2);
	highchartsDivHeight = 250;
	
	var videoDiv = $("#video");
	videoDiv.css("width",480);
	
	$("#speakpercentagesplit").change(function(){
		var splitLength = parseInt($("#speakpercentagesplit").val())*60;
		plotLines = new Array();
		for(var i = 1; i <= duration_length/splitLength; i++){
			var line = {
					value:splitLength*i,
					color: 'blue',
		        	dashStyle: 'solid',
		        	width:2,
		        	id: 'plot-line-'+(i+1)
				};
			plotLines.push(line);
		}
		
		displaySpeechPercentage(plotLines);
		setTimeout(function(){
			$("#nonverbal_li2").click();
			$("#nonverbal_li2").click();
			$("#nonverbal_li2").click();
		}, 1000);
		
	});
	
	
	$("#feedbackButton").click(function(){
		var url = window.location.href;
		var archiveId = url.split("&")[1];
		
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+DOCTORPATH+'/video/'+archiveId,
			dataType : "json",
			success : function(data) {
				if(data!=null){
					var sessionId = data.sessionId;
					window.location.href="/stu/nonverbalfeedback_stu/"+sessionId;
				}
			}
		});
	});
	
	hideAllNonverbal();
	var itemsStr = items.split(',');
	
	for(var i = 0; i < itemsStr.length; i++){
		var itemNo  = itemsStr[i];
		
		$("#nonverbal_div"+itemNo).show();
		if(itemNo==2){
			displaySpeechPercentage();
		}else if(itemNo==3){
			drawsoundProperty(totalLength, totalFrame);
		}else if(itemNo==4){
			drawsoundProperty(totalLength, totalFrame);
		}else if(itemNo==5){
			displaySmile();
		}else if(itemNo==6){
			displayFrown();
		}else if(itemNo==7){
			displayLeaning();
		}else if(itemNo==8){
			displayTilting();
		}else if(itemNo==10){
			displayTouches();
		}else if(itemNo==11){
			displayMovements();
		}else if(itemNo==12){
			displayAttention();
		}else if(itemNo==13){
			displayStretch();
		}
	}
	
	
	var myvideo = document.getElementById("video");
	
	myvideo.ontimeupdate = function(){
		
		var startTime = myvideo.currentTime;
		var startTimeMunite = Math.floor(startTime/60);
		var startTimeSecond = Math.floor(startTime)%60;
		
		var windowStartTime = new Date(2015,0,1,0,startTimeMunite,startTimeSecond,0);
		
		timeline_headmovment.setCustomTime(windowStartTime);
		
		if(lastUpdatedTimeForTurnTakingWindow!=windowStartTime){
			lastUpdatedTimeForTurnTakingWindow = windowStartTime;
			var windowEndTime = new Date(2015,0,1,0,startTimeMunite+1,startTimeSecond,0);
			timeline_turntaking.setVisibleChartRange(windowStartTime, windowEndTime);
		}
		
		for(var i = 0; i < itemsStr.length; i++){
			var itemNo  = itemsStr[i];
			
			if(itemNo==2){
				updateCurrentTime(speakPercentageChart);
			}else if(itemNo==3){
				updateCurrentTime(loudnessChart, loudnessData);
				updateCurrentTime(pitchChart, pitchData);
			}else if(itemNo==4){
				updateCurrentTime(loudnessChart, loudnessData);
				updateCurrentTime(pitchChart, pitchData);
			}else if(itemNo==5){
				updateCurrentTime(smileChart);
			}else if(itemNo==6){
				updateCurrentTime(frownChart);
			}else if(itemNo==7){
				updateCurrentTime(leaningChart);
			}else if(itemNo==8){
				updateCurrentTime(tiltingChart);
			}else if(itemNo==10){
				updateCurrentTime(touchesChart);
			}else if(itemNo==11){
				updateCurrentTime(movesChart);
			}else if(itemNo==12){
				updateCurrentTime(attentionChart);
			}else if(itemNo==13){
				updateCurrentTime(stretchChart);
			}
		}
	};	
	
});

function drawAnnotations(currentChart,currentData){
	for (var i = 0; i < comments.length; i++) {
		var comment = comments[i].comment;
		var time = comments[i].time;
		var itemInData = findNearestPoint(time, currentData);
		
		currentChart.addAnnotation({
        	xValue: itemInData.x,
        	yValue: itemInData.y,
        	title: {
        		text: 'C',
        	},
        	linkedTo: comment,
            allowDragY: false,
            allowDragX: false,
            anchorX: "center",
            anchorY: "center",
            shape: {
                type: 'circle',
                params: {
                    r: 9,
                    stroke: '#c55'
                }
            },
            events: {
            	mouseover: function(e){
            		
            	  var  tempComment = findInComment(comments,this.options.xValue);
            	  
            	  $("#reporting").html(tempComment.noteContent);
                  var left = e.pageX+10;
                  var top = e.pageY-10;
                  $("#reporting").css("top",top + "px");
                  $("#reporting").css("left",left + "px"); 
                  $("#reporting").show();
            	},
            	click: function(e){
            		jumpTo(this.options.xValue);
            	},
	            mouseout: function(e){
	            	$("#reporting").hide();
	            	$("#reporting").html("");
	            }            
            	
            }
         });
	}
}

function updateCurrentTime(currentChart){
	var myvideo = document.getElementById("video");
	currentChart.xAxis[0].removePlotLine('plot-line-1');
	currentChart.xAxis[0].addPlotLine({
		value:myvideo.currentTime,
		color: 'red',
    	dashStyle: 'solid',
    	width:2,
    	id: 'plot-line-1'
	});	
}



function jumpTo(time){
	var myvideo = document.getElementById("video");
	myvideo.currentTime=time;
}


function seeDetail(detailName){
	var temp = "tr[name='"+detailName+"']";
	$(temp).toggle();
}

function sparseArray(denseArray, intervalNumber){
	
	// return denseArray;
	var returnArray = [],i;
	for (i = 0; i < denseArray.length; i=i+intervalNumber) {
	    	var temp  = denseArray[i];
	    	returnArray.push(temp);
	}
	 return returnArray;
}

function hideAllNonverbal(){
	for (var i = 0; i < 14; i++) {
		$("#nonverbal_div"+i).hide();
	}
//	$("#nonverbal_div1").hide();
//	$("#nonverbal_div2").hide();
//	$("#nonverbal_div9").hide();
	
}

function removeAllActive(){
	for (var i = 0; i < 15; i++) {
		$("#nonverbal_li"+i).removeAttr("class");
	}
}


function findNearestPoint(time, data){
	
	var difference = 1000000;
	var result = 0;
	for (var i = 0; i < data.length; i++) {
		var timeInData = data[i].x;
		if(Math.abs(timeInData-time)<difference){
			difference = Math.abs(timeInData-time);
			result = data[i];
		}
	}
	
	return result;
}



function findInComment(comments, xValue){
	
	for (var i = 0; i < comments.length; i++) {
		var comment = comments[i];
		if(Math.abs(comment.time-xValue)<2){
			return comment;
		}
	}
	
	return null; 
}


$(window).bind("beforeunload",submitReflectionLogCloseWindow);

function submitReflectionLogCloseWindow(){
	console.log("closing the window");
	
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/updatereflectionlog/' + reflectionLogId,
		dataType : "json",
		success : function(data) {
		}
	});
}

