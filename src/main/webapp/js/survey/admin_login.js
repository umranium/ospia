$(document).ready(function() {
	
	validate();
	
});

function validate(){
	
	$("#loginForm").validate({
		rules: {
			j_username: {
				required:true,
				notblank:true,
			},
			j_password: {
				required: true,
				notblank:true,
			},
		},
		messages: {
			j_username: {
				required:"Please enter your username",
				notblank:"Not a valid firstname",
			},
			j_password: {
				required: "Please provide a password",
				minlength: "Your password must be 6-20 characters long",
			},
		},
		
	});
	
	
}
