$(document).ready(function() {
	
	// user log into the system
	$("#type").change(function() {
		var typeText =$("#type option:selected").text();
		if (hasOption(typeText)) {
			$("#allOptions").empty();
			$("#addoption").click();
			$("#allOptionsDiv").show();
		} else {
			$("#allOptionsDiv").hide();
		}
	});

	var optionNumber = 0;

	$("#addoption").click(function() {
		optionNumber++;
		var optionId = "option" + optionNumber;
		var delteOptionId = "delete" + optionNumber;

		var divElement = "<div id='"+ optionId + "'>"
				+ "<label class='col-md-1 control-label' for='textinput'> option key: </label> " 
				+ "<input class='col-md-3' id='optkey"+ optionNumber+ "' name='optkey' type='text' />"
				+ "<label class='col-md-1 control-label' for='textinput'> option value: </label> " 
				+ "<input class='col-md-3' id='optvalue"+ optionNumber+ "' name='optvalue' type='text' />"
				+ " <input class='usyd-ui-button-primary' type='button' onclick='deleteOption("+ optionNumber+ ")' id='"+ delteOptionId + "' value='delete'>"
				+ " <input class='usyd-ui-button-primary' type='button' onclick='moveUp("+ optionNumber+ ")' id='"+ delteOptionId + "' value='up'>"
				+ " <input class='usyd-ui-button-primary' type='button' onclick='moveDown("+ optionNumber+ ")' id='"+ delteOptionId + "' value='down'>";
		$("#allOptions").append(divElement);
	});

	$("#saveQuestion").click(function() {
		var question = new Object();
		var questionType = new Object();
		
		var title = $("#title").val();
		var description = $("#description").val();
		var typeId = $("#type").val();
		var typeText =$("#type option:selected").text();
		questionType.id=typeId;
		questionType.type=typeText;
		
		question.title = title;
		question.description = description;
		question.typeS= typeText;
		question.type = questionType;
		
		
		if (hasOption(typeText)) {
			var options = $('[id^=option]');
			var optionsArray = new Array();
			$.each(options, function(i, val) {
				var divId = val.id;
				var idNumber = parseInt(divId.substr(6));
				console.log(idNumber);
				var tempOption = new Object();
				tempOption.optkey = $("#optkey" + idNumber)
						.val();
				tempOption.optvalue = $(
						"#optvalue" + idNumber).val();
				optionsArray.push(tempOption);
			});
			question.options = optionsArray;
		}
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+SURVEYPATH+'/savequestion',
			contentType : 'application/json',
			dataType : "json",
			data : JSON.stringify(question),
			success : function(data) {
				if (data.code == 1) {
					alert("save success");
					
				} else {
					alert("save fail");
				}
			}
		});

	});

});

function deleteOption(id) {
	var divParent = $("#option" + id);
	divParent.remove();

}


function moveUp(id){
	var preDiv = null;
	var currentDiv = null;
	var options = $('[id^=option]');
	
	$.each(options, function(i, val) {
		preDiv = currentDiv;
		currentDiv = val;
		
		var divId = currentDiv.id;
		var idNumber = parseInt(divId.substr(6));
		if(idNumber == id){
			return false;
		}
	});
	
	if(preDiv!=null && currentDiv!=null){
		preDiv = $("#"+preDiv.id);
		currentDiv = $("#"+currentDiv.id);
		currentDiv.after(preDiv);
	}
}

function moveDown(id){
	var currentDiv = null;
	var nextDiv = null;
	var options = $('[id^=option]');
	
	var findTheObj = false;
	$.each(options, function(i, val) {
		currentDiv = nextDiv;
		nextDiv = val;
		
		if(currentDiv!=null){
			var divId = currentDiv.id;
			var idNumber = parseInt(divId.substr(6));
			if(idNumber == id){
				findTheObj = true;
				return false;
			}	
		}
	});
	
	if(nextDiv!=null && currentDiv!=null && findTheObj==true){
		nextDiv = $("#"+nextDiv.id);
		currentDiv = $("#"+currentDiv.id);
		nextDiv.after(currentDiv);
	}
}


function hasOption(typeText){
	if(typeText == "RADIO" || typeText == "CHECKBOX"){
		return true;
	}
	return false;
}



function deleteQuestion(questionId){
	if(confirm("Delete this question?")){
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+SURVEYPATH+'/deletequestion/'+questionId,
			dataType : "json",
			success : function(data) {
				if (data.code == 1) {
					alert("delete success");
					window.location.reload();
				} else {
					alert(data.content);
				}
			}
		});
	}
}




