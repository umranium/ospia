<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Program Information</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/sp/02_SP_login.js"></script>
</head>

<body class="usyd-js-load">
	<div class="usyd-page usyd-no-feedback usyd-no-yammer ">
		<c:import url="/spheader.jsp"></c:import>

		<c:import url="/programinfo_sp.jsp"></c:import>

		<c:import url="/footer.jsp"></c:import> 
	</div>
</body>
</html>
