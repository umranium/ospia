<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Registration Edit</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript">
	var patientid = ${sessionScope.loginedPatient.patientid};
</script>
<script src="<%=request.getContextPath()%>/js/sp/08_SP_RegistEdit.js"></script>

</head>

<body class="usyd-js-load">
	<div class="usyd-page usyd-no-feedback usyd-no-yammer ">
		<c:import url="/spheader.jsp"></c:import>

		<form class="form-horizontal">
			<fieldset>

				<div class="form-group">
					<label class="col-md-4 control-label" for="textinput"></label>
					<div class="col-md-4">
						<h3>Register as a simulated patient</h3>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for="textinput">Name</label>
					<div class="col-md-4">
						<input id="name" name="name" type="text" value=""
							placeholder="Name" class="form-control input-md">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="textinput">Email</label>
					<div class="col-md-4">
						<input id="username" name="username" type="text" value=""
							placeholder="Email" class="form-control input-md">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="radios">Gender</label>
					<div class="col-md-4">
						<label class="radio-inline" for="radios-0"> <input
							type="radio" name="radios" id="radios-0" value="1"
							checked="checked"> Female
						</label> <label class="radio-inline" for="radios-1"> <input
							type="radio" name="radios" id="radios-1" value="2"
							checked="checked"> Male
						</label>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="selectbasic">Day
						Of Birth</label>
					<div class="col-md-4 input-group" style="padding-left:15px">

						<select id="selectbasic" name="selectbasic" class="form-control"
							style="width:100px;">
							<option value="0">Day</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3" selected="selected">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
						</select> <select id="selectbasic" name="selectbasic" class="form-control"
							style="width:100px;">
							<option value="0" selected="selected">Month</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5" selected="selected">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
						</select> <select id="selectbasic" name="selectbasic" class="form-control"
							style="width:100px;">
							<option value="0" selected="selected">Year</option>
							<option value="1">1970</option>
							<option value="2">1971</option>
							<option value="3">1972</option>
							<option value="4">1973</option>
							<option value="5">1974</option>
							<option value="6">1975</option>
							<option value="7" selected="selected">1976</option>
						</select>
					</div>
				</div>

				<!-- Button -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="singlebutton"></label>
					<div class="col-md-4">
						<a href="07_SP_HomeRegistered.jsp"> <input type="button"
							id="singlebutton" name="singlebutton"
							class="usyd-ui-button-primary" value="Edit">
						</a>
					</div>
				</div>

			</fieldset>
		</form>

		<c:import url="/footer.jsp"></c:import> 

	</div>

</body>
</html>
