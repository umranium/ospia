<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Training</title>
<c:import url="/common.jsp"></c:import>
<script src="<%=request.getContextPath()%>/js/sp/09_SP_Training.js"></script>

</head>

<body>
	<c:import url="/spheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<input type="hidden" id="patientid" name="patientid" value="<sec:authentication property="principal.patient.patientid"/>">
						<input type="hidden" id="trainedsceids" name="trainedsceids"
							value="<sec:authentication property="principal.patient.trainedsceids"/>">

						<div class="jumbotron" style="padding-top:0px;padding-bottom:0px;margin-bottom:0px;">
							<h3>Welcome to the training page for OSPIA.The videos will take less than 30 minutes to view in total. <br/>If there are any issues, please contact
					                 CS Administrator</h3><br>
                                        <b>T: </b> +61 (2) 9385 2550  <b> &nbsp;&nbsp;E:</b> csadmin@unsw.edu.au</p>
							<b style="text-align:left"> <a target="_tab" href="<%=request.getContextPath()%>/files/CodeOfConductSP.pdf"> <img
									class="pdficon" src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">The code of
										conduct</span>
							</a> &nbsp;&nbsp;&nbsp;&nbsp; <a target="_tab" href="<%=request.getContextPath()%>/files/GuideToInterviewSP.pdf"> <img
									class="pdficon" src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">The guide to the
										interview</span>
							</a> &nbsp;&nbsp;&nbsp;&nbsp; <a target="_tab" href="<%=request.getContextPath()%>/files/Scenarios.pdf"> <img class="pdficon"
									src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">Scenario</span>
							</a> &nbsp;&nbsp;&nbsp;&nbsp; <a target="_tab" href="<%=request.getContextPath()%>/files/SOCA.pdf"> <img class="pdficon"
									src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">SOCA assessment</span>
							</a> &nbsp;&nbsp;&nbsp;&nbsp; <a target="_tab" href="<%=request.getContextPath()%>/files/ResearchConsentForm_SP.pdf"> <img
									class="pdficon" src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">Research consent
										form</span>
							</a>
							</b>
						</div>

						<br />


						<table border="0" cellpadding="1" cellspacing="1" style="width: 100%;border:0" class="borderless">
							<tbody>
								<tr>
									<td style="width: 30%; vertical-align: top;">
										<p>
											<a target="_blank" href="<%=request.getContextPath()%>/video/SPTraining1.mp4"> <img alt=""
												src="<%=request.getContextPath()%>/img/eqclinic-sp.png" class="trainingimg">
											</a> <a target="_blank" href="<%=request.getContextPath()%>/video/SPTraining1.mp4">
												<h3>1. Program overview</h3>
											</a> <a target="_blank" href="<%=request.getContextPath()%>/files/Summary1.pdf"> <img class="pdficon"
												src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">Summary</span>
											</a>
										</p>
									</td>
									<td style="width: 5%; vertical-align: top;"></td>
									<td style="width: 30%; vertical-align: top;">
										<p>
											<a target="_blank" href="<%=request.getContextPath()%>/video/SPTraining2.mp4"> <img alt=""
												src="<%=request.getContextPath()%>/img/eqclinic-sp1.png" class="trainingimg">
											</a> <a target="_blank" href="<%=request.getContextPath()%>/video/SPTraining2.mp4">
												<h3>2. How to use the scenario</h3>
											</a> <a target="_blank" href="<%=request.getContextPath()%>/files/Summary2.pdf"> <img class="pdficon"
												src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">Summary</span>
											</a>
										</p>
									</td>
									<td style="width: 5%; vertical-align: top;"></td>
									<td style="width: 30%; vertical-align: top;">
										<p>
											<a target="_blank" href="<%=request.getContextPath()%>/video/SPTraining3.mp4"> <img alt=""
												src="<%=request.getContextPath()%>/img/eqclinic-sp2.png" class="trainingimg">
											</a> <a target="_blank" href="<%=request.getContextPath()%>/video/SPTraining3.mp4">
												<h3>3. Booking appointments</h3>
											</a> <a target="_blank" href="<%=request.getContextPath()%>/files/Summary3.pdf"> <img class="pdficon"
												src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">Summary</span>
											</a>
										</p>
									</td>
								</tr>

								<tr>
									<td style="width: 30%; vertical-align: top;">
										<p>
											<a target="_blank" href="<%=request.getContextPath()%>/video/SPTraining4.mp4"> <img alt=""
												src="<%=request.getContextPath()%>/img/eqclinic-sp3.png" class="trainingimg">
											</a> <a target="_blank" href="<%=request.getContextPath()%>/video/SPTraining4.mp4">
												<h3>4. The interview</h3>
											</a> <a target="_blank" href="<%=request.getContextPath()%>/files/Summary4.pdf"> <img class="pdficon"
												src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">Summary</span>
											</a>
										</p>
									</td>
									<td style="width: 5%; vertical-align: top;"></td>
									<td style="width: 30%; vertical-align: top;">
										<p>
											<a target="_blank" href="<%=request.getContextPath()%>/video/SPTraining5.mp4"> <img alt=""
												src="<%=request.getContextPath()%>/img/eqclinic-sp4.png" class="trainingimg">
											</a> <a target="_blank" href="<%=request.getContextPath()%>/video/SPTraining5.mp4">
												<h3>5. Assessing the student</h3>
											</a> <a target="_blank" href="<%=request.getContextPath()%>/files/Summary5.pdf"> <img class="pdficon"
												src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">Summary</span>
											</a>
										</p>
									</td>
									<td style="width: 5%; vertical-align: top;"></td>
									<td style="width: 30%; vertical-align: top;">
										<p>
											<a target="_blank" href="<%=request.getContextPath()%>/video/SPTraining6.mp4"> <img alt=""
												src="<%=request.getContextPath()%>/img/eqclinic-sp5.png" class="trainingimg">
											</a> <a target="_blank" href="<%=request.getContextPath()%>/video/SPTraining6.mp4">
												<h3>6. Conclusion</h3>
											</a> <a target="_blank" href="<%=request.getContextPath()%>/files/Summary6.pdf"> <img class="pdficon"
												src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">Summary</span>
											</a>
										</p>
									</td>
								</tr>


							</tbody>
						</table>
						
						<div class="jumbotron" style="padding-top:22px;padding-bottom:2px">
                            <span style="font-size:20px;">To test your set up (microphone and camera) before your first interview go to <a target="_blank" href="https://ospia.med.unsw.edu.au/tester">https://ospia.med.unsw.edu.au/tester</a></span>
                        </div>
						
						<div class="jumbotron" style="padding-top:22px;padding-bottom:2px;display:none" id="agreeScenarioDiv">
							<input type="checkbox" value="0" id="agreeScenario" name="agreeScenario">&nbsp; <span class="pagefont" style="font-size:20px;">I have
								completed and understood the OSPIA process and believe that I am able to provide consultations for students.</span> <br> <br>
							<input type="button" id="submitAgreeScenario" class="usyd-ui-button-primary" value="submit" disabled>
						</div>
						
						<div class="jumbotron" style="padding-top:22px;padding-bottom:2px;display:none" id="caliLinkDiv">
                            <span style="font-size:20px;">Now you've completed the training, visit this <a target="_blank" href="https://ospia.med.unsw.edu.au/cali">Assessment Training page</a> to view sample interviews and practice assessing students. They are up to 20 minutes long - a little longer than what you will normally experience with students.</span>
                        </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<c:import url="/footer.jsp"></c:import>

</body>
</html>
