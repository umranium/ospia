<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Home</title>
<c:import url="/common.jsp"></c:import>
</head>

<body class="usyd-js-load">
	<div class="usyd-page usyd-no-feedback usyd-no-yammer ">
		<c:import url="/stuheader.jsp"></c:import>

		<c:import url="/welcome_stu.jsp"></c:import>

		<c:import url="/footer.jsp"></c:import>
	</div>
</body>
</html>
