<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Calendar</title>
<c:import url="/common.jsp"></c:import>
<link href='<%=request.getContextPath()%>/css/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<%=request.getContextPath()%>/css/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<%=request.getContextPath()%>/js/fullcalendar/moment.min.js'></script>
<script src='<%=request.getContextPath()%>/js/fullcalendar/fullcalendar.min.js'></script>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<script src='<%=request.getContextPath()%>/js/stu/19_Stu_MyCalen.js'></script>
</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<h3>
							<span id="dateSpan"> </span>
						</h3>
						<div id="emptyNote" style="display:none">
							<p>No appointment in this day.</p>
						</div>
						<div class="scrollTable" id="scrollTable">
							<table id="detailTable" class="table">
								<thead>
									<tr>
										<th>Date</th>
										<th>Time</th>
										<th>SP Name</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="tbodyList">
								</tbody>
							</table>
						</div>
						<b>
							<h3>
								<span id="dateSpan"> </span>
							</h3>
							<table id="detailTable" class="table" style="display:none">
								<thead>
									<tr>
										<th>Date</th>
										<th>Time</th>
										<th>SP Name</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="tbodyList">
								</tbody>
							</table> <span style="color:green;font-size:14px">Available &nbsp;&nbsp;&nbsp;</span> <span style="color:red;font-size:14px">Your
								Request</span>&nbsp;&nbsp;&nbsp; <span style="color:blue;font-size:14px">Confirmed Appointment</span>&nbsp;&nbsp;&nbsp; <span
							style="color:orange;font-size:14px">Unavailable</span>
						</b>
						<div id='calendar'></div>
						<div id="requestAptPopup" style="display:none">
							<p id="reqAptErrorInfo" style="color:red;display:none"></p>
							<table class="table borderless" style="border:0">
								<input type="hidden" id="makeRequestId" value="">
								<tr>
									<td colspan="2"><h4>
											<span id="requireAptDay"></span>
										</h4></td>
								</tr>
								<tr>
									<td colspan="2"><h4>
											<span id="requireAptStartTime"></span> - <span id="requireAptEndTime"></span>
										</h4></td>
								</tr>
								<tr>
									<td colspan="2"><h4>
											SP - <span id="requireSPName"></span>
										</h4></td>
								</tr>
								<!-- 
				<tr>
					<td colspan="2"><h4>
							Scenario - <span id="requireScenario">#12 AAA</span>
						</h4></td>
				</tr>
				 -->
								<tr>
									<td><button style="width:90px" class="usyd-ui-button-primary" onclick="makeRequest()" id="requestBtn">Request</button></td>
									<td><button style="width:80px" class="usyd-ui-button-primary" onclick="cancelPopup()">Cancel</button></td>
								</tr>
							</table>
						</div>

						<div id="cancelAptPopup" style="display:none">
							<input type="hidden" id="cancelAptId" value="">
							<p id="cancelAptErrorInfo" style="color: red; display: none"></p>
							<table class="table borderless" style="border:0">
								<tr>
									<td colspan="2"><h4>What is the reason to cancel this appointment?</h4></td>
								</tr>
								<tr>
									<td colspan="2"><textarea id="cancelReason" name="cancelReason" cols="50" rows="8"></textarea></td>
								</tr>
								<tr>
									<td><button style="width:80px" class="usyd-ui-button-primary" onclick="confirmCancelApt()">Submit</button></td>
									<td><button style="" class="usyd-ui-button-primary" onclick="cancelPopup()">I don't want to Cancel</button></td>
								</tr>
							</table>
						</div>

						<div id="waitingPopup" style="display:none">
							<h4>Please wait a moment.</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
