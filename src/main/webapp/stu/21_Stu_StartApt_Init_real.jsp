<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Conversation</title>
<c:import url="/common.jsp"></c:import>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- <script src='<%=request.getContextPath()%>/js/opentok.min.js'></script> -->
<script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
<script type="text/javascript">
	var apiKey = "${apiKey}";
	var sessionId = "${sessionId}";
	var token = "${token}";
</script>
<script src="<%=request.getContextPath()%>/js/stu/21_Stu_StartApt_Init_real.js"></script>
<style type="text/css">
.OT_subscriber:-webkit-full-screen {
	width: 100% !important;
	height: 100% !important;
}

.OT_subscriber:-moz-full-screen {
	width: 100% !important;
	height: 100% !important;
}

.OT_subscriber:-ms-full-screen {
	width: 100% !important;
	height: 100% !important;
}

.OT_subscriber:-o-full-screen {
	width: 100% !important;
	height: 100% !important;
}

.OT_subscriber:full-screen {
	width: 100% !important;
	height: 100% !important;
}

.OT_publisher:-webkit-full-screen {
	width: 100% !important;
	height: 100% !important;
}

.OT_publisher:-moz-full-screen {
	width: 100% !important;
	height: 100% !important;
}

.OT_publisher:-ms-full-screen {
	width: 100% !important;
	height: 100% !important;
}

.OT_publisher:-o-full-screen {
	width: 100% !important;
	height: 100% !important;
}

.OT_publisher:full-screen {
	width: 100% !important;
	height: 100% !important;
}
</style>
</head>

<body id="bigBody">

	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<input type="hidden" id="hiddenArchiveId" />
						<div class="container">
							<table border="0" cellpadding="1" cellspacing="1" style="width: 100%;border:0" class="borderless">
								<tbody>
									<tr>
										<td style="width: 65%; vertical-align: top;">
											<div id="participantImageTitle">
												<h4>Simulated Patient</h4>
											</div> <br>
											<h3 id="noShownNote">Please share your camera and microphone if requested. <br/><br/> Thanks for participating in this interaction, please wait whilst the call connects. Thank you.</h3>
											<div id="oppositeContainer" style="width: 640px; height: 480px;">
												<div id="fullScreenDiv" style="display:none">
													<!-- 
													    <p style="float:right; font-size:16px;font-weight:bold;color:red;background-color:black;">Please refresh this page</p>
													 -->
													 <p id="timer" style="font-size:20px;font-weight:bold;color:white;text-align:center; background-color:black;padding:0px;margin:0px;">0 Minute</p>
												</div>

												<button class="usyd-ui-button-primary" style="float:right; display:none; width:180px;height:40px;" id="stop_fullscreen">Finish
													Interview</button>
											</div>
										</td>
										<td style="width: 3%; vertical-align: top;"></td>
										<td style="width: 32%; vertical-align: top;">
											<div id="yourImageTitle">
												<h4>Student</h4>
											</div>
											<div id="selfContainer">This is the image of yourself</div> <br /> <input type="button" id="fullScreen"
											class="usyd-ui-button-primary" value="Start formal interview"> <br /> <br /> <input type="button"
											id="fullScreenAnyTime" class="usyd-ui-button-primary" value="Full Screen" style="display:none">
											<h3><b>Please note: </b></h3>
						                    <p>1. After an informal introduction with the simulated patient, click the "START FORMAL INTERVIEW" button.</p>
						                    <p>2. If you experience an interruption, please refresh this page.</p>
						                    <p>&nbsp; -- You will no longer be in full screen mode. If you wish to, click the "FULL SCREEN" button to return to full screen mode.</p>
						                    <p>&nbsp; -- It is not possible to re-start an interview - you need to continue from when the interruption occurred. In this case the timer will be inaccurate, and so it is important to keep track of for how much longer you will continue.</p>
						                    <p>&nbsp; -- Do not exit full screen mode once the interview is underway.</p>
						                    <p>3. Please use ear- or head-phones for all appointments.</p>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
