<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Consultation feedback</title>
<c:import url="/common.jsp"></c:import>
<link href="<%=request.getContextPath()%>/css/wufoo/form.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/wufoo/theme.css" rel="stylesheet">
</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<form id="Survey_SOCA" name="Survey_SOCA" action="spSOCA_submit">

							<header class="info">
								<h2>Student assessment on the reflection feedback information</h2>
								<div>By providing the reflection information (nonverbal behavior and SOCA rating), how do you rate following question:</div>
							</header>

							<ul>

								<li id="foli108" class="likert notranslate col4">
									<table cellspacing="0">
										<thead>
											<tr>
												<th style="width:30%">&nbsp;</th>
												<td style="width:10%">1</td>
												<td style="width:10%">2</td>
												<td style="width:10%">3</td>
												<td style="width:10%">4</td>
												<td style="width:10%">5</td>
											</tr>
										</thead>
										<tbody>
											<tr class="statement1 alt">
												<th><label for="Field1">To what extent do you think the ROC feedback information was informative?</label></th>
												<td title="1"><input id="Field1_1" name="Field1" type="radio" tabindex="1" value="1" /></td>
												<td title="2"><input id="Field1_2" name="Field1" type="radio" tabindex="2" value="2" /></td>
												<td title="3"><input id="Field1_3" name="Field1" type="radio" tabindex="3" value="3" /></td>
												<td title="4"><input id="Field1_4" name="Field1" type="radio" tabindex="4" value="4" /></td>
												<td title="5"><input id="Field1_5" name="Field1" type="radio" tabindex="5" value="5" /></td>
											</tr>
											<tr class="statement1 alt">
												<th><label for="Field1">To what extent do you think the ROC feedback information was confusing?</label></th>
												<td title="1"><input id="Field2_1" name="Field2" type="radio" tabindex="1" value="1" /></td>
												<td title="2"><input id="Field2_2" name="Field2" type="radio" tabindex="2" value="2" /></td>
												<td title="3"><input id="Field2_3" name="Field2" type="radio" tabindex="3" value="3" /></td>
												<td title="4"><input id="Field2_4" name="Field2" type="radio" tabindex="4" value="4" /></td>
												<td title="5"><input id="Field2_5" name="Field2" type="radio" tabindex="5" value="5" /></td>
											</tr>
											<tr class="statement1 alt">
												<th><label for="Field1">To what extent do you think the ROC feedback information will help you develop as a
														medical student? </label></th>
												<td title="1"><input id="Field3_1" name="Field" type="radio" tabindex="1" value="1" /></td>
												<td title="2"><input id="Field3_2" name="Field3" type="radio" tabindex="2" value="2" /></td>
												<td title="3"><input id="Field3_3" name="Field3" type="radio" tabindex="3" value="3" /></td>
												<td title="4"><input id="Field3_4" name="Field3" type="radio" tabindex="4" value="4" /></td>
												<td title="5"><input id="Field3_5" name="Field3" type="radio" tabindex="5" value="5" /></td>
											</tr>
											<tr class="statement1 alt">
												<th><label for="Field1">To what extent will you change your communication behaviour with patients after
														seeing the ROC feedback information?</label></th>
												<td title="1"><input id="Field4_1" name="Field4" type="radio" tabindex="1" value="1" /></td>
												<td title="2"><input id="Field4_2" name="Field4" type="radio" tabindex="2" value="2" /></td>
												<td title="3"><input id="Field4_3" name="Field4" type="radio" tabindex="3" value="3" /></td>
												<td title="4"><input id="Field4_4" name="Field4" type="radio" tabindex="4" value="4" /></td>
												<td title="5"><input id="Field4_5" name="Field4" type="radio" tabindex="5" value="5" /></td>
											</tr>
										</tbody>
									</table>
								</li>

								<li id="foli112" class="notranslate  threeColumns     ">
									<fieldset>
										<![if !IE | (gte IE 8)]>
										<legend id="title112" class="desc"> Which sections of the feedback information were most helpful for you? </legend>
										<![endif]>
										<!--[if lt IE 8]>
						<label id="title112" class="desc">
						Which sections of the feedback information were most helpful for you?
						</label>
						<![endif]-->
										<div>
											<input id="checkboxDefault_112" name="checkboxDefault_112" type="hidden" value="" /> <span> <input
												id="Field112_0" name="Field112" type="checkbox" class="field checkbox" value="Turn taking" tabindex="10" /> <label
												class="choice" for="Field112_0"> Turn taking</label>
											</span> <span> <input id="Field113_1" name="Field113_1" type="checkbox" class="field checkbox" value="Sound loudness"
												tabindex="11" /> <label class="choice" for="Field112_1"> Sound loudness</label>
											</span> <span> <input id="Field113_2" name="Field113_2" type="checkbox" class="field checkbox" value="Sound pitch"
												tabindex="12" /> <label class="choice" for="Field112_2"> Sound pitch</label>
											</span> <span> <input id="Field113_3" name="Field113_3" type="checkbox" class="field checkbox" value="Head tilting"
												tabindex="13" /> <label class="choice" for="Field112_3"> Head tilting</label>
											</span> <span> <input id="Field113_4" name="Field113_4" type="checkbox" class="field checkbox" value="Body leaning"
												tabindex="14" /> <label class="choice" for="Field112_4"> Body leaning</label>
											</span> <span> <input id="Field113_5" name="Field113_5" type="checkbox" class="field checkbox" value="Smiling"
												tabindex="15" /> <label class="choice" for="Field112_5"> Smiling</label>
											</span> <span> <input id="Field113_6" name="Field113_6" type="checkbox" class="field checkbox" value="Forwning"
												tabindex="16" /> <label class="choice" for="Field112_6"> Forwning</label>
											</span> <span> <input id="Field113_7" name="Field113_7" type="checkbox" class="field checkbox" value="Head nodding"
												tabindex="17" /> <label class="choice" for="Field112_7"> Head nodding</label>
											</span> <span> <input id="Field113_8" name="Field113_8" type="checkbox" class="field checkbox" value="Head shaking"
												tabindex="18" /> <label class="choice" for="Field112_8"> Head shaking</label>
											</span> <span> <input id="Field113_9" name="Field113_9" type="checkbox" class="field checkbox" value="Face touching"
												tabindex="19" /> <label class="choice" for="Field112_9"> Face touching</label>
											</span>
										</div>
									</fieldset>
								</li>


								<li id="foli112" class="notranslate  threeColumns     ">
									<fieldset>
										<![if !IE | (gte IE 8)]>
										<legend id="title112" class="desc"> Which sections of the feedback information were least helpful for you? </legend>
										<![endif]>
										<!--[if lt IE 8]>
						<label id="title112" class="desc">
						Which sections of the feedback information were most helpful for you?
						</label>
						<![endif]-->
										<div>
											<input id="checkboxDefault_112" name="Field112" type="hidden" value="" /> <span> <input id="Field112_0"
												name="Field112" type="checkbox" class="field checkbox" value="Turn taking" tabindex="10" /> <label class="choice"
												for="Field112_0"> Turn taking</label>
											</span> <span> <input id="Field112_1" name="Field112_1" type="checkbox" class="field checkbox" value="Sound loudness"
												tabindex="11" /> <label class="choice" for="Field112_1"> Sound loudness</label>
											</span> <span> <input id="Field112_2" name="Field112_2" type="checkbox" class="field checkbox" value="Sound pitch"
												tabindex="12" /> <label class="choice" for="Field112_2"> Sound pitch</label>
											</span> <span> <input id="Field112_3" name="Field112_3" type="checkbox" class="field checkbox" value="Head tilting"
												tabindex="13" /> <label class="choice" for="Field112_3"> Head tilting</label>
											</span> <span> <input id="Field112_4" name="Field112_4" type="checkbox" class="field checkbox" value="Body leaning"
												tabindex="14" /> <label class="choice" for="Field112_4"> Body leaning</label>
											</span> <span> <input id="Field112_5" name="Field112_5" type="checkbox" class="field checkbox" value="Smiling"
												tabindex="15" /> <label class="choice" for="Field112_5"> Smiling</label>
											</span> <span> <input id="Field112_6" name="Field112_6" type="checkbox" class="field checkbox" value="Forwning"
												tabindex="16" /> <label class="choice" for="Field112_6"> Forwning</label>
											</span> <span> <input id="Field112_7" name="Field112_7" type="checkbox" class="field checkbox" value="Head nodding"
												tabindex="17" /> <label class="choice" for="Field112_7"> Head nodding</label>
											</span> <span> <input id="Field112_8" name="Field112_8" type="checkbox" class="field checkbox" value="Head shaking"
												tabindex="18" /> <label class="choice" for="Field112_8"> Head shaking</label>
											</span> <span> <input id="Field112_9" name="Field112_9" type="checkbox" class="field checkbox" value="Face touching"
												tabindex="19" /> <label class="choice" for="Field112_9"> Face touching</label>
											</span>
										</div>
									</fieldset>
								</li>


								<li id="foli108" class="likert notranslate col4">
									<table cellspacing="0">
										</caption>
										<thead>
											<tr>
												<th style="width:30%">&nbsp;</th>
												<td style="width:10%">1</td>
												<td style="width:10%">2</td>
												<td style="width:10%">3</td>
												<td style="width:10%">4</td>
												<td style="width:10%">5</td>
											</tr>
										</thead>
										<tbody>
											<tr class="statement1 alt">
												<th><label for="Field1">To what extent would you be open to using the ROC feedback system again?</label></th>
												<td title="1"><input id="Field5_1" name="Field5" type="radio" tabindex="1" value="1" /></td>
												<td title="2"><input id="Field5_2" name="Field5" type="radio" tabindex="2" value="2" /></td>
												<td title="3"><input id="Field5_3" name="Field5" type="radio" tabindex="3" value="3" /></td>
												<td title="4"><input id="Field5_4" name="Field5" type="radio" tabindex="4" value="4" /></td>
												<td title="5"><input id="Field5_5" name="Field5" type="radio" tabindex="5" value="5" /></td>
											</tr>
										</tbody>
									</table>
								</li>


								<li class="notranslate      "><label class="desc" for="Field417"> Do you have any comments to help us improve
										the system? </label>

									<div>
										<textarea id="Field13" name="Field13" class="field textarea medium" spellcheck="true" rows="10" cols="50" tabindex="18"
											onkeyup=""></textarea>
									</div></li>

								<li class="buttons ">
									<div>
										<a href="33_Tut_SP_screen.jsp"> <input id="saveForm" name="saveForm" class="usyd-ui-button-primary" type="button"
											value="Submit" />
										</a>
									</div>
								</li>

								<li class="hide"><label for="comment">Do Not Fill This Out</label> <textarea name="comment" id="comment" rows="1"
										cols="1"></textarea> <input type="hidden" id="idstamp" name="idstamp" value="cGiw7XDVJ61c1PupHNZReMGpotcRLgHz3ypyJA+gKAg=" /></li>
							</ul>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
