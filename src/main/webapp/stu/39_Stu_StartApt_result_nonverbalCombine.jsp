<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Nonverbal Reflection</title>
<c:import url="/common.jsp"></c:import>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/timeline.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/timeline_cs.css">
<script src="<%=request.getContextPath()%>/js/highstock.js"></script>
<script src="<%=request.getContextPath()%>/js/exporting.js"></script>
<script src="<%=request.getContextPath()%>/js/annotations.js"></script>
<script type="text/javascript">
	var shakingline = "${shakingline}";
	var noddingline = "${noddingline}";
	var selftouchline = "${selftouchline}";
	var video_name = "${video_name}";
	var angerline = "${angerline}";
	var disgustline = "${disgustline}";
	var fearline = "${fearline}";
	var happyline = "${happyline}";
	var sadnessline = "${sadnessline}";
	var surpriseline = "${surpriseline}";
	var endPoints_doctor = "${endPoints_doctor}";
	var endPoints_patient = "${endPoints_patient}";
	var notesString = '${notesString}';
	var comments = eval(notesString);

	var loudnessTop = "${baselineObject.loudnessTop}";
	var loudnessBottom = "${baselineObject.loudnessBottom}";
	var pitchTop = "${baselineObject.pitchTop}";
	var pitchBottom = "${baselineObject.pitchBottom}";
	var leaningTop = "${baselineObject.leaningTop}";
	var leaningBottom = "${baselineObject.leaningBottom}";
	var tiltingTop = "${baselineObject.tiltingTop}";
	var tiltingBottom = "${baselineObject.tiltingBottom}";
	var movementTop = "${baselineObject.movementTop}";
	var movementBottom = "${baselineObject.movementBottom}";
	var items = "${items}";

	var totalFrame = "${totalFrame}";
	var totalLength = "${totalLength}";
	var reflectionLogId = "${reflectionLogId}";
</script>
<script src='<%=request.getContextPath()%>/js/jsapi.js'></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/timeline.js"></script>
<script src='<%=request.getContextPath()%>/js/stu/39_Stu_StartApt_result_nonverbalCombine.js'></script>
</head>

<body>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<div class="container" id="bigContainer">
							<div class="row">
								<div style="text-align:center;">
									<video id="video" controls>
										<source src="<%=request.getContextPath()%>/video/${video_name}/${video_name}.mp4" type="video/mp4">
									</video>
								</div>
								<br />
								<div>
									<div id="nonverbal_div1">
										<div id="turntaking"></div>
										<hr class="nvbfeedbackDividLine">
									</div>
									<div id="nonverbal_div2">
										<div id="speechpercentagecontainer"></div>
										In order to review the detail of 'Speak Ratio', you can it split by : <select id="speakpercentagesplit">
											<option value="100">Whole</option>
											<option value="1">1 Minute</option>
											<option value="2">2 Minutes</option>
											<option value="4">4 Minutes</option>
											<option value="8">8 Minutes</option>
										</select>
										<hr class="nvbfeedbackDividLine">
									</div>

									<div id="nonverbal_div3">
										<div id="loudnesscontainer"></div>
										<hr class="nvbfeedbackDividLine">
									</div>
									
									<div id="nonverbal_div4">
										<div id="pitchcontainer"></div>
										<hr class="nvbfeedbackDividLine">
									</div>
									<div id="nonverbal_div5">
										<div id="smilecontainer"></div>
										<hr class="nvbfeedbackDividLine">
									</div>
									<div id="nonverbal_div6">
										<div id="frowncontainer"></div>
										<hr class="nvbfeedbackDividLine">
									</div>
									<div id="nonverbal_div7">
										<div id="leaningcontainer"></div>
										<hr class="nvbfeedbackDividLine">
									</div>
									<div id="nonverbal_div8">
										<div id="tiltingcontainer"></div>
										<hr class="nvbfeedbackDividLine">
									</div>
									<div id="nonverbal_div9">
										<div id="headmovment"></div>
										<hr class="nvbfeedbackDividLine">
									</div>
									<div id="nonverbal_div14">
										<div id="attentioncontainer"></div>
									</div>
									<!-- 
									<div id="nonverbal_div10">
										<p id="selftouchTimes"></p>
										<div id="selftouchmovment"></div>
									</div>
									<div id="nonverbal_div12">
										<div id="touchescontainer"></div>
										<hr class="nvbfeedbackDividLine">
									</div>
									<div id="nonverbal_div13">
										<div id="movementcontainer"></div>
										<hr class="nvbfeedbackDividLine">
									</div>
									<div id="nonverbal_div15">
										<div id="stretchcontainer"></div>
										<hr class="nvbfeedbackDividLine">
									</div>
									 -->

								</div>

							</div>
						</div>

						<div class="container">
							<div class="row">
								<div class="col-sm-11 col-sm-offset-1 blog-sidebar"></div>
							</div>
						</div>
						<br> <br> <br> <br> <br> <br>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="reporting" style="display:none"></div>

	<c:import url="/footer.jsp"></c:import>

</body>
</html>
