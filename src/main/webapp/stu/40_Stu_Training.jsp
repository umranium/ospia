<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Training</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/stu/40_Stu_Training.js"></script>
</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<input type="hidden" id="doctorId" value="<sec:authentication property="principal.doctor.doctorId" />">
	
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<div class="jumbotron" style="padding-top:0px;padding-bottom:0px;margin-bottom:0px;">
							<h3>Welcome to the training page for OSPIA</h3>
                            <br>
							<b style="text-align:left"> <a target="_tab" href="<%=request.getContextPath()%>/files/CodeOfConductStu.pdf"> <img
									class="pdficon" src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">The code of
										conduct</span>
							</a> &nbsp;&nbsp;&nbsp;&nbsp; <a target="_tab" href="<%=request.getContextPath()%>/files/GuideToInterviewStu.pdf"> <img
									class="pdficon" src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">The guide to the
										interview</span>
							</a> &nbsp;&nbsp;&nbsp;&nbsp; <a target="_tab" href="<%=request.getContextPath()%>/files/SOCA.pdf"> <img class="pdficon"
									src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">SOCA assessment</span>
							</a> &nbsp;&nbsp;&nbsp;&nbsp; 
							<br/>
							<a target="_tab" href="<%=request.getContextPath()%>/files/ResearchConsentForm_Stu.pdf"> <img
									class="pdficon" src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">Research consent
										form</span>
							</a> &nbsp;&nbsp;&nbsp;&nbsp; <a target="_tab" href="<%=request.getContextPath()%>/files/Summary_Student.pdf"> <img
									class="pdficon" src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">Instruction
										Summary</span>
							</a>
							</b>
						</div>
						<div class="row">
							<section class="col-sm-12">
								<a id="main-content"></a>
								<div class="jumbotron">
									<video id="myvideo" width="640" controls>
										<source src="<%=request.getContextPath()%>/video/StuTraining.mp4" type="video/mp4">
									</video>
								</div>
							</section>
						</div>
						
						<div class="jumbotron" style="padding-top:22px;padding-bottom:2px">
                            <span style="font-size:20px;">To test your set up (microphone and camera) before your first interview go to <a target="_blank" href="https://ospia.med.unsw.edu.au/tester">https://ospia.med.unsw.edu.au/tester</a></span>
                        </div>
						
						<sec:authentication property='principal.doctor.training' var="training"/>
						<div class="jumbotron" style="padding-top:22px;padding-bottom:2px; <c:if test="${training==1}">display:none</c:if>"  id="agreeScenarioDiv"  >
                            <input type="checkbox" value="0" id="agreeScenario" name="agreeScenario">&nbsp; <span class="pagefont" style="font-size:20px;">I have completed and understood the OSPIA training and believe that I am able to have consultations with simulated patients. I agree to the Code of Conduct for use of the OSPIA platform.</span> <br> <br>
                            <input type="button" id="submitAgreeScenario" class="usyd-ui-button-primary" value="submit">
                        </div>
                        
					</div>
				</div>
			</div>
		</div>

	</div>
	<c:import url="/footer.jsp"></c:import>

</body>
</html>
