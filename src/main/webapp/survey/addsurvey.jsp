
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Add Survey</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/survey/addsurvey.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<form class="form-horizontal">

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput"></label>
									<div class="controls">
										<h3>Create a survey</h3>
									</div>
								</div>
							</div>

							<div class="form-group" id="registerErroDiv" style="display:none">
								<label class="col-md-4 control-label" for="textinput"></label>
								<div class="col-md-4">
									<p style="color:red" id="registerError"></p>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput">Survey
										Title</label>
									<div class="controls">
										<input id="title" name=title type="text"
											placeholder="Survey Title">
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput">Survey
										Description</label>
									<div class="controls">
										<textarea id="description" name="description" rows="10"
											cols="100"></textarea>
									</div>
								</div>
							</div>
						</form>

						<div id="searchResultDiv">
							<h3>Question list</h3>
							<div style="height:500px;overflow: auto;">
								<table class="table">
									<thead>
										<tr>
											<th></th>
											<th>Title</th>
											<th>Description</th>
											<th>Type</th>
											<th>Options</th>
										</tr>
									</thead>

									<tbody>
										<c:forEach var="question" items="${questions}">
											<tr>
												<td><input type="checkbox" id="qcheckbox${question.id}" /></td>
												<td>${question.title}</td>
												<td>${question.description}</td>
												<td>${question.typeS}</td>
												<td><c:forEach var="option" items="${question.options}">
							         ${option.optkey}:${option.optvalue} &nbsp;
							     </c:forEach></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
						<br />
						<form class="form-horizontal">
							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput"></label>
									<div class="controls">
										<input class="usyd-ui-button-primary" type="button"
											value="save this survey" id="saveSurvey" />
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
