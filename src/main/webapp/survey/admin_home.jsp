<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Admin Home</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>

	<c:import url="/programinfo_sp.jsp"></c:import>

	<c:import url="/footer.jsp"></c:import>
</body>
</html>
