<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Admin Login</title>
<c:import url="/common.jsp"></c:import>
<style type="text/css">
#loginForm label.error {
	color: red;
	margin-left: 10px;
	width: auto;
	display: inline;
}
</style>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/sp/02_SP_login.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<div class="usyd-inline-wrap">
							<h1>Log in</h1>
						</div>
						<form class="form-horizontal"
							action="<%=request.getContextPath()%>/survey/j_spring_security_check"
							method="post" id="loginForm">
							<div class="usyd-form-component"
								<c:if test="${param.error != 'true'}"> style="display:none" </c:if>>
								<div class="control-group" id="errorInfo" style="color:red">
									<label class="control-label" for="textinput"></label>
									<div class="controls">
										<p>invalid username or password</p>
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="textinput">User Email</label>
									<div class="controls">
										<!-- <input id="username" name="username" type="text" placeholder="Email"> -->
										<input id="j_username" name="j_username" type="text" />
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="passwordinput">Password</label>
									<div class="controls">
										<!-- <input id="password" name="password" type="password" placeholder="Password"	onkeydown='if(event.keyCode==13){submitForm();}'> -->
										<input id="j_password" name="j_password" type="password" />
									</div>
								</div>
							</div>

							<div class="usyd-form-component">
								<div class="control-group">
									<label class="control-label" for="singlebutton"></label>
									<div class="controls">
										<!-- <input id="submit_login" name="submit_login" class="usyd-ui-button-primary" type="button" value="Log In"> -->
										<input id="submit_login" name="submit_login"
											class="usyd-ui-button-primary" type="submit" value="Log In">
									</div>
								</div>
							</div>
						</form>
					</div>

				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>

</body>
</html>
