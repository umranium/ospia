<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Survey List</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/survey/listpatient.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<div style="text-align:center">
							<form action="<%=request.getContextPath()%>/survey/patients" method="GET">
								<input type="text" id="searchContent" name="searchContent" value="${searchContent}" style="width:500px;"> <input
									type="submit" id="patientSearchBtn" value="search" class="usyd-ui-button-primary">
							</form>
						</div>
						<div id="searchResultDiv" style="">
							<br>
							<h3>All registered simulate patients</h3>
							<table class="table">
								<thead>
									<tr>
										<th style="width:20%">Name</th>
										<th style="width:20%">Mobile phone</th>
										<th style="width:20%">Status</th>
										<th style="width:20%">Register at</th>
										<th>Operations</th>
									</tr>
								</thead>

								<tbody>
									<c:forEach var="patient" items="${patients}">
										<tr>
											<td>${patient.firstname}&nbsp;${patient.lastname}</td>
											<td>${patient.phone}</td>
											<c:if test="${patient.status==0}">
												<td>Disabled</td>
											</c:if>

											<c:if test="${patient.status==1}">
												<td>Active</td>
											</c:if>
											<jsp:useBean id="dateValue" class="java.util.Date"/>
											<jsp:setProperty name="dateValue" property="time" value="${patient.createAt}"/>
											<td><fmt:formatDate value="${dateValue}" pattern="dd/MM/yyyy HH:mm"/></td>
											<td>
												<a class="usyd-ui-button-primary" href="<%=request.getContextPath()%>/survey/editpatient/${patient.patientid}">Edit</a> <c:if
													test="${patient.status==1}">
													<button class="usyd-ui-button-primary" onclick="disablePatient(${patient.patientid})">Disable</button>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
