<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Survey List</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/survey/listsurvey.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<div id="searchResultDiv" style="">
							<a href="<%=request.getContextPath()%>/survey/addsurvey"><button
									class="usyd-ui-button-primary">Create a survey</button></a> <a
								href="<%=request.getContextPath()%>/survey/listquestion"><button
									class="usyd-ui-button-primary">Question list</button></a>

							<br><br>
							<h3>All the survey</h3>
							<table class="table">
								<thead>
									<tr>
										<th style="width:20%">Title</th>
										<th style="width:20%">Description</th>
										<th style="width:40%">Questions</th>
										<th>Operations</th>
									</tr>
								</thead>

								<tbody>
									<c:forEach var="survey" items="${surveys}">
										<tr>
											<td>${survey.title}</td>
											<td>${survey.description}</td>

											<c:set var="orders"
												value="${survey.orderOfQuestions.split(',')}" />

											<td><c:forEach var="order" items="${orders}">
													<c:forEach var="question" items="${survey.questions}">
														<c:if test="${question.id == order }">
                                         ${question.title}<br />
														</c:if>
													</c:forEach>
												</c:forEach></td>

											<td><a
												href="<%=request.getContextPath()%>/survey/editsurvey/${survey.id}"><img
													src="<%=request.getContextPath()%>/img/edit.png"
													class="operationicon"></a> <a
												onclick="deleteSurvey(${survey.id})"><img
													src="<%=request.getContextPath()%>/img/delete.png"
													class="operationicon"></a> <a target="_new"
												href="<%=request.getContextPath()%>/survey/preview/${survey.id}"><img
													src="<%=request.getContextPath()%>/img/find.png"
													class="operationicon"></a> <a target="_new"
												href="<%=request.getContextPath()%>/survey/answersurvey/${survey.id}"><img
													src="<%=request.getContextPath()%>/img/answer.png"
													class="operationicon"></a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
