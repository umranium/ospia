<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath =
                    request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
                                    + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>

<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/sp/welcome_sp.js"></script>
</head>

<body>

	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<div class="jumbotron">
							<video id="myvideo" width="640" height="360" controls> <source
								src="<%=request.getContextPath()%>/video/Ospia Training Introduction_2016.mp4" type="video/mp4"></video>
						</div>
						<div class="jumbotron" style="padding-top:2px;padding-bottom:2px;">
							<h2>OSPIA for Simulated Patients</h2>
							<br />
							<p class="lead" style=" text-align:justify">Welcome to the Online Simulated Patient Interaction and Assessment website
								(OSPIA).</p>
							<p class="lead" style=" text-align:justify">OSPIA's aim is to help our medical students feel more comfortable with, and
								be more successful at having respectful, caring and effective conversations with patients.</p>
							<p class="lead" style=" text-align:justify">We rely on volunteers to act as "simulated patients" (SPs) so that students
								can practice developing a great bedside manner. Online practice is just one new way in which we are already doing this with
								our students. <a id="readMoreLink" onclick="showAllScript()">Read more.. </a></p>
							
							<div id="allscriptdiv" style="display:none">
							     <p class="lead" style=" text-align:justify">
							     The SP is provided with a patient scenario complete with all the information needed to share with the student. This fictional medical history means a volunteer never shares their own medical details.
							     </p>
							     
							     <p class="lead" style=" text-align:justify">
							     The student's job is to speak to the SP and gain a picture of the medical history and any underlying issue.  No memorising of the scenario is necessary.  In the training provided after registration, we give tips on how you should conduct the interview.
							     </p>
							     
							     <p class="lead" style=" text-align:justify">
							     The great thing about OSPIA is the flexibility: you can take part almost anywhere, at a time that suits you.  You indicate when you are available through the calendar system, and a student will formalize that appointment time with you. You will be sent emails and text messages to confirm and remind you about appointments.
							     </p>
							     
							     <p class="lead" style=" text-align:justify">
							     One appointment plus the assessment takes approximately 30 minutes.
                                 </p>
                                 
                                 <p class="lead" style=" text-align:justify">
                                 What an SP needs: 
                                 </p>
                                 
                                
                                    <ul style="font-size:20px;text-align:left; margin-left:40px;">
                                       <li>
                                            a desktop computer, laptop or Android tablet (no iPads), running the Firefox or Chrome browser
                                       </li>
                                       
                                       <li>
                                            a stable internet connection
                                       </li>
                                       
                                       <li>
                                            a working webcam and microphone
                                       </li>
                                        
                                    </ul>                                    
                                 
							     <p class="lead" style=" text-align:justify">
                                 Volunteering as an online simulated patient for OSPIA is a great way to get involved in helping our future doctors learn crucial communication skills. 
                                 </p>
                                 
                                 <p class="lead" style=" text-align:justify">
                                 If you are interested, please proceed to register now. 
                                 </p>
                                 
                                  <p class="lead" style=" text-align:center">
                                       <button class="usyd-ui-button-primary" onclick="location.href='<%=request.getContextPath()%>/sp/uregister_sp'">Register</button>
                                 </p>
                                 
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>
</html>
