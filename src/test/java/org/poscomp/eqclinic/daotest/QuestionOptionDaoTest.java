package org.poscomp.eqclinic.daotest;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.poscomp.survey.dao.QuestionOptionDao;
import org.poscomp.survey.domain.QuestionOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

@ContextConfiguration(locations = {"classpath:spring-mvc.xml", "classpath:applicationContext.xml",
                "classpath:applicationContext-hibernate.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@Transactional
public class QuestionOptionDaoTest {

    @Autowired
    private QuestionOptionDao questionOptionDao;

    // public void saveQuestionOption(QuestionOption questionOption);
    @Test
    public void testSaveQuestionOption() {
        QuestionOption qo1 = new QuestionOption();
        qo1.setOptkey("A");
        qo1.setOptvalue("very good");
        QuestionOption qo2 = new QuestionOption();
        qo2.setOptkey("B");
        qo2.setOptvalue("good");
        questionOptionDao.saveQuestionOption(qo1);
        questionOptionDao.saveQuestionOption(qo2);
    }
    // java.lang.UnsupportedClassVersionError: javax/interceptor/InterceptorBinding : Unsupported
    // major.minor version 51.0

    //
    // // public QuestionOption findById(int id);
    // @Test
    // public void testFindById() {
    // int id = 1;
    // QuestionOption qo = questionOptionDao.findById(id);
    // System.out.println(qo.getKey());
    // System.out.println(qo.getValue());
    // }
    //
    //
    // // public List<QuestionOption> findByIds(ArrayList<Integer> ids);
    // @Test
    // public void testFindByIds() {
    // ArrayList<Integer> ids = new ArrayList<Integer>();
    // ids.add(1);
    // ids.add(2);
    // List<QuestionOption> qo = (List<QuestionOption>)questionOptionDao.findByIds(ids);
    // System.out.println(qo.size());
    // }

}
